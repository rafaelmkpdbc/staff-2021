import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {
    @Test
    public void obterContatoPorNome() {
        AgendaContatos agenda = new AgendaContatos();
        Contato marcos = new Contato( "Marcos", "555555" );
        Contato mithrandir = new Contato( "Mithrandir", "444444" );
        agenda.adicionarContato( marcos );
        agenda.adicionarContato( mithrandir );
        String buscado = agenda.obterTelefone("Marcos");
        assertEquals( "555555", buscado );
    }
    
    @Test
    public void obterContatoPorTelefone() {}
    
    @Test
    public void listarContatos(){}
}
