import java.util.*;
import java.text.Collator;

public class AgendaContatos {
    private ArrayList<Contato> contatos1;
    private HashMap<String, String> contatos;
    
    {
        this.contatos1 = new ArrayList<>();
    }
    
    public AgendaContatos() {
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionarContato ( String nome, String telefone ) {
        contatos.put( nome, telefone );
    }
    
    public String obterTelefone ( String nome ) {
        return contatos.get( nome );
    }
    
    public String obterNome ( String telefone ) {
        for( HashMap.Entry<String, String> par : contatos.entrySet() ) {
            if( par.getValue().equals( telefone ) ) {
                return par.getKey();
            }
        }
        
        return null;
    }
    
    public String toCSV() {
        StringBuilder texto = new StringBuilder("");
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, String> par : contatos.entrySet() ) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format( "%s, %s%s", chave, valor, separador );
            texto.append(contato);
            
        }
        
        return texto.toString();
    }
}
