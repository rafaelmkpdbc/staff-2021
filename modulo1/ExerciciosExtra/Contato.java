public class Contato {
   private String nome;
   private String tel;
   
   public Contato(String nome, String tel) {
       this.nome = nome;
       this.tel = tel;
   }
   
   public String getNome() {
       return this.nome;
   }
   
   public void setNome( String nome ) {
       this.nome = nome;
   }
   
   public String getTel() {
       return this.tel;
   }
   
   public void setTel( String tel ) {
       this.tel = tel;
   }
   
   public String toString() {
       return this.nome + ", " + this.tel;
   }
   
   public boolean equals(Object obj ) {
        Contato outroContato = (Contato)obj;
        return this.nome == outroContato.getNome() && 
                this.tel == outroContato.getTel(); 
    }
}
