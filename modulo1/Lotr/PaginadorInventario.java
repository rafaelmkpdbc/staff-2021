import java.util.*;

public class PaginadorInventario {
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario( Inventario inventario ) {
        this.inventario = inventario;
        this.marcador = 0;
    }
    
    public void pular( int inicial ) {
        if( this.inventario.getItems().size() > inicial ) this.marcador = inicial;
    }
    
    public ArrayList<Item> limitar( int tamanho ) {
        ArrayList<Item> itens = new ArrayList<>();
        int ptFinal;
        tamanho--;
        if( this.marcador + tamanho < this.inventario.getItems().size() ) {
            ptFinal = this.marcador + tamanho;
        }else {
            ptFinal = this.inventario.getItems().size();
        }

        for( int i = this.marcador; i <= ptFinal; i++ ){
            itens.add( this.inventario.obter(i) );
        }
        
        return itens;
    }
}
