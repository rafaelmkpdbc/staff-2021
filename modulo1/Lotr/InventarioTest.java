import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest
{
    /*TESTE LEGADO
    @Test
    public void adiciona2ItensNextEmptyRetorna2(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "item1" ) );
        inventario.adicionar( new Item( 1, "item2" ) );
        assertEquals( 2, inventario.getTamanho() );
    }
    */
    
    @Test
    public void iniciaInventarioCom20Posicoes(){
        Inventario inventario = new Inventario(20);
        assertEquals( 20, inventario.getCapacidade() );
    }
    
    @Test
    public void retornaDescricaoMachadoLanca() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );   
        inventario.adicionar( new Item( 1, "Lanca" ) );
        assertEquals("Machado,Lanca", inventario.getDescricoesItems());
    }
    
    @Test
    public void cria20Flecha1Arco3FacaRetornaFlecha() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar(new Item( 1, "Arco" ) );
        inventario.adicionar(new Item( 20, "Flecha" ) );
        inventario.adicionar(new Item( 3, "Faca" ) );
        assertEquals( "Flecha", inventario.getMaiorQuantidade().getDescricao() );
    }
    
    @Test
    public void ocupaInventario2ItensRetorna18Livre(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar(new Item( 1, "Arco" ) );
        inventario.adicionar(new Item( 20, "Flecha" ) );
        assertEquals( 18, inventario.getEspacoLivre() );
    }
    
    /* TESTE LEGADO
    @Test
    public void adiciona3ItensRemove1ProxLivre2(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item(1, "Arco" ) );
        inventario.adicionar( new Item(20, "Flecha" ) );
        inventario.adicionar( new Item(1, "Faca" ) );
        inventario.remover(2);
        assertEquals( 2, inventario.getNextEmpty() );
    }
    */ 
    
    /* TESTE LEGADO
    @Test
    public void adiciona3ItensRemoveMeioProxLivre1(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Arco" ) );
        inventario.adicionar( new Item( 20, "Flecha" ) );
        inventario.adicionar( new Item( 1, "Faca" ) );
        inventario.remover(1);
        assertEquals( 1, inventario.getNextEmpty() );
    }
    */

    @Test
    public void buscaEscudoRetornaObj(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 20, "Escudo" ) );
        inventario.adicionar( new Item( 1, "Faca" ) );
        assertEquals( "Escudo", inventario.buscarItem("Escudo").getDescricao() );
    }
    
    @Test
    public void inverterInventarioRetornaEscudoMachado(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 20, "Escudo" ) );
        ArrayList<Item> listaInvertida = inventario.inverter();
        assertEquals( "Escudo", listaInvertida.get(0).getDescricao() );
        assertEquals( "Machado", listaInvertida.get(1).getDescricao() );
    }
    
    @Test
    public void inverterInventarioNaoAlteraSeqMachadoEscudo(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 20, "Escudo" ) );
        ArrayList<Item> listaInvertida = inventario.inverter();
        assertEquals( "Machado", inventario.obter(0).getDescricao() );
        assertEquals( "Escudo", inventario.obter(1).getDescricao() );
    }
    
    @Test
    public void ordenarInventarioEscudoBraceleteEspadaPocao() {
        Inventario inventario = new Inventario(4);
        inventario.adicionar(new Item(3, "Espada"));
        inventario.adicionar(new Item(1, "Escudo de metal"));
        inventario.adicionar(new Item(4, "Poçao de HP"));
        inventario.adicionar(new Item(2, "Bracelete"));
        inventario.ordenar();
        
        assertEquals( "Escudo de metal", inventario.getDescricaoItem(0) );
        assertEquals( "Bracelete", inventario.getDescricaoItem(1) );
        assertEquals( "Espada", inventario.getDescricaoItem(2) );
        assertEquals( "Poçao de HP", inventario.getDescricaoItem(3) );
    }
    
    @Test
    public void ordenarDescInventarioPocaoEspadaBraceleteEscudo() {
        Inventario inventario = new Inventario(4);
        inventario.adicionar(new Item(3, "Espada"));
        inventario.adicionar(new Item(1, "Escudo de metal"));
        inventario.adicionar(new Item(4, "Poçao de HP"));
        inventario.adicionar(new Item(2, "Bracelete"));
        inventario.ordenar(TipoOrdenacao.DESC);
        
        assertEquals( "Poçao de HP", inventario.getDescricaoItem(0) );
        assertEquals( "Espada", inventario.getDescricaoItem(1) );
        assertEquals( "Bracelete", inventario.getDescricaoItem(2) );
        assertEquals( "Escudo de metal", inventario.getDescricaoItem(3) );
    }
}
