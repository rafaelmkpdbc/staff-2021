public class Personagem {
    protected String nome;
    protected int experiencia;
    protected int expPorAtk;
    protected Inventario inventario;
    protected Status status;
    protected double vida;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario(20);
        this.experiencia = 0;
        this.expPorAtk = 1;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    protected void aumentarXP(){
        this.experiencia += expPorAtk;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
     public double getVida() {
       return this.vida;
    }
    
    public Inventario getInventario() {
       return this.inventario;
    }
   
    public Status getStatus(){
       return this.status;
    }
    
    public void ganharItem(Item item) {
        this.inventario.adicionar(item);
    }
    
    //TODO: alterar metodo com sempre existente
    public void perderItem(Item item) {
        this.inventario.remover(item);
    }
    
    private boolean podeSofrerDano(){
       return this.status != Status.MORTO;
    }
   
    protected void sofrerDano( ) {
       if( !this.podeSofrerDano() ) return;    
       this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
       this.status = this.vida > 0 ? Status.SOFREU_DANO : Status.MORTO;
    }
}
