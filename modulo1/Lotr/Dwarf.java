public class Dwarf extends Personagem {
   private boolean escudoEquipado;
    
   {
       this.inventario = new Inventario(20);
   }
   
   public Dwarf( String nome ) {
       super(nome);
       super.vida = 110.0;
       this.inventario.adicionar( new Item( 1, "Escudo" ) );
       this.escudoEquipado = false;
       super.qtdDano = 10.0;
   }
  
   public void equiparDesequiparEscudo() {
       this.escudoEquipado = !this.escudoEquipado;
       super.qtdDano = this.escudoEquipado ? 5.0 : 10.0;
   }
}