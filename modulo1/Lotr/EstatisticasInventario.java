import java.util.*;

public class EstatisticasInventario {
    private Inventario inventario;

    public Inventario getInventario(){
        return this.inventario;
    }
    
    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    private boolean ehVazio(){
        return this.inventario.getItems().isEmpty();
    }
    
    public double calcularMedia() {
        if( this.ehVazio() ) {
            return Double.NaN;
        }
        
        int soma = 0;
        for( Item item : this.inventario.getItems() ) {
            soma += item.getQuantidade();
        }

        return (double)soma / this.inventario.getItems().size();
    }
    
    private ArrayList<Item> ordernarInventario(ArrayList<Item> itens){
        ArrayList<Item> itensOrdenados = new ArrayList<>(itens);
        Item temp;
        for( int i = 0; i < itensOrdenados.size(); i++ ){
            for( int j = 1; j < (itensOrdenados.size() - i); j++ ) {
                if( itensOrdenados.get(j-1).getQuantidade() > itensOrdenados.get(j).getQuantidade() ) {
                    temp = itensOrdenados.get(j-1);
                    itensOrdenados.set( j-1, itensOrdenados.get(j) );
                    itensOrdenados.set( j, temp );
                }
            }
        }
        
        return itensOrdenados;
    }
    
    public double calcularMediana(){
        if( this.ehVazio() ) {
            return Double.NaN;
        }
        
        ArrayList<Item> itensOrdenados = ordernarInventario(this.inventario.getItems());
        double mediana;
        int posicaoMedia = 0;

        if( itensOrdenados.size() % 2 == 0 ){
            posicaoMedia = itensOrdenados.size() / 2;
            mediana = ( itensOrdenados.get(posicaoMedia-1).getQuantidade() + itensOrdenados.get(posicaoMedia).getQuantidade() ) / 2.0;
        } else {
            posicaoMedia = itensOrdenados.size() / 2 ; 
            mediana = itensOrdenados.get(posicaoMedia).getQuantidade();
        } 
        
        return mediana;
    }
    
    public int qtdItensAcimaDaMedia() {       
        double media = this.calcularMedia();
        if( Double.isNaN( media ) ) return 0;
        int qtd = 0;
        for( Item item : this.inventario.getItems() ) {
            if( item.getQuantidade() > media ) qtd++;
        }    
        return qtd;
    }
}
