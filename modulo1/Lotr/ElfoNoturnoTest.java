import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    @Test
    public void atira1xPerde15VidaGanha3Xp() {
        ElfoNoturno legolas = new ElfoNoturno( "Legolas" );
        Dwarf anao = new Dwarf( "anao" );
        legolas.atirarFlecha(anao);
        assertEquals( 3, legolas.getExperiencia() );
        assertEquals( 85.0, legolas.getVida(), 0 );
    }
}
