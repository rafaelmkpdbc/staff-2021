import java.util.*;

public class ElfoVerde extends Elfo {
    private final ArrayList<String> DESCRICOES_PERMITIDAS = new ArrayList<>(
        Arrays.asList(    
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de Vidro"
        )
    );
    
    public ElfoVerde( String nome ) {
        super(nome);
        super.expPorAtk = 2;
    }
    
    protected void aumentarXP() {
        this.experiencia += 2;
    }
    
    private boolean isDescricaoValida( String descricao ) {
        return DESCRICOES_PERMITIDAS.contains( descricao );
    }
    
    public void ganharItem( Item item ) {
        if( isDescricaoValida( item.getDescricao() ) ) {
            super.inventario.adicionar( item );
        }
    }
    
    public void perderItem( Item item ) {
        if( isDescricaoValida( item.getDescricao() ) ) {
            super.inventario.remover( item );
        }
    }
}
