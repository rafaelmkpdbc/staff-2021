import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoTest {
    @Test
    public void alistarElfoVerdeNoturnoPermitido() {
        ElfoVerde verde = new ElfoVerde( "legolas" );
        ElfoNoturno noturno = new ElfoNoturno( "legolas2" );
        Exercito exercito = new Exercito();
        exercito.alistarElfo(verde);
        exercito.alistarElfo(noturno);
        assertEquals( verde, exercito.getElfoAlistado(0) );
        assertEquals( noturno, exercito.getElfoAlistado(1) );
    }
    
    @Test
    public void alistarElfoDaLuzNaoPermitido() {
        ElfoVerde verde = new ElfoVerde( "legolas" );
        ElfoDaLuz daluz = new ElfoDaLuz( "legolas1" );
        ElfoNoturno noturno = new ElfoNoturno( "legolas2" );
        Exercito exercito = new Exercito();
        exercito.alistarElfo(daluz);
        exercito.alistarElfo(verde);
        exercito.alistarElfo(noturno);
        assertNotEquals( daluz, exercito.getElfoAlistado(0) );
        assertEquals( verde, exercito.getElfoAlistado(0) );
        assertEquals( noturno, exercito.getElfoAlistado(1) );
    }
    
    @Test
    public void buscarListaStatusRecemCriado() {
        ElfoVerde verde = new ElfoVerde( "legolas" );
        ElfoNoturno noturno = new ElfoNoturno( "legolas1" );
        ElfoVerde verdao = new ElfoVerde( "legolas2" );
        ElfoNoturno legolas = new ElfoNoturno( "legolas2" );
        Dwarf anao = new Dwarf( "alvo" );
        noturno.atirarFlecha(anao);
        Exercito exercito = new Exercito();
        exercito.alistarElfo(verde);
        exercito.alistarElfo(noturno);
        exercito.alistarElfo(verdao);
        exercito.alistarElfo(legolas);
        ArrayList<Elfo> elfosRecemCriado = exercito.buscarPorStatus(Status.RECEM_CRIADO);
        
        assertEquals( 3, elfosRecemCriado.size() );
        assertEquals( verde, elfosRecemCriado.get(0) );
        assertEquals( verdao, elfosRecemCriado.get(1) );
        assertEquals( legolas, elfosRecemCriado.get(2) );
    }
}
