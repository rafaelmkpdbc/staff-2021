import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void Machado1Escudo1Faca2Arco1Flecha10Media3(){
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 1, "Escudo" ) );
        inventario.adicionar( new Item( 2, "Faca" ) );
        inventario.adicionar( new Item( 1, "Arco" ) );
        inventario.adicionar( new Item( 10, "Flecha") );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals( 3.0, estatisticas.calcularMedia(), 0 );
    }
    
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario(20);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double media = estatisticas.calcularMedia();
        
        assertTrue( Double.isNaN( media ));
    }
    
    @Test
    public void calcularMedianaInventarioVazio(){
        Inventario inventario = new Inventario(20);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double mediana = estatisticas.calcularMediana();
        
        assertTrue( Double.isNaN( mediana ));
    }
    
    @Test
    public void calcularMedianaInventarioImpar() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 2, "Faca" ) );
        inventario.adicionar( new Item( 1, "Escudo" ) );
        inventario.adicionar( new Item( 4, "Flecha" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals( 2, estatisticas.calcularMediana(), 0 );
    }
    
    @Test
    public void Faca2Escudo1Flecha4Arco1Mediana1eMeio() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 2, "Faca" ) );
        inventario.adicionar( new Item( 1, "Escudo" ) );
        inventario.adicionar( new Item( 4, "Flecha" ) );
        inventario.adicionar( new Item( 1, "Arco" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals( 1.5, estatisticas.calcularMediana(), 0 );
    }
    
    @Test
    public void qtdItensAcimaMediaInventarioVazio() {
        Inventario inventario = new Inventario(20);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        int qtd = estatisticas.qtdItensAcimaDaMedia();    
        assertEquals( 0, qtd );
    }
    
    @Test
    public void Machado1Escudo1Faca2Arco1Flecha10AcimaMedia1() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 1, "Escudo" ) );
        inventario.adicionar( new Item( 2, "Faca" ) );
        inventario.adicionar( new Item( 1, "Arco" ) );
        inventario.adicionar( new Item( 10, "Flecha") );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals( 1, estatisticas.qtdItensAcimaDaMedia() );
    }
    
    @Test
    public void qtdItensAcimaMediaInventarioTodosIguaisMedia() {
        Inventario inventario = new Inventario(20);
        inventario.adicionar( new Item( 1, "Machado" ) );
        inventario.adicionar( new Item( 1, "Escudo" ) );
        inventario.adicionar( new Item( 1, "Faca" ) );
        inventario.adicionar( new Item( 1, "Arco" ) );
        inventario.adicionar( new Item( 1, "Flecha") );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals( 0, estatisticas.qtdItensAcimaDaMedia() );
    }
}
