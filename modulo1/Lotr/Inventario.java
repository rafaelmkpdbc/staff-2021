import java.util.*;

public class Inventario {
    private ArrayList<Item> itens;
    private final int capacidade;
    
    public Inventario( int capacidade ) {
        this.itens = new ArrayList<>(capacidade);
        this.capacidade = capacidade;
    }
    
    public ArrayList<Item> getItems() {
        return this.itens;
    }
    
    private boolean validaPosicao(int posicao) {
        return posicao < this.itens.size();
    }
    
    public void adicionar( Item item ) {
        if( this.itens.size() < this.capacidade ) this.itens.add(item);
    }
    
    public Item obter( int posicao ) {
        return validaPosicao(posicao) ? this.itens.get(posicao) : null;
    }
    
    public Item buscarItem(String descricao){
        for(Item item : itens ) {
            if( descricao.equals(item.getDescricao()) ) return item;
        }
        
        return null;
    }
    
    public void remover( int posicao ) {
        if( validaPosicao( posicao ) ) this.itens.remove(posicao);
    }
    
    public void remover( Item item ) {
        this.itens.remove(item);
    }
    
    public int getCapacidade() {
        return this.capacidade;
    }
    
    public int getEspacoLivre() {
        return this.capacidade - this.itens.size();
    }
    
    public String getDescricaoItem( int posicao ) {
        return this.itens.get(posicao).getDescricao();
    }
    
    public String getDescricoesItems() {
        StringBuilder descricoes = new StringBuilder("");
        for( Item item : itens ){
            descricoes.append( item.getDescricao() );
            descricoes.append(",");
        }
        if( descricoes.length() > 0 ) descricoes.deleteCharAt( descricoes.length() - 1 );
        
        return descricoes.toString();
    }
    
    public Item getMaiorQuantidade() {
        Item maior = 
            this.itens.get(0) != null ? 
            this.itens.get(0) : null;
        for( Item item : itens ) {
            if( item.getQuantidade() > maior.getQuantidade() ) maior = item;
        }
        
        return maior;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> invertido = new ArrayList<>();
        for( int i = itens.size() - 1; i >= 0; i-- ) {
            invertido.add( itens.get(i) );
        } 
        
        return invertido;
    }
    
    public void ordenar() {
        Item temp;
        for( int i = 0; i < this.itens.size(); i++ ){
            for( int j = 1; j < (this.itens.size() - i); j++ ) {
                if( this.itens.get(j-1).getQuantidade() > this.itens.get(j).getQuantidade() ) {
                    temp = this.itens.get(j-1);
                    this.itens.set( j-1, this.itens.get(j) );
                    this.itens.set( j, temp );
                }
            }
        }
    }
    
    public void ordenar(TipoOrdenacao ordenacao) {
        Item temp;
        if(ordenacao == TipoOrdenacao.ASC) {
            this.ordenar();
            return;
        }
        for( int i = 0; i < this.itens.size(); i++ ){
            for( int j = 1; j < (this.itens.size() - i); j++ ) {
                if( this.itens.get(j-1).getQuantidade() < this.itens.get(j).getQuantidade() ) {
                    temp = this.itens.get(j-1);
                    this.itens.set( j-1, this.itens.get(j) );
                    this.itens.set( j, temp );
                }
            }
        }
    }
}
