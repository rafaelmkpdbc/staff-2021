import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void testarContadorDeInstancias() {
        int contadorAntesDoTeste = Elfo.getContadorInstancia();
        ElfoVerde verde = new ElfoVerde( "legolas" );
        ElfoDaLuz daluz = new ElfoDaLuz( "legolas1" );
        ElfoNoturno noturno = new ElfoNoturno( "legolas2" );
        Elfo elfo = new Elfo ( "elfo" );
        int contadorAposTeste = contadorAntesDoTeste + 4;
        assertEquals( contadorAposTeste, Elfo.getContadorInstancia() );
    }
    
    @Test
    public void exemploMudancaValoresItem() {
        Elfo elfo = new Elfo("teste");
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals(1000, flecha.getQuantidade());
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfo = new Elfo("teste");
        assertEquals(2, elfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() { 
        Elfo elfo = new Elfo("teste");
        Dwarf anao = new Dwarf("anao");
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(1, elfo.getExperiencia());
    }
    
    @Test
    public void elfoAtiraFlecha3xGanha2XP() {
        Elfo elfo = new Elfo("teste");
        Dwarf anao = new Dwarf("anao");
        for(int i = 1; i <= 3; i++) {
            elfo.atirarFlecha(anao);
        }
        assertEquals(0, elfo.getFlecha().getQuantidade());
        assertEquals(2, elfo.getExperiencia());
    }
    
    @Test
    public void elfoAtiraDwarfPerdeVida() {
        //Se o metodo atiraFlecha recebe destino como parametro, com o tipo correto
        //nao se faz necessario testar tipo do destino
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao= new Dwarf("Gimli");
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(100.0, anao.getVida(), 0);
    }
}
