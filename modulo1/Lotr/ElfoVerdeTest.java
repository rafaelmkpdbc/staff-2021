import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    @Test
    public void ataca1xGanha2Xp() {
        ElfoVerde legolas = new ElfoVerde( "Legolas" );
        Dwarf anao = new Dwarf( "anao" );
        legolas.atirarFlecha(anao);
        assertEquals( 2, legolas.getExperiencia() );
    }
    
    @Test
    public void adicionaArcoDeVidroInventario() {
        ElfoVerde legolas = new ElfoVerde( "Legolas" );
        Item escudo = new Item( 1, "Arco de Vidro" );
        legolas.getInventario().adicionar(escudo);
        
    }
    
    @Test 
    public void adicionaEscudoInventarioNPermitido() {
        ElfoVerde legolas = new ElfoVerde( "Legolas" );
        Item escudo = new Item( 1, "escudo" );
        legolas.getInventario().adicionar(escudo);
        
    }
}
