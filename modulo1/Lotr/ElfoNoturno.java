public class ElfoNoturno extends Elfo {
    public ElfoNoturno( String nome ) {
        super(nome);
        super.expPorAtk = 3;
        super.qtdDano = 15.0;
    }
    
    public void atirarFlecha( Dwarf dwarf ){        
        //verifica se ha flechas para atirar
        super.atirarFlecha(dwarf);
    } 
}
