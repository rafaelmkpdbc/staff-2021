import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest {
    @Test
    public void add4ItensRetorna2Ultimos(){
        Inventario inventario = new Inventario(4);
        inventario.adicionar(new Item(1, "Espada"));
        inventario.adicionar(new Item(2, "Escudo de metal"));
        inventario.adicionar(new Item(3, "Poçao de HP"));
        inventario.adicionar(new Item(4, "Bracelete"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> pagina = new ArrayList<>();
        paginador.pular(2);
        pagina = paginador.limitar(2);
        
        assertEquals( "Poçao de HP", pagina.get(0).getDescricao() );
        assertEquals( "Bracelete", pagina.get(1).getDescricao() );
    }
    
    @Test
    public void add4ItensRetorna2Primeiros(){
        Inventario inventario = new Inventario(4);
        inventario.adicionar(new Item(1, "Espada"));
        inventario.adicionar(new Item(2, "Escudo de metal"));
        inventario.adicionar(new Item(3, "Poção de HP"));
        inventario.adicionar(new Item(4, "Bracelete"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> pagina = new ArrayList<>();
        paginador.pular(0);
        pagina = paginador.limitar(2);
        assertEquals( "Espada", pagina.get(0).getDescricao() );
        assertEquals( "Escudo de metal", pagina.get(1).getDescricao() );
    }
}
