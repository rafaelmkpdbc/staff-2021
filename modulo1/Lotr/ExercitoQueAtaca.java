import java.util.*;

public class ExercitoQueAtaca  extends Exercito {
    private EstrategiaDeAtaque estrategia;
    
    public ExercitoQueAtaca( EstrategiaDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia( EstrategiaDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }
    
    public void atacar ( ArrayList<Dwarf> anoes ) {
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque( super.getElfos() );
        for( Elfo elfo : ordem ) {
            for( Dwarf anao : anoes ) {
                elfo.atirarFlecha(anao);
            }
        }
    }
}
