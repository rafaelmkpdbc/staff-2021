import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @Test
    public void dwarfDeveNascerCom110Vida() {
        Dwarf dwarf = new Dwarf("anao");
        assertEquals( 110.0, dwarf.getVida(), 0 );        
    }
    
    @Test
    public void dwarfSofreDano12xEFicaCom0Vida() {
        Dwarf dwarf = new Dwarf("anao");
        for( int i = 0; i < 12; i++ ) {
            dwarf.sofrerDano();
        }
        assertEquals( 0.0, dwarf.getVida(), 0 );        
    }
    
    @Test
    public void dwarfSofre120DanoFicaStatusMortoVidaFica0() {
        Dwarf dwarf = new Dwarf( "anao" );
        for( int i = 0; i < 12; i++ ) {
            dwarf.sofrerDano();
        }
        assertEquals( Status.MORTO, dwarf.getStatus() );
        assertEquals( 0.0, dwarf.getVida(), 0 );
    }
    
    @Test
    public void dwarfEscudoEquipadoMetadeDano() {
        Dwarf dwarf = new Dwarf("anao");
        dwarf.equiparDesequiparEscudo();
        dwarf.sofrerDano();
        assertEquals( 105.0, dwarf.getVida(), 0 );
    }
    
    @Test
    public void dwarfEscudoNEquipadoDanoCheio() {
        Dwarf dwarf = new Dwarf("anao");
        dwarf.sofrerDano();
        assertEquals( 100.0, dwarf.getVida(), 0 );
    }
    
    @Test
    public void anaoNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        Item esperado = new Item( 1, "Escudo" );
        Item resultado = dwarf.getInventario().obter(0);
        assertEquals( esperado, resultado );
    }
}
