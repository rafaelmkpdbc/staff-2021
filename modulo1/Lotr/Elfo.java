public class Elfo extends Personagem {
    private static int contadorInstancia = 0;
    
    {
        Elfo.contadorInstancia = 0;
    }
    
    public Elfo( String nome ) {
        super(nome);
        Elfo.contadorInstancia++;
        this.vida = 100.0;
        this.inventario.adicionar(new Item( 1, "Arco" ) );
        this.inventario.adicionar(new Item( 2, "Flecha" ) );
    }
    
    public static int getContadorInstancia() {
        return Elfo.contadorInstancia;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(1);   
    }
    
    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha( Dwarf dwarf ){        
        //verifica se ha flechas para atirar
        if( podeAtirar() ) {
            int qtdFlecha = this.getQtdFlecha();
            this.getFlecha().setQuantidade( qtdFlecha - 1 );
            super.sofrerDano();
            dwarf.sofrerDano();
            this.aumentarXP();
        }
    }
}