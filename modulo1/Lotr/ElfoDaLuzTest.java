import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @Test
    public void ataqueImparPerdeVidaAtaqueParGanha() {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        feanor.atacarComEspada(new Dwarf( "Farlum" )); 
        double vidaAtk1 = feanor.getVida(); 
        feanor.atacarComEspada(new Dwarf( "Gul" )); 
        double vidaAtk2 = feanor.getVida();
        assertEquals( 79.0, vidaAtk1, 0 );
        assertEquals( 89.0, vidaAtk2, 0 );
    }
    
    @Test
    public void ganhar1XpPorAtk() {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        feanor.atacarComEspada(new Dwarf( "Farlum" ));
        feanor.atacarComEspada(new Dwarf( "Gul" ));
        assertEquals( 2, feanor.getExperiencia() );
    }
    
    @Test
    public void ElfoDaLuzNasceComArcoEFlecha() {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        Item arco = new Item( 1, "Arco" );
        Item flecha = new Item( 2, "Flecha" );
        assertEquals( arco, feanor.getInventario().getItems().get(0) );
        assertEquals( flecha, feanor.getInventario().getItems().get(1) );
    }
}
