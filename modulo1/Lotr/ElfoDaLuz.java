import java.util.*;
public class ElfoDaLuz extends Elfo {
    private boolean ehAtkImpar;
    private final ArrayList<String> ITENS_PERMANENTES = new ArrayList<>(
        Arrays.asList(    
            "Espada de Galvorn"
        )
    );
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        this.ehAtkImpar = true;
        this.qtdDano = 21.0;
        this.inventario.adicionar( new Item( 1, ITENS_PERMANENTES.get(0) ) );
    }
    
    //TODO: usar metodo sempre existente
    public void perderItem( Item item ) {
        if( !item.getDescricao().equals( ITENS_PERMANENTES.get(0) ) )
            super.perderItem(item);
    }
    
    public void recuperarVida() {
        super.vida += 10.0;
    }
    
    
    public void atacarComEspada( Dwarf dwarf ) {
        if(this.ehAtkImpar) 
            super.sofrerDano(); 
        else
            this.recuperarVida();
        dwarf.sofrerDano();
        this.aumentarXP();
        this.ehAtkImpar = !this.ehAtkImpar;
    }
}
