import java.util.*;

public class Exercito {
   private ArrayList<Elfo> exercito;
   private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
   private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(    
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
   public Exercito() {
       this.exercito = new ArrayList<>();
   }

   private boolean podeAlistarElfo(Elfo elfo ) {
       return TIPOS_PERMITIDOS.contains( elfo.getClass() );
   }
    
   public void alistarElfo( Elfo elfo ) {
       if( this.podeAlistarElfo(elfo) ) this.exercito.add(elfo);
       ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
       if( elfoDeUmStatus == null ) {
           elfoDeUmStatus = new ArrayList<>();
           porStatus.put( elfo.getStatus(), elfoDeUmStatus );
       }
       elfoDeUmStatus.add(elfo);
   }
    
   public ArrayList<Elfo> buscarPorStatus( Status status ) {
       return this.porStatus.get(status);
   }
   
   public ArrayList<Elfo> buscarExcluindoUmStatus( Status status ) {
       ArrayList<Elfo> devolver = this.exercito;
       ArrayList<Elfo> tirar = this.porStatus.get( status );
       devolver.removeAll( tirar );
       return devolver;
   }
   
   public Elfo getElfoAlistado( int posicao ){
       if( posicao < this.exercito.size() ) { 
           return this.exercito.get(posicao);
       }
       return new Elfo( "null" );
   }
   
   public ArrayList<Elfo> getElfos() {
       return this.exercito;
   }
}
