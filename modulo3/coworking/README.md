# Coworking

API voltada para gestão de ambiente de coworking.   

[Ir para aplicação de front-end](../../frontend-final/react-coworking)

## Instalação

Crie um repositório local e clone este repositório:
```
git init 
git clone https://rafaelmkpdbc@bitbucket.org/rafaelmkpdbc/staff-2021.git
```

## Execução   

Acesse a pasta do projeto:
```
cd coworking
```

Na pasta do projeto, cria uma imagem docker a partir do [Dockerfile](Dockerfile).
```
docker build .
```

Com a imagem criada, crie um conteiner a partir dela:
```
docker run -p 8080:8080 --name coworking [ID_IMAGEM]
```

A aplicação será iniciada conectando a porta 8080 local à porta 8080 do conteiner.