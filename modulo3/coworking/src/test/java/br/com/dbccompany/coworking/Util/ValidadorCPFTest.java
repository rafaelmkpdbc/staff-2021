package br.com.dbccompany.coworking.Util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidadorCPFTest {

    @Test
    public void cpf12345678912RetornaFalso() {
        String cpfInvalido = "12345678912";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }

    @Test
    public void cpf11111111111RetornaFalso() {
        String cpfInvalido = "11111111111";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }

    @Test
    public void cpf11122233345RetornaFalso() {
        String cpfInvalido = "11122233345";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }

    @Test
    public void cpf22222222222RetornaFalso() {
        String cpfInvalido = "22222222222";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }

    @Test
    public void cpf88888888888RetornaFalso() {
        String cpfInvalido = "88888888888";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }

    @Test
    public void cpf99999999999RetornaFalso() {
        String cpfInvalido = "99999999999";
        assertFalse( ValidadorCPF.ehCPFValido( cpfInvalido ) );
    }


    @Test
    public void cpf12517473960RetornaTrue() {
        String cpfValido = "12517473960";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf12697278265RetornaTrue() {
        String cpfValido = "12697278265";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf62373460408RetornaTrue3() {
        String cpfValido = "62373460408";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf57048827220RetornaTrue4() {
        String cpfValido = "57048827220";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf87521261704RetornaTrue() {
        String cpfValido = "87521261704";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf13996691688RetornaTrue() {
        String cpfValido = "13996691688";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf60567810461RetornaTrue() {
        String cpfValido = "60567810461";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf08512964758RetornaTrue() {
        String cpfValido = "08512964758";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf89507254528RetornaTrue() {
        String cpfValido = "89507254528";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

    @Test
    public void cpf75828861352RetornaTrue() {
        String cpfValido = "75828861352";
        assertTrue( ValidadorCPF.ehCPFValido( cpfValido ) );
    }

}
