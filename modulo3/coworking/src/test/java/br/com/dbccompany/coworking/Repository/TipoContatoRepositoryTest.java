package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
//@Profile( "test" )
public class TipoContatoRepositoryTest {
    @Autowired
    TipoContatoRepository repository;

    @Test
    public void salvarTipoContato() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome( "Telefone" );
        repository.save( tipoContato );
        assertEquals( tipoContato.getNome(), repository.findByNome( "Telefone" ).getNome() );
    }

    @Test
    public void tipoNaoExiste() {
        String nome = "Email";
        assertNull( repository.findByNome( nome ) );
    }

    @Test
    public void buscarTodosRetornarDoisTipos() {
        TipoContatoEntity email = new TipoContatoEntity();
        email.setNome( "email" );
        repository.save( email );
        TipoContatoEntity telefone = new TipoContatoEntity();
        telefone.setNome( "telefone" );
        repository.save( telefone );
        assertEquals( 2, repository.findAll().size() );
    }
}
