package br.com.dbccompany.coworking.Controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;
import java.sql.Date;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ClienteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private String JWT;

    @BeforeEach
    public void setup() {
        JWT = Jwts.builder()
                .setSubject( "teste" )
                .setExpiration( new Date( System.currentTimeMillis() + 860_000_000 ) )
                .signWith( SignatureAlgorithm.HS512, "secret" )
                .compact();
    }

    @Test
    public void retornar200QuandoConsultarItem() throws Exception {
        URI uri = new URI( "/coworking/cliente/" );

        mockMvc.
                perform( MockMvcRequestBuilders
                        .get( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer " + JWT )
                ).andExpect( MockMvcResultMatchers
                    .status()
                    .is(200)
                );
    }

    @Test
    public void salvarCliente() throws Exception {
        URI uri = new URI( "/coworking/cliente/salvar" );

        mockMvc
                .perform( MockMvcRequestBuilders
                    .post( uri )
                    .contentType( MediaType.APPLICATION_JSON )
                    .header( "Authorization", "Bearer " + JWT )
                    .content( "{ \"nome\": \"Fulano\", " +
                                "\"cpf\": \"12517473960\", " +
                                "\"dataNascimento\": \"11/10/1990\", " +
                                "\"contato\": [ " +
                            " { \"tipoContato\": \"telefone\", \"valor\": \"123456789\" }," +
                            " { \"tipoContato\": \"email\", \"valor\": \"mail@mail.com\" } ] }" ))
                    .andExpect( MockMvcResultMatchers.status().isOk() )
                    .andExpect( MockMvcResultMatchers.content().contentType( MediaType.APPLICATION_JSON ) )
                    .andExpect( MockMvcResultMatchers.jsonPath( "$.id" ).value( 1 ) );
    }

    @Test
    public void retornar400SalvarSemCamposObrigatorios() throws Exception {
        URI uri = new URI( "/coworking/cliente/salvar" );
        String nome = "\"nome\": \"Fulano\"";
        String cpf = "\"cpf\": \"12517473960\"";
        String dataNascimento = "\"dataNascimento\": \"11/10/1990\"";
        String contatos = "\"contato\": [ " +
                " { \"tipoContato\": \"telefone\", \"valor\": \"123456789\" }, " +
                "{ \"tipoContato\": \"email\", \"valor\": \"mail@mail.com\" } ]";

        mockMvc
                .perform( MockMvcRequestBuilders
                        .post( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer " + JWT )
                        .content( "{ " + cpf + ", " + dataNascimento + ", " + contatos + " }" ) )
                        .andExpect( result -> assertTrue( result.getResolvedException() instanceof ResponseStatusException ) )
                        .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                        .andExpect( MockMvcResultMatchers.status()
                                .reason( "Necessário informar nome, CPF e data de nascimento!" ) );
        mockMvc
                .perform( MockMvcRequestBuilders
                        .post( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer " + JWT )
                        .content( "{ " + nome + ", " + dataNascimento + ", " + contatos + " }" ) )
                        .andExpect( result -> assertTrue( result.getResolvedException() instanceof ResponseStatusException ) )
                        .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                        .andExpect( MockMvcResultMatchers.status()
                                .reason( "Necessário informar nome, CPF e data de nascimento!" ) );
        mockMvc
                .perform( MockMvcRequestBuilders
                        .post( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer " + JWT )
                        .content( "{ " + nome + ", " + cpf + ", " + contatos + " }" ) )
                        .andExpect( result -> assertTrue( result.getResolvedException() instanceof ResponseStatusException ) )
                        .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                        .andExpect( MockMvcResultMatchers.status()
                                .reason( "Necessário informar nome, CPF e data de nascimento!" ) );
    }

    @Test
    public void retornar400SalvarSemContatosObrigatorios() throws Exception {
        URI uri = new URI( "/coworking/cliente/salvar" );
        String nome = "\"nome\": \"Fulano\"";
        String cpf = "\"cpf\": \"12517473960\"";
        String dataNascimento = "\"dataNascimento\": \"11/10/1990\"";
        String telefone = "{ \"tipoContato\": \"telefone\", \"valor\": \"123456789\" }";
        String email = "{ \"tipoContato\": \"email\", \"valor\": \"mail@mail.com\" }";

        mockMvc
                .perform( MockMvcRequestBuilders
                        .post( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer" + JWT )
                        .content( "{ " + nome + ", " + cpf + ", " + dataNascimento + ", " +
                                "\"contato\": [ " + telefone + "] }" ) )
                        .andExpect( result -> assertTrue( result.getResolvedException() instanceof ResponseStatusException ) )
                        .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                        .andExpect( MockMvcResultMatchers.status()
                                .reason( "Necessário informar no mínimo um email e um telefone para contato." ) );
        mockMvc
                .perform( MockMvcRequestBuilders
                        .post( uri )
                        .contentType( MediaType.APPLICATION_JSON )
                        .header( "Authorization", "Bearer" + JWT )
                        .content( "{ " + nome + ", " + cpf + ", " + dataNascimento + ", " +
                                "\"contato\": [ " + email + "] }" ) )
                        .andExpect( result -> assertTrue( result.getResolvedException() instanceof ResponseStatusException ) )
                        .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                        .andExpect( MockMvcResultMatchers.status()
                                .reason( "Necessário informar no mínimo um email e um telefone para contato." ) );
    }
}
