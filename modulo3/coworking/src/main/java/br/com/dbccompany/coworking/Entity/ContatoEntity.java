package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity extends GenericEntity{
    @Id
    @SequenceGenerator( name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ" )
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "id_tipo" )
    private TipoContatoEntity tipoContato;

    @Column( nullable = false )
    private String valor;

    @ManyToOne
    @JoinColumn( name = "id_cliente" )
    private ClienteEntity cliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato( TipoContatoEntity tipoContato ) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor( String valor ) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente( ClienteEntity cliente ) {
        this.cliente = cliente;
    }
}
