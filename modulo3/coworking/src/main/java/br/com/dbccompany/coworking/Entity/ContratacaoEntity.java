package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "id_cliente" )
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn( name = "id_espaco" )
    private EspacoEntity espaco;

    @Column( nullable = false )
    private TipoContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;
    private Double desconto;

    @Column( nullable = false )
    private Integer prazo;

    @OneToMany( mappedBy = "contratacao" )
    private List<PagamentoEntity> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( TipoContratacaoEnum tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto( Double desconto ) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo( Integer prazo ) {
        this.prazo = prazo;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente( ClienteEntity cliente ) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco( EspacoEntity espaco ) {
        this.espaco = espaco;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento( List<PagamentoEntity> pagamento ) {
        this.pagamento = pagamento;
    }
}
