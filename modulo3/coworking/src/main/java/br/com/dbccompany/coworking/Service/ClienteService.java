package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteArgumentoFaltando;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteCPFInvalidoException;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteException;
import br.com.dbccompany.coworking.Exception.Cliente.FaltaContatoException;
import br.com.dbccompany.coworking.Exception.Pagamento.ArgumentosInvalidosException;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.ClienteFactory;
import br.com.dbccompany.coworking.Util.ContatoFactory;
import br.com.dbccompany.coworking.Util.ValidadorCPF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService extends GenericService<ClienteRepository, ClienteEntity, Integer> {
    @Autowired
    private ClienteRepository cliRepository;
    @Autowired
    private ContatoRepository contRepository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    private ClienteFactory clienteFactory = ClienteFactory.getInstance();
    private ContatoFactory contatoFactory = ContatoFactory.getInstance();

    public List<ClienteDTO> buscarTodosClientes() throws ClienteException {
        try {
            List<ClienteEntity> clientes = super.buscarTodos();
            return clienteFactory.fazerVariosDTO( clientes );
        } catch ( Exception e ) {
            throw new ClienteException( "Não foi possível retornar lista de clientes" );
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteDTO salvar( ClienteDTO dto ) throws FaltaContatoException, ClienteArgumentoFaltando, ClienteCPFInvalidoException {
        validarCampos( dto );
        validarContatos( dto.getContato() );
        ClienteEntity clienteSalvo = super.salvarEEditar( clienteFactory.fazerEntity( dto ) );
        List<ContatoEntity> contatosDoCliente = extrairContatosCliente( dto );
        vincularClienteAContato( contatosDoCliente, clienteSalvo );
        salvarContatosDoCliente( contatosDoCliente );

        return clienteFactory.fazerDTO( clienteSalvo );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteDTO editar( ClienteDTO dto, Integer id ) {
        ClienteEntity cliente = clienteFactory.fazerEntity( dto );
        cliente.setId( id );
        ClienteEntity entidade = super.salvarEEditar( cliente );
        return clienteFactory.fazerDTO( entidade );
    }

    private void validarContatos( List<ContatoDTO> contatos ) throws FaltaContatoException {
        int email = 0;
        int telefone = 0;
        for( ContatoDTO contato : contatos ) {
            if (contato.getTipoContato().equalsIgnoreCase( "Telefone" )) {
                telefone++;
            } else if(contato.getTipoContato().equalsIgnoreCase( "Email" )) {
                email++;
            }
        }
        if( telefone < 1 || email < 1 ) {
            throw new FaltaContatoException();
        }
    }

    private void validarCampos( ClienteDTO dto ) throws ClienteArgumentoFaltando, ClienteCPFInvalidoException {
        this.validarExistenciaDeCampos( dto );
        this.validarCPF( dto );
    }

    private void validarExistenciaDeCampos( ClienteDTO dto )  throws ClienteArgumentoFaltando {
        if( dto.getCpf() == null || dto.getDataNascimento() == null || dto.getNome() == null ) {
            throw new ClienteArgumentoFaltando();
        }
    }

    private void validarCPF( ClienteDTO dto ) throws ClienteCPFInvalidoException {
        if( !ValidadorCPF.ehCPFValido( dto.getCpf() ) ) {
            throw new ClienteCPFInvalidoException();
        }
    }

    private List<ContatoEntity> extrairContatosCliente( ClienteDTO cliente ) {
        List<ContatoEntity> contatosDoCliente = new ArrayList<>();
        for( ContatoDTO contato : cliente.getContato() ) {
            TipoContatoEntity tipoContato = tipoContatoRepository.findByNome( contato.getTipoContato() );
            ContatoEntity entidade = contatoFactory.fazerEntity( contato );
            entidade.setTipoContato( tipoContato );
            contatosDoCliente.add( entidade );
        }
        return contatosDoCliente;
    }
    private void vincularClienteAContato( List<ContatoEntity> contatos, ClienteEntity cliente ) {
        for( ContatoEntity contato : contatos ) {
            contato.setCliente( cliente );
        }
    }
    private void salvarContatosDoCliente( List<ContatoEntity> contatos ) {
        contatos = (List<ContatoEntity>) contRepository.saveAll( contatos );
    }
}
