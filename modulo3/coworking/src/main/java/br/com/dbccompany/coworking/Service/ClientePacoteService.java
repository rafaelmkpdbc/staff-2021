package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Util.ClientePacoteFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientePacoteService extends GenericService<ClientePacoteRepository, ClientePacoteEntity, Integer> {
    @Autowired
    private ClientePacoteRepository repository;

    private ClientePacoteFactory factory = ClientePacoteFactory.getInstance();

    public List<ClientePacoteDTO> buscarTodosClientePacote() {
        List<ClientePacoteEntity> clientesPacotes = super.buscarTodos();
        return factory.fazerVariosDTO( clientesPacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacoteDTO salvar( ClientePacoteDTO dto ) {
        ClientePacoteEntity clientePacote = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( clientePacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacoteDTO editar( ClientePacoteDTO dto, Integer id) {
        dto.setId( id );
        ClientePacoteEntity clientePacote = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( clientePacote );
    }
}
