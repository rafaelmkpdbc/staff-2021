package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Date;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class SaldoClienteDTO {
    private SaldoClienteId id;
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private String tipoContratacao;
    private Integer quantidade;
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId( SaldoClienteId id ) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente( ClienteDTO cliente ) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco( EspacoDTO espaco ) {
        this.espaco = espaco;
    }

    public String getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( String tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento( Date vencimento ) {
        this.vencimento = vencimento;
    }
}
