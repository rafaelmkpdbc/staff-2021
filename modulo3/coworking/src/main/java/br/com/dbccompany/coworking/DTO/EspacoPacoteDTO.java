package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class EspacoPacoteDTO {
    private Integer id;
    private EspacoDTO espaco;
    private PacoteDTO pacote;
    private String tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco( EspacoDTO espaco ) {
        this.espaco = espaco;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote( PacoteDTO pacote ) {
        this.pacote = pacote;
    }

    public String getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( String tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo( Integer prazo ) {
        this.prazo = prazo;
    }
}
