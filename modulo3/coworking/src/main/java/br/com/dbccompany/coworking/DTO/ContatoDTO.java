package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "valor")
public class ContatoDTO {
    private Integer id;
    private String valor;
//    @JsonBackReference
    private String tipoContato;
    private String cliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor( String valor ) {
        this.valor = valor;
    }

    public String getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato( String tipoContato ) {
        this.tipoContato = tipoContato;
    }

    public String getCliente() {
        return this.cliente;
    }

    public void setCliente( String cliente ) {
        this.cliente = cliente;
    }
}
