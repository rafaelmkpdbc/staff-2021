package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    List<ClienteEntity> findAll();
    Optional<ClienteEntity> findById( Integer id );
    List<ClienteEntity> findAllByIdIn( List<Integer> ids );
    ClienteEntity findByNome( String nome );
    List<ClienteEntity> findAllByNome( String nome );
    ClienteEntity findByDataNascimento( Date data );
    List<ClienteEntity> findAllByDataNascimento( Date data );
    ClienteEntity findByContato( ContatoEntity contato );
}

