package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.LogDTO;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteException;
import jdk.internal.net.http.common.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Locale;

public class LogSender {
    private static LogSender factory;
    private static Logger logger = LoggerFactory.getLogger( CoworkingApplication.class );
    private static RestTemplate restTemplate = new RestTemplate();
    private static final String URL = "http://localhost:8081/coworking-log/salvar";
    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

    private LogSender(){}

    public static LogSender getInstance() {
        if( factory == null ){
            factory = new LogSender();
        }
        return factory;
    }

    public void emitirEEnviarErroCliente( ClienteException e ) {
        String currentTime = getCurrentTimeStamp();
        String PID = getPID();
        String tipo = ClienteException.class.toString().toUpperCase( Locale.ROOT );
        String descricao = e.getMensagem();
        LogDTO log = new LogDTO( currentTime, PID, tipo, descricao );
        String fullLog = montarMensagemLog( log );
        logger.error( fullLog );
        this.enviarLog( log );
    }

    private void enviarLog( LogDTO dto ) {
        //Comentado para ambiente de desenvolvimento
        //Codigo deve permanecer em ambiente de producao
//        restTemplate.postForObject( URL, log, Object.class );
    }

    private String getCurrentTimeStamp() {
        return new SimpleDateFormat(TIME_FORMAT).format(System.currentTimeMillis());
    }

    private String getPID() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        String rawName = rt.getName();
        return rawName.split( "@" )[0];
    }

    private String montarMensagemLog( LogDTO log ) {
        StringBuffer texto = new StringBuffer();
        texto.append( log.getTimeStamp() ).append( " " );
        texto.append( log.getProcesso() ).append( " " );
        texto.append( log.getTipo() ).append( " " );
        texto.append( log.getDescricao() ).append( " " );

        return texto.toString();
    }
}
