package br.com.dbccompany.coworking.Util;

import java.util.Arrays;

public class ValidadorCPF {
    private static final int CPF_TAMANHO = 11;
    private static final String[] PADROES_INVALIDOS = {
        "11111111111",
        "22222222222",
        "33333333333",
        "44444444444",
        "55555555555",
        "66666666666",
        "77777777777",
        "88888888888",
        "99999999999",
        "00000000000",
    };

    public static boolean ehCPFValido( String cpf ) {
        if(ehPadraoInvalido( cpf )) {
            return false;
        }
        int[] numerosDoCPF = parsearParaArrayInt( cpf );
        int validadorDigito1 = calcularValidadorDigito( Arrays.copyOf( numerosDoCPF, 9 ) );
        int validadorDigito2 = calcularValidadorDigito( Arrays.copyOf( numerosDoCPF, 10 ) );

        return ( ehDigitoValido( validadorDigito1, numerosDoCPF[9] ) &&
                 ehDigitoValido( validadorDigito2, numerosDoCPF[10] ) );
    }

    private static boolean ehPadraoInvalido( String cpf ) {
        for( String padroInvalido : PADROES_INVALIDOS ) {
            if(cpf.equalsIgnoreCase( padroInvalido )) {
               return true;
            }
        }
        return false;
    }

    private static int[] parsearParaArrayInt( String cpf ) {
        int[] values = new int[11];
        char[] cpfToChar = cpf.toCharArray();
        for( int i = 0; i < cpfToChar.length; i++ ) {
            values[i] = Integer.parseInt( String.valueOf( cpfToChar[i] ) );
        }
        return values;
    }

    private static int calcularValidadorDigito( int[] numerosCPF ) {
        int pesoDigito = 2;
        int somaTotalPrimeiroDigito = 0;
        for( int i = numerosCPF.length -1 ; i >= 0 ; i-- ) {
            somaTotalPrimeiroDigito += numerosCPF[i] * pesoDigito;
            pesoDigito++;
        }
        return somaTotalPrimeiroDigito % CPF_TAMANHO;
    }

    private static boolean ehDigitoValido( int restoDivisao, int digito ) {
        if( CPF_TAMANHO - restoDivisao < 9 ) {
            return ( CPF_TAMANHO - restoDivisao == digito );
        } else {
            return ( digito == 0 );
        }
    }
}
