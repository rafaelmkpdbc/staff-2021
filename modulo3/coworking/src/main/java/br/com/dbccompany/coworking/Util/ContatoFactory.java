package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class ContatoFactory extends AbsFactoryModelMapper {
    private static ContatoFactory factory;

    private ContatoFactory(){}

    public static ContatoFactory getInstance() {
        if( factory == null ) {
            factory = new ContatoFactory();
        }
        return factory;
    }

    public ContatoDTO fazerDTO( ContatoEntity entidade ) {
        return super.modelMapper.map( entidade, ContatoDTO.class );
    }

    public ContatoEntity fazerEntity( ContatoDTO dto ) {
        return super.modelMapper.map( dto, ContatoEntity.class );
    }

    public ArrayList<ContatoDTO> fazerVariosDTO( List<ContatoEntity> entidades ) {
        return super.modelMapper.map( entidades, listContatoDTO );
    }

    public ArrayList<ContatoEntity> fazerVariosEntity( List<ContatoDTO> dtos ) {
        return super.modelMapper.map( dtos, listContatoEntity );
    }
}
