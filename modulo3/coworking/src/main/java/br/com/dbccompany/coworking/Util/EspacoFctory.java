package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;

import java.util.List;

public class EspacoFctory extends AbsFactoryModelMapper {
    private static EspacoFctory factory;

    private EspacoFctory() {}

    public static EspacoFctory getInstance() {
        if(factory == null) {
            factory = new EspacoFctory();
        }
        return factory;
    }

    public EspacoDTO fazerDTO( EspacoEntity entidade ) {
        return super.modelMapper.map( entidade, EspacoDTO.class );
    }

    public EspacoEntity fazerEntity( EspacoDTO dto ) {
        return super.modelMapper.map( dto, EspacoEntity.class );
    }

    public List<EspacoDTO> fazerVariosDTO( List<EspacoEntity> entidades ) {
        return super.modelMapper.map( entidades, listEspacoDTO );
    }

    public List<EspacoEntity> fazerVariasEntity( List<EspacoDTO> dtos ) {
        return super.modelMapper.map( dtos, listEspacoEntity );
    }
}
