package br.com.dbccompany.coworking.Exception.TipoContato;

public class TipoContatoException extends Exception {
    private String mensagem;

    public TipoContatoException( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
