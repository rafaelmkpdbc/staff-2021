package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.EspacoPacoteFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoPacoteService extends GenericService<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {
    @Autowired
    private EspacoPacoteRepository espPacRepository;

    @Autowired
    private PacoteRepository pctRepository;

    @Autowired
    private EspacoRepository espRepository;

    private EspacoPacoteFactory factory = EspacoPacoteFactory.getInstance();

    public List<EspacoPacoteDTO> buscarTodosEspacoPacote() {
        List<EspacoPacoteEntity> espacosPacotes = super.buscarTodos();
        return factory.fazerVariosDTO( espacosPacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacoteDTO salvar( EspacoPacoteDTO dto ) {

        EspacoPacoteEntity espacoPacote = factory.fazerEntity( dto );
//        espacoPacote.setEspaco( espRepository.findById( dto.getEspaco().getId() ).get() );
//        espacoPacote.setPacote( pctRepository.findById( dto.getPacote().getId() ).get() );
        EspacoPacoteEntity entidade = super.salvarEEditar( espacoPacote );
        return factory.fazerDTO( entidade );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacoteDTO editar( EspacoPacoteDTO dto, Integer id ) {
        dto.setId( id );
        EspacoPacoteEntity espacoPacote = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( espacoPacote );
    }

    public EspacoPacoteDTO buscarEspacoPacotePorId( Integer id ) {
        EspacoPacoteEntity espacoPacote = super.buscarPorId( id );
        return factory.fazerDTO( espacoPacote );
    }
}
