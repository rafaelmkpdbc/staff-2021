package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//testar
//fazer outros buscar
@Controller
@RequestMapping( value = "/coworking/contato" )
public class ContatoController {
    @Autowired
    private ContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ContatoDTO> buscarTodosContatos() {
        return service.buscarTodosContatos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ContatoDTO salvar( @RequestBody  ContatoDTO contato ) {
        return service.salvar( contato );
    }

    @PostMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContatoDTO editar( @RequestBody  ContatoDTO contato, @PathVariable Integer id ) {
        return service.editar( contato, id );
    }
}
