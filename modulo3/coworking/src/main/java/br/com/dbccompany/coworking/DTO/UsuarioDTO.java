package br.com.dbccompany.coworking.DTO;


public class UsuarioDTO {
    private Integer id;
    private String username;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername( String username ) {
        this.username = username;
    }

}
