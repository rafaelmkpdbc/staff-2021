package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PacoteDTO {
        private Integer id;
        private String valor;
        private List<ClientePacoteDTO> clientePacote;
        private List<EspacoPacoteDTO> espacoPacote;

        public Integer getId() {
                return id;
        }

        public void setId( Integer id ) {
                this.id = id;
        }

        public String getValor() {
                return valor;
        }

        public void setValor( String valor ) {
                this.valor = valor;
        }

        public List<ClientePacoteDTO> getClientePacote() {
                return clientePacote;
        }

        public void setClientePacote( List<ClientePacoteDTO> clientePacote ) {
                this.clientePacote = clientePacote;
        }

        public List<EspacoPacoteDTO> getEspacoPacote() {
                return espacoPacote;
        }

        public void setEspacoPacote( List<EspacoPacoteDTO> espacoPacote ) {
                this.espacoPacote = espacoPacote;
        }
}
