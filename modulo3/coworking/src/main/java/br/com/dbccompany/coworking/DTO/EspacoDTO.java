package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "nome")
public class EspacoDTO {
    private Integer id;
    private String nome;
    private Integer qtdPessoas;
    private String valor;
    private List<EspacoPacoteDTO> espacoPacote;
    private List<ContratacaoDTO> contratacao;
    private List<SaldoClienteDTO> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas( Integer qtdPessoas ) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor( String valor ) {
        this.valor = valor;
    }

    public List<EspacoPacoteDTO> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote( List<EspacoPacoteDTO> espacoPacote ) {
        this.espacoPacote = espacoPacote;
    }

    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao( List<ContratacaoDTO> contratacao ) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteDTO> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( List<SaldoClienteDTO> saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }
}
