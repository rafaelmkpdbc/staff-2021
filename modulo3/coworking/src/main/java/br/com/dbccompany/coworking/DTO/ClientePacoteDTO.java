package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ClientePacoteDTO {
    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    private Integer quantidade;
    private List<PagamentoDTO> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente( ClienteDTO cliente ) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote( PacoteDTO pacote ) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public List<PagamentoDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento( List<PagamentoDTO> pagamento ) {
        this.pagamento = pagamento;
    }
}
