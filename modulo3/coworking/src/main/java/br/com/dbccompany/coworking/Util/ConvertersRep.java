package br.com.dbccompany.coworking.Util;

import org.modelmapper.Converter;

public class ConvertersRep {
    private static RealConversor realConversor = RealConversor.getInstance();

    private static Converter<String, Double> monetaryToDouble = ctx -> ctx.getSource() == null ? null:
            realConversor.extrairValor( ctx.getSource() );

    private static Converter<Double, String> doubleToMonetary = ctx -> ctx.getSource() == null ? null :
            realConversor.gerarMonetario( ctx.getSource() );

    public static Converter<String, Double> getMonetaryToDouble() {
        return monetaryToDouble;
    }

    public static Converter<Double, String> getDoubleToMonetary() {
        return doubleToMonetary;
    }
}
