package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.util.List;

public class AcessoFactory extends AbsFactoryModelMapper {
    private static AcessoFactory factory;

    private AcessoFactory(){}

    public static AcessoFactory getInstance() {
        if( factory == null ){
            factory = new AcessoFactory();
        }
        return factory;
    }

    public AcessoDTO fazerDTO( AcessoEntity entidade ) {
        return super.modelMapper.map( entidade, AcessoDTO.class );
    }

    public AcessoEntity fazerEntity( AcessoDTO dto ) {
        return super.modelMapper.map( dto, AcessoEntity.class );
    }

    public List<AcessoDTO> fazerVariosDTO( List<AcessoEntity> entidades ) {
        return super.modelMapper.map( entidades, listAcessoDTO );
    }

    public List<AcessoEntity> fazerVariasEntity( List<AcessoDTO> dtos ) {
        return super.modelMapper.map( dtos, listAcessoEntity );
    }
}
