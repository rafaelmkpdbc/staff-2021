package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Exception.Cliente.ClienteException;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler( {ClienteException.class, } )
    public final ResponseEntity<ClienteExceptionResponse> handleClienteException( ClienteException exception ) {
        ClienteExceptionResponse response = new ClienteExceptionResponse( System.currentTimeMillis(), exception.getMensagem(), HttpStatus.NOT_ACCEPTABLE.toString() );
        return new ResponseEntity<>( response, HttpStatus.valueOf( response.getHttpCodeMessage() ) );
    }

}
