package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PagamentoDTO {
    private Integer id;
    private ClientePacoteDTO clientePacote;
    private ContratacaoDTO contratacao;
    private String tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public ClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote( ClientePacoteDTO clientePacote ) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao( ContratacaoDTO contratacao ) {
        this.contratacao = contratacao;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento( String tipoPagamento ) {
        this.tipoPagamento = tipoPagamento;
    }
}
