package br.com.dbccompany.coworking.Exception.Cliente;

public class ClienteException extends  Exception {
    private String mensagem;

    public ClienteException( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
