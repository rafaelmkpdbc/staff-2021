package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import com.sun.istack.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    @NotNull
    @Override
    List<UsuarioEntity> findAll();

    Optional<UsuarioEntity> findByUsername( String username );
}
