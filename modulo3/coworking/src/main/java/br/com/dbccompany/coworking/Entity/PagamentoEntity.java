package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ" )
    @GeneratedValue( generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "id_cliente_pacote" )
    private ClientePacoteEntity clientePacote;

    @ManyToOne
    @JoinColumn( name = "id_contratacao" )
    private ContratacaoEntity contratacao;

    @Column( nullable = false )
    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento( TipoPagamentoEnum tipoPagamento ) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote( ClientePacoteEntity clientePacote ) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao( ContratacaoEntity contrtacao ) {
        this.contratacao = contrtacao;
    }
}
