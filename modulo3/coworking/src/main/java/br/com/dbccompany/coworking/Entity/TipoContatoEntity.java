package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TipoContatoEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ" )
    @GeneratedValue( generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false, unique = true )
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }
}
