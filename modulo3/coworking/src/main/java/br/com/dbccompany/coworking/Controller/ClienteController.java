package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteException;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.Util.LogSender;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/cliente" )
public class ClienteController {

    private final ClienteService cliService;

    private LogSender logSender = LogSender.getInstance();

    public ClienteController( ClienteService clienteService ) {
        this.cliService = clienteService;
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ClienteDTO> buscarTodos() {
        try {
            return  cliService.buscarTodosClientes();
        } catch ( ClienteException e ) {
            logSender.emitirEEnviarErroCliente( e );
            throw new ResponseStatusException( HttpStatus.NO_CONTENT, e.getMensagem() );
        }
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ClienteDTO salvarCliente( @RequestBody ClienteDTO cliente ) throws ClienteException {
        try{
            return cliService.salvar( cliente );
        } catch ( ClienteException e ) {
            logSender.emitirEEnviarErroCliente( e );
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, e.getMensagem() );
        }
    }

    @PostMapping( value = "/editar/{id}")
    @ResponseBody
    public ClienteDTO editarCliente( @RequestBody ClienteDTO cliente, @PathVariable Integer id ) {
        return cliService.editar( cliente, id );
    }
}
