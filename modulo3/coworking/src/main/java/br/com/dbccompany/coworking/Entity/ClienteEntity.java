package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jdk.vm.ci.meta.Local;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Entity
public class ClienteEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ" )
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true )
    private String cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    @Column( nullable = false )
    private LocalDate dataNascimento;

    @OneToMany( mappedBy = "cliente" )
    private List<ContatoEntity> contato;

    @OneToMany( mappedBy = "cliente")
    private List<ClientePacoteEntity> clientePacote;

    @OneToMany( mappedBy = "cliente")
    private List<ContratacaoEntity> contratacao;

    @OneToMany( mappedBy = "cliente")
    private List<SaldoClienteEntity> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf( String cpf ) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento( LocalDate dataNascimento ) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato( List<ContatoEntity> contato ) {
        this.contato = contato;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote( List<ClientePacoteEntity> clientePacote ) {
        this.clientePacote = clientePacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao( List<ContratacaoEntity> contratacao ) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( List<SaldoClienteEntity> saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }
}
