package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LogDTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss.SSSZ")
    private String timeStamp;
    private String processo;
    private String tipo;
    private String descricao;

    public LogDTO( String timeStamp, String processo, String tipo, String descricao ) {
        this.timeStamp = timeStamp;
        this.processo = processo;
        this.tipo = tipo;
        this.descricao = descricao;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp( String timeStamp ) {
        this.timeStamp = timeStamp;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso( String processo ) {
        this.processo = processo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo( String tipo ) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao( String descricao ) {
        this.descricao = descricao;
    }
}
