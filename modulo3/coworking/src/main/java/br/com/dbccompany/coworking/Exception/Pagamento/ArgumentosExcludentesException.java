package br.com.dbccompany.coworking.Exception.Pagamento;

public class ArgumentosExcludentesException extends PagamentoException {
    public ArgumentosExcludentesException() {
        super( "Necessário informar apenas contratação OU pacote!" );
    }
}
