package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.SaldoClienteFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService extends GenericService<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId>{
    @Autowired
    private SaldoClienteRepository repository;
    @Autowired
    private ClienteRepository cliRepository;
    @Autowired
    private EspacoRepository espRepository;

    private SaldoClienteFactory factory = SaldoClienteFactory.getInstance();

    public List<SaldoClienteDTO> buscarTodosSaldosCliente() {
        List<SaldoClienteEntity> saldoClientes = super.buscarTodos();
        return factory.fazerVariosDTO( saldoClientes );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteDTO salvar( SaldoClienteDTO dto ) {
        Optional<ClienteEntity> cliente = cliRepository.findById( dto.getCliente().getId() );
        Optional<EspacoEntity> espaco = espRepository.findById( dto.getEspaco().getId() );
        SaldoClienteId id = new SaldoClienteId( dto.getCliente().getId(), dto.getEspaco().getId() );
        dto.setId( id );
        SaldoClienteEntity entidade = factory.fazerEntity( dto );
        entidade.setCliente( cliente.get() );
        entidade.setEspaco( espaco.get() );
        entidade = super.salvarEEditar( entidade );
//        SaldoClienteEntity entidade = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( entidade );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteDTO editar( SaldoClienteDTO dto, Integer idCliente, Integer idEspaco ) {
        SaldoClienteId id = new SaldoClienteId( idCliente, idEspaco );
        dto.setId( id );
        SaldoClienteEntity entidade = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( entidade );
    }

    protected void gerarSaldoPorContratacao( ContratacaoEntity contratacao ) {
        Integer idCliente = contratacao.getCliente().getId();
        Integer idEspaco = contratacao.getEspaco().getId();
        SaldoClienteId id = new SaldoClienteId( idCliente, idEspaco );
        SaldoClienteEntity saldo = super.buscarPorId( id );
        if( saldo == null ) {
            saldo = this.construirSaldo( contratacao );
            saldo.setId( id );
        } else {
            Integer diasAMais = contratacao.getPrazo();
            LocalDate novoVencimento = saldo.getVencimento().plusDays( diasAMais );
            saldo.setVencimento( novoVencimento );
        }
        super.salvarEEditar( saldo );
    }

    protected void gerarSaldoPorPacote( ClientePacoteEntity clientePacote ) {
        List<SaldoClienteEntity> saldosAtualizados = new ArrayList<>();
        for( EspacoPacoteEntity ep : clientePacote.getPacote().getEspacoPacote() ) {
            Integer idCliente = clientePacote.getCliente().getId();
            Integer idEspaco = ep.getEspaco().getId();
            SaldoClienteId id = new SaldoClienteId( idCliente, idEspaco );
            SaldoClienteEntity saldo = super.buscarPorId( id );
            if( saldo == null ) {
                saldo = this.construirSaldo( ep );
                saldo.setId( id );
                saldo.setCliente( clientePacote.getCliente() );
            } else {
                Integer diasAMais = ep.getPrazo();
                LocalDate novoVencimento = saldo.getVencimento().plusDays( diasAMais );
                saldo.setVencimento( novoVencimento );
            }
            saldosAtualizados.add( saldo );
        }
        repository.saveAll( saldosAtualizados );
    }

    private SaldoClienteEntity construirSaldo( ContratacaoEntity contratacao ) {
        SaldoClienteEntity saldo = new SaldoClienteEntity();
        saldo.setCliente( contratacao.getCliente() );
        saldo.setEspaco( contratacao.getEspaco() );
        saldo.setQuantidade( contratacao.getQuantidade() );
        saldo.setTipoContratacao( contratacao.getTipoContratacao() );
        LocalDate vencimento = LocalDate.now().plusDays( (Integer) contratacao.getPrazo() );
        saldo.setVencimento( vencimento );
        return saldo;
    }

    private SaldoClienteEntity construirSaldo( EspacoPacoteEntity espacoPacote ) {
        SaldoClienteEntity saldo = new SaldoClienteEntity();
        saldo.setEspaco( espacoPacote.getEspaco() );
        saldo.setQuantidade( espacoPacote.getQuantidade() );
        saldo.setTipoContratacao( espacoPacote.getTipoContratacao() );
        LocalDate vencimento = LocalDate.now().plusDays( (Integer) espacoPacote.getPrazo() );
        saldo.setVencimento( vencimento );
        return saldo;
    }
}
