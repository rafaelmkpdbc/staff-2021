package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.GenericEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
public abstract class GenericService<R extends CrudRepository<E, ID>,
        E extends GenericEntity, ID extends Serializable> {
    @Autowired
    private R repository;

    public List<E> buscarTodos(){
        return (List<E>) repository.findAll();
    }

    public E buscarPorId( ID id ) {
        Optional<E> entidade = repository.findById( id );
        return entidade.orElse( null );
    }

    protected E salvarEEditar( E entidade ) {
        return repository.save( entidade );
    }
}
