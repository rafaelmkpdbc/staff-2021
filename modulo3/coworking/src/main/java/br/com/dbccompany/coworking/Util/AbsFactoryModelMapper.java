package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Entity.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public abstract class AbsFactoryModelMapper {
    protected ModelMapper modelMapper;

    Type listClienteDTO = new TypeToken<ArrayList<ClienteDTO>>(){}.getType();
    Type listClienteEntity = new TypeToken<ArrayList<ClienteEntity>>(){}.getType();
    Type listTipoContatoDTO = new TypeToken<ArrayList<TipoContatoDTO>>(){}.getType();
    Type listTipoContatoEntity = new TypeToken<ArrayList<TipoContatoEntity>>(){}.getType();
    Type listContatoDTO = new TypeToken<ArrayList<ContatoDTO>>(){}.getType();
    Type listContatoEntity = new TypeToken<ArrayList<ContatoEntity>>(){}.getType();
    Type listEspacoDTO = new TypeToken<ArrayList<EspacoDTO>>(){}.getType();
    Type listEspacoEntity = new TypeToken<ArrayList<EspacoEntity>>(){}.getType();
    Type listPacoteDTO = new TypeToken<ArrayList<PacoteDTO>>(){}.getType();
    Type listPacoteEntity = new TypeToken<ArrayList<PacoteEntity>>(){}.getType();
    Type listClientePacoteDTO = new TypeToken<ArrayList<ClientePacoteDTO>>(){}.getType();
    Type listClientePacoteEntity = new TypeToken<ArrayList<ClientePacoteEntity>>(){}.getType();
    Type listEspacoPacoteDTO = new TypeToken<ArrayList<EspacoPacoteDTO>>(){}.getType();
    Type listEspacoPacoteEntity = new TypeToken<ArrayList<EspacoPacoteEntity>>(){}.getType();
    Type listContratacaoDTO = new TypeToken<ArrayList<ContratacaoDTO>>(){}.getType();
    Type listContratacaoEntity = new TypeToken<ArrayList<ContratacaoEntity>>(){}.getType();
    Type listSaldoClienteDTO = new TypeToken<ArrayList<SaldoClienteDTO>>(){}.getType();
    Type listSaldoClienteEntity = new TypeToken<ArrayList<SaldoClienteEntity>>(){}.getType();
    Type listPagamentoDTO = new TypeToken<ArrayList<PagamentoDTO>>(){}.getType();
    Type listPagamentoEntity = new TypeToken<ArrayList<PagamentoEntity>>(){}.getType();
    Type listAcessoDTO = new TypeToken<ArrayList<AcessoDTO>>(){}.getType();
    Type listAcessoEntity = new TypeToken<ArrayList<AcessoEntity>>(){}.getType();

    protected AbsFactoryModelMapper() {
        this.modelMapper = new ModelMapper();
        setMappingPacoteValorDTO();
        setMappingPacoteValorEntity();
        setMappingEspacoValorDTO();
        setMappingEspacoValorEntity();
        setContatoMapping();
//        setMappingPagamento();
    }

    private void setMappingPacoteValorDTO() {
        this.modelMapper.typeMap( PacoteEntity.class , PacoteDTO.class ).addMappings(
                mapper -> mapper.using( ConvertersRep.getDoubleToMonetary() ).map( PacoteEntity::getValor, PacoteDTO::setValor )
        );
    }

    private void setMappingPacoteValorEntity() {
        this.modelMapper.typeMap( PacoteDTO.class, PacoteEntity.class ).addMappings(
                mapper -> mapper.using( ConvertersRep.getMonetaryToDouble() ).map( PacoteDTO::getValor, PacoteEntity::setValor )
        );
    }

    private void setMappingEspacoValorDTO() {
        this.modelMapper.typeMap( EspacoEntity.class, EspacoDTO.class ).addMappings(
                mapper -> mapper.using( ConvertersRep.getDoubleToMonetary() ).map(EspacoEntity::getValor, EspacoDTO::setValor)
        );
    }

    private void setMappingEspacoValorEntity() {
        this.modelMapper.typeMap( EspacoDTO.class, EspacoEntity.class ).addMappings(
                mapper -> mapper.using( ConvertersRep.getMonetaryToDouble() ).map( EspacoDTO::getValor, EspacoEntity::setValor )
        );
    }

//    private void setMappingPagamento() {
//        this.modelMapper.typeMap( ClientePacoteDTO.class, ClientePacoteEntity.class ).addMappings(
//                mapper -> {
//                    mapper.map(src -> src.get, PagamentoEntity::setClientePacote);
//                }
//        );
//    }

    private void setContatoMapping() {
        this.modelMapper.typeMap( ContatoEntity.class, ContatoDTO.class ).addMappings(
            mapper -> {
                mapper.map(src -> src.getCliente().getNome(), ContatoDTO::setCliente);
                mapper.map(src -> src.getTipoContato().getNome(), ContatoDTO::setTipoContato);
            }
        );
    }
}
