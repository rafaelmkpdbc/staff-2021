package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class SaldoClienteEntity extends GenericEntity {
    @EmbeddedId
    private SaldoClienteId id = new SaldoClienteId();

    @ManyToOne
    @MapsId( "id_cliente" )
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId( "id_espaco" )
    private EspacoEntity espaco;

    @Column( nullable = false )
    private TipoContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private LocalDate vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId( SaldoClienteId id ) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente( ClienteEntity cliente ) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco( EspacoEntity espaco ) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( TipoContratacaoEnum tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento( LocalDate vencimento ) {
        this.vencimento = vencimento;
    }
}
