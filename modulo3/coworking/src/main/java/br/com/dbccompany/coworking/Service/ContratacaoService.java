package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ContratacaoFactory;
import br.com.dbccompany.coworking.Util.RealConversor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService extends GenericService<ContratacaoRepository, ContratacaoEntity, Integer> {
    @Autowired
    private ContratacaoRepository repository;
    @Autowired
    private EspacoRepository espacoRepository;
    @Autowired
    private ClienteRepository clienteRepository;

    private RealConversor conversor = RealConversor.getInstance();

    private ContratacaoFactory factory = ContratacaoFactory.getInstance();

    public List<ContratacaoDTO> buscarTodosContratacao() {
        List<ContratacaoEntity> contratacoes = super.buscarTodos();
        return factory.fazerVariosDTO( contratacoes );
    }

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoDTO salvar( ContratacaoDTO dto ) {
        ContratacaoEntity contratacao = super.salvarEEditar( factory.fazerEntity( dto ) );
        Optional<EspacoEntity> espaco = espacoRepository.findById( dto.getEspaco().getId() );
        Optional<ClienteEntity> cliente = clienteRepository.findById( dto.getCliente().getId() );
        contratacao.setEspaco( espaco.get() );
        contratacao.setCliente( cliente.get() );
        ContratacaoDTO dtoSalvo = factory.fazerDTO( contratacao );
        this.calcularValorContratacao( dtoSalvo );
        return dtoSalvo;
    }

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoDTO editar( ContratacaoDTO dto, Integer id ) {
        dto.setId( id );
        ContratacaoEntity contratacao = super.salvarEEditar( factory.fazerEntity( dto ) );
        ContratacaoDTO dtoSalvo = factory.fazerDTO( contratacao );
        this.calcularValorContratacao( dtoSalvo );
        return dtoSalvo;
    }

    private void calcularValorContratacao( ContratacaoDTO dto ) {
        EspacoDTO espaco = dto.getEspaco();
        Double valor = conversor.extrairValor( espaco.getValor() );
        valor *= (double) dto.getQuantidade();
        valor = dto.getDesconto() != null ?
                valor * dto.getDesconto() :
                valor;
        String valorMonetario = conversor.gerarMonetario( valor );
        dto.setValor( valorMonetario );
    }

}
