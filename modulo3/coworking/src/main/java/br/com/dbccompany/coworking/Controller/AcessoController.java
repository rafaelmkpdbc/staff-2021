package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/acesso" )
public class AcessoController {
    @Autowired
    private AcessoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<AcessoDTO> buscarTodosAcessos() {
        return service.buscarTodosAcessos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public AcessoDTO salvar( @RequestBody AcessoDTO dto ) {
        return service.salvar( dto );
    }
}
