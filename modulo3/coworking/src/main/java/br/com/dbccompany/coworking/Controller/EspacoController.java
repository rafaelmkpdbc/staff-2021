package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/espaco" )
public class EspacoController {

    @Autowired
    private EspacoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<EspacoDTO> buscarTodos() {
        return service.buscarTodosEspacos();
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public EspacoDTO salvar( @RequestBody EspacoDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacoDTO editar( @RequestBody EspacoDTO dto, @PathVariable Integer id) {
        return service.editar( dto, id );
    }
}
