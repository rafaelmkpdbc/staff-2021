package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/contratacao" )
public class ContratacaoController {
    @Autowired
    private ContratacaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ContratacaoDTO> buscarTodasContratacao() {
        return service.buscarTodosContratacao();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ContratacaoDTO salvar( @RequestBody ContratacaoDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContratacaoDTO editar( @RequestBody ContratacaoDTO dto, @PathVariable Integer id ) {
        return service.editar( dto, id );
    }
}
