package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;

import java.util.List;

public class PagamentoFactory extends AbsFactoryModelMapper {
    private static PagamentoFactory factory;

    private PagamentoFactory(){}

    public static PagamentoFactory getInstance() {
        if( factory == null ){
            factory = new PagamentoFactory();
        }
        return factory;
    }

    public PagamentoDTO fazerDTO( PagamentoEntity entidade ) {
        return super.modelMapper.map( entidade, PagamentoDTO.class );
    }

    public PagamentoEntity fazerEntity( PagamentoDTO dto ) {
        return super.modelMapper.map( dto, PagamentoEntity.class );
    }

    public List<PagamentoDTO> fazerVariosDTO( List<PagamentoEntity> entidades ) {
        return super.modelMapper.map( entidades, listPagamentoDTO );
    }

    public List<PagamentoEntity> fazerVariasEntity( List<PagamentoDTO> dtos ) {
        return super.modelMapper.map( dtos, listPagamentoEntity );
    }
}
