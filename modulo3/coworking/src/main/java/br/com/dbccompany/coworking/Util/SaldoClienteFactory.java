package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;

import java.util.List;

public class SaldoClienteFactory extends AbsFactoryModelMapper {
    private static SaldoClienteFactory factory;

    private SaldoClienteFactory(){}

    public static SaldoClienteFactory getInstance() {
        if( factory == null ){
            factory = new SaldoClienteFactory();
        }
        return factory;
    }

    public SaldoClienteDTO fazerDTO( SaldoClienteEntity entidade ) {
        return super.modelMapper.map( entidade, SaldoClienteDTO.class );
    }

    public SaldoClienteEntity fazerEntity( SaldoClienteDTO dto ) {
        return super.modelMapper.map( dto, SaldoClienteEntity.class );
    }

    public List<SaldoClienteDTO> fazerVariosDTO( List<SaldoClienteEntity> entidades ) {
        return super.modelMapper.map( entidades, listSaldoClienteDTO );
    }

    public List<SaldoClienteEntity> fazerVariasEntity( List<SaldoClienteDTO> dtos ) {
        return super.modelMapper.map( dtos, listSaldoClienteEntity );
    }
}
