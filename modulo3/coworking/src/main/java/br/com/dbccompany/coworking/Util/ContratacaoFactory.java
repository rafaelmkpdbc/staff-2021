package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;

import java.util.List;

public class ContratacaoFactory extends AbsFactoryModelMapper {
    private static ContratacaoFactory factory;

    private ContratacaoFactory(){}

    public static ContratacaoFactory getInstance() {
        if( factory == null ){
            factory = new ContratacaoFactory();
        }
        return factory;
    }

    public ContratacaoDTO fazerDTO( ContratacaoEntity entidade ) {
        return super.modelMapper.map( entidade, ContratacaoDTO.class );
    }

    public ContratacaoEntity fazerEntity( ContratacaoDTO dto ) {
        return super.modelMapper.map( dto, ContratacaoEntity.class );
    }

    public List<ContratacaoDTO> fazerVariosDTO( List<ContratacaoEntity> entidades ) {
        return super.modelMapper.map( entidades, listContratacaoDTO );
    }

    public List<ContratacaoEntity> fazerVariasEntity( List<ContratacaoDTO> dtos ) {
        return super.modelMapper.map( dtos, listContratacaoEntity );
    }
}
