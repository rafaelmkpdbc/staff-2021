package br.com.dbccompany.coworking.Entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Entity
public class UsuarioEntity implements UserDetails {
    @Id
    @SequenceGenerator( name = "USER_SEQ", sequenceName = "USER_SEQ" )
    @GeneratedValue( generator = "USER_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false, unique = true )
    private String username;

    @Column( nullable = false )
    private String password;

    private String roles = "";

    private String permissions = "";

    public UsuarioEntity(){
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername( String username ) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> autorizacoes = new ArrayList<>();
        for (String permission : this.getPermissionList() ) {
            GrantedAuthority authority = new SimpleGrantedAuthority( permission );
            autorizacoes.add( authority );
        }

        for (String role : this.getRoleList() ) {
            GrantedAuthority authority = new SimpleGrantedAuthority( "ROLE_" + role );
            autorizacoes.add( authority );
        }

        return null;
    }

    private List<String> getRoleList () {
        if ( this.roles != null ) {
            return Arrays.asList( this.roles.split( "," ) );
        }
        return new ArrayList<>();
    }

    private List<String> getPermissionList () {
        if ( this.permissions != null ) {
            return Arrays.asList( this.permissions.split( "," ) );
        }
        return new ArrayList<>();
    }

    public void setRoles( String roles ) {
        this.roles = roles;
    }

    public void setPermissions( String permissions ) {
        this.permissions = permissions;
    }
}
