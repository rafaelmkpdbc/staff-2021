package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.ContatoFactory;
import br.com.dbccompany.coworking.Util.TipoContatoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService extends GenericService<TipoContatoRepository, TipoContatoEntity, Integer> {
    @Autowired
    private TipoContatoRepository repository;

    private TipoContatoFactory tipoFactory = TipoContatoFactory.getInstance();
    private ContatoFactory contatoFactory = ContatoFactory.getInstance();

    public List<TipoContatoDTO> buscarTodosTiposContato() {
        List<TipoContatoEntity> tipos = super.buscarTodos();
        return tipoFactory.fazerVariosDTO( tipos );
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoDTO salvar( TipoContatoDTO dto ) {
        TipoContatoEntity contato =  super.salvarEEditar( tipoFactory.fazerEntity( dto ) );
        return tipoFactory.fazerDTO( contato );
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoDTO editar( TipoContatoDTO dto, Integer id ) {
        dto.setId( id );
        TipoContatoEntity contato =  super.salvarEEditar( tipoFactory.fazerEntity( dto ) );
        return tipoFactory.fazerDTO( contato );
    }

    public TipoContatoDTO buscarTipoPorId( Integer id ) {
        TipoContatoEntity tipo = super.buscarPorId( id );
        return tipoFactory.fazerDTO( tipo );
    }

    public TipoContatoDTO buscarTipoPorNome( String nome ) {
        TipoContatoEntity tipo = repository.findByNome( nome );
        return tipoFactory.fazerDTO( tipo );
    }
}
