package br.com.dbccompany.coworking.Exception.Cliente;

public class ClienteCPFInvalidoException extends ClienteException {
    public ClienteCPFInvalidoException() {
        super( "CPF Inválido" );
    }
}
