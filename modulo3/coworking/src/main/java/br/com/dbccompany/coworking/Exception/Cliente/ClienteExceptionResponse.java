package br.com.dbccompany.coworking.Exception.Cliente;

public class ClienteExceptionResponse  {
    private Long timestamp;
    private String message;
    private String HttpCodeMessage;

    public ClienteExceptionResponse( Long timestamp, String message, String httpCodeMessage ) {
        this.timestamp = timestamp;
        this.message = message;
        HttpCodeMessage = httpCodeMessage;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp( Long timestamp ) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage( String message ) {
        this.message = message;
    }

    public String getHttpCodeMessage() {
        return HttpCodeMessage;
    }

    public void setHttpCodeMessage( String httpCodeMessage ) {
        HttpCodeMessage = httpCodeMessage;
    }
}
