package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;

import java.util.List;

public class PacoteFactory extends AbsFactoryModelMapper {
    private static PacoteFactory factory;

    private PacoteFactory() {}

    public static PacoteFactory getInstance() {
        if( factory == null ) {
            factory = new PacoteFactory();
        }
        return factory;
    }

    public PacoteDTO fazerDTO( PacoteEntity entidade ) {
        return super.modelMapper.map( entidade, PacoteDTO.class );
    }

    public PacoteEntity fazerEntity( PacoteDTO dto ) {
        return super.modelMapper.map( dto, PacoteEntity.class );
    }

    public List<PacoteDTO> fazerVariosDTO( List<PacoteEntity> entidades ) {
        return super.modelMapper.map( entidades, listPacoteDTO );
    }

    public List<PacoteEntity> fazerVariasEntity( List<PacoteDTO> dtos ) {
        return super.modelMapper.map( dtos, listPacoteEntity );
    }
}
