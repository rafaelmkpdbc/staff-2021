package br.com.dbccompany.coworking.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsServiceImpl userService;

    @Override
    protected void configure( HttpSecurity http ) throws Exception{
        http.headers().frameOptions().sameOrigin().and().cors().and()
                .csrf().disable().authorizeRequests()
                .antMatchers( "/coworking/login" ).permitAll()
                .antMatchers( HttpMethod.POST, "/coworking/usuario/salvar" ).permitAll()
                .antMatchers( "/console/**" ).permitAll()
                .anyRequest().authenticated().and()
                .addFilterBefore( new JWTLoginFilter( "/coworking/login", authenticationManager() ),
                        UsernamePasswordAuthenticationFilter.class )
                .addFilterBefore( new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class );
    }


    @Override
    protected void configure( AuthenticationManagerBuilder auth ) throws Exception {
//        auth.userDetailsService( userService ).passwordEncoder(new BCryptPasswordEncoder());
        auth.inMemoryAuthentication()
                .withUser( "admin" )
                .password( "{noop}admin" )
                .roles( "admin" );
    }
}
