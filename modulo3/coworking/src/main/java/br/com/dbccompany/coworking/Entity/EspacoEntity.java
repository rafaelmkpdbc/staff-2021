package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity extends GenericEntity{
    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ" )
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false, unique = true )
    private String nome;

    @Column( nullable = false )
    private Integer qtdPessoas;

    @Column( nullable = false )
    private Double valor;

    @OneToMany( mappedBy = "espaco" )
    private List<EspacoPacoteEntity> espacoPacote;

    @OneToMany( mappedBy = "espaco" )
    private List<ContratacaoEntity> contratacao;

    @OneToMany( mappedBy = "espaco" )
    private  List<SaldoClienteEntity> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas( Integer qtdPessoas ) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor( Double valor ) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote( List<EspacoPacoteEntity> espacoPacote ) {
        this.espacoPacote = espacoPacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao( List<ContratacaoEntity> contratacao ) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( List<SaldoClienteEntity> saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }
}
