package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//testar
//fazer outros buscar
@Controller
@RequestMapping( value = "/coworking/tipo-contato")
public class TipoContatoController {
    @Autowired
    private TipoContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<TipoContatoDTO> buscarTodosTipos() {
        return service.buscarTodosTiposContato();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public TipoContatoDTO salvar( @RequestBody TipoContatoDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContatoDTO editar( @RequestBody TipoContatoDTO dto, @PathVariable Integer id ) {
        return service.editar( dto, id );
    }
}
