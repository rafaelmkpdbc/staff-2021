package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/pacote" )
public class PacoteController {
    @Autowired
    private PacoteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<PacoteDTO> buscarTodos() {
        return service.buscarTodosPacotes();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public PacoteDTO salvarPacote( @RequestBody PacoteDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{id}" )
    @ResponseBody
    public PacoteDTO editarPacote( @RequestBody PacoteDTO dto, @PathVariable Integer id ) {
        return service.editar( dto, id );
    }
}
