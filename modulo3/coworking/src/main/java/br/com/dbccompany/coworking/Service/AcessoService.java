package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.AcessoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService extends GenericService<AcessoRepository, AcessoEntity, Integer> {
    @Autowired
    private AcessoRepository repository;
    @Autowired
    private SaldoClienteRepository saldoRepository;

    private AcessoFactory factory = AcessoFactory.getInstance();

    public List<AcessoDTO> buscarTodosAcessos() {
        List<AcessoEntity> acessos = super.buscarTodos();
        return factory.fazerVariosDTO( acessos );
    }

    @Transactional( rollbackFor = Exception.class )
    public AcessoDTO salvar( AcessoDTO dto ) {
        dto.setData( LocalDateTime.now() );
        if(dto.getEntrada()) {
            if ( validarEntrada( dto ) ) {
                AcessoEntity entidade = factory.fazerEntity( dto );
                return factory.fazerDTO( super.salvarEEditar( entidade ) );
            } else {
                return null;
            }
        } else {
            if(efetuarSaida( dto )) {
                AcessoEntity entidade = factory.fazerEntity( dto );
                return factory.fazerDTO(super.salvarEEditar( entidade ));
            }
        }
        return null;
    }

    private boolean validarEntrada( AcessoDTO acesso ) {
        SaldoClienteId id = acesso.getSaldoCliente().getId();
        Optional<SaldoClienteEntity> saldoCliente = saldoRepository.findById( id );
        if( saldoCliente.get().getQuantidade() < 0 ) {
            System.err.println("Não há saldo disponível!");
            return false;
        }
        if( acesso.getData().isBefore( saldoCliente.get().getVencimento().atStartOfDay() ) ) {
            saldoCliente.get().setQuantidade( 0 );
            saldoRepository.save( saldoCliente.get() );
            System.err.println("Contratação vencida!");
            return false;
        }
        return true;
    }

    private boolean efetuarSaida( AcessoDTO acesso ) {
        SaldoClienteId id = acesso.getSaldoCliente().getId();
        Optional<SaldoClienteEntity> saldoCliente = saldoRepository.findById( id );
        AcessoEntity ultimoAcesso = repository.findFirstBySaldoClienteOrderByData( saldoCliente.get() );
        Duration tempoUso = Duration.between( ultimoAcesso.getData(), acesso.getData() );
        Integer novaQuantidade = saldoCliente.get().getQuantidade() - (int) tempoUso.toMinutes();
        saldoCliente.get().setQuantidade( novaQuantidade );
        saldoRepository.save( saldoCliente.get() );
        return true;
    }
}
