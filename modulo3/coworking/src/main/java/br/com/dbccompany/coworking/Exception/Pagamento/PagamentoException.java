package br.com.dbccompany.coworking.Exception.Pagamento;

public class PagamentoException extends Exception {
    private String mensagem;

    public PagamentoException ( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem( String mensagem ) {
        this.mensagem = mensagem;
    }
}
