package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    List<SaldoClienteEntity> findAll();
    Optional<SaldoClienteEntity> findById( SaldoClienteId id );
    List<SaldoClienteEntity> findAllByCliente( ClienteEntity cliente );
    List<SaldoClienteEntity> findAllByEspaco( EspacoEntity espaco );
    List<SaldoClienteEntity> findAllByTipoContratacao( TipoContratacaoEnum tipoContratacao );
    List<SaldoClienteEntity> findAllByVencimento( Date vencimento );
}
