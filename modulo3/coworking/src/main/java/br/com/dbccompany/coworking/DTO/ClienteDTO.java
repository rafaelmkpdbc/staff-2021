package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "nome")
public class ClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    private LocalDate dataNascimento;
    private List<ContatoDTO> contato;
    private List<ClientePacoteDTO> clientePacote;
    private List<ContratacaoDTO> contratacao;
    private List<SaldoClienteDTO> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf( String cpf ) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento( LocalDate dataNascimento ) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContato() {
        return contato;
    }

    public void setContato( ArrayList<ContatoDTO> contato ) {
        this.contato = contato;
    }

    public List<ClientePacoteDTO> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote( ArrayList<ClientePacoteDTO> clientePacote ) {
        this.clientePacote = clientePacote;
    }

    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao( ArrayList<ContratacaoDTO> contratacao ) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteDTO> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( ArrayList<SaldoClienteDTO> saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }
}
