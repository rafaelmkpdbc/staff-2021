package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClientePacoteEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ" )
    @GeneratedValue( generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "id_cliente" )
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn( name = "id_pacote")
    private PacoteEntity pacote;

    @Column( nullable = false )
    private Integer quantidade;

    @OneToMany( mappedBy = "clientePacote" )
    private List<PagamentoEntity> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente( ClienteEntity cliente ) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote( PacoteEntity pacote ) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento( List<PagamentoEntity> pagamento ) {
        this.pagamento = pagamento;
    }
}
