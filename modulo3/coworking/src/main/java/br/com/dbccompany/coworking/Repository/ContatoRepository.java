package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    List<ContatoEntity> findAllByTipoContato( TipoContatoEntity tipo );
    ContatoEntity findByValor( String valor );
    ContatoEntity findByCliente( ClienteEntity cliente );
    List<ContatoEntity> findAllByCliente( ClienteEntity cliente );
}
