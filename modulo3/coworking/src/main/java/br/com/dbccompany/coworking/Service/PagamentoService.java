package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.Pagamento.ArgumentosExcludentesException;
import br.com.dbccompany.coworking.Exception.Pagamento.ArgumentosInvalidosException;
import br.com.dbccompany.coworking.Exception.Pagamento.PagamentoException;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.ClientePacoteFactory;
import br.com.dbccompany.coworking.Util.ContratacaoFactory;
import br.com.dbccompany.coworking.Util.PagamentoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService extends GenericService<PagamentoRepository, PagamentoEntity, Integer> {
    @Autowired
    private PagamentoRepository repository;
    @Autowired
    private ClientePacoteRepository cpRepository;
    @Autowired
    private ContratacaoRepository contraRepository;
    @Autowired
    private SaldoClienteRepository saldoRepository;

    @Autowired
    private SaldoClienteService saldoService;

    private PagamentoFactory pagFactory = PagamentoFactory.getInstance();
    private ClientePacoteFactory cpFactory = ClientePacoteFactory.getInstance();
    private ContratacaoFactory contraFactory = ContratacaoFactory.getInstance();

    public List<PagamentoDTO> buscarTodosPagamentos() {
        List<PagamentoEntity> entidades = super.buscarTodos();
        return pagFactory.fazerVariosDTO( entidades );
    }

    @Transactional( rollbackFor = Exception.class )
    public PagamentoDTO salvar( PagamentoDTO dto ) throws ArgumentosExcludentesException, ArgumentosInvalidosException {
        validarInputRefPagamento( dto );
        PagamentoEntity entidade = pagFactory.fazerEntity( dto );
        vincularCPOuContratacao( entidade );
        if( ehRefContratacao( entidade ) ) {
            gerarSaldoPorContratacao( entidade );
        } else {
            gerarSaldoPorPacote( entidade );
        }
        entidade = super.salvarEEditar( entidade );
        return pagFactory.fazerDTO( entidade );
    }

    private void validarInputRefPagamento( PagamentoDTO dto ) throws ArgumentosExcludentesException, ArgumentosInvalidosException {
        if( dto.getClientePacote() != null && dto.getContratacao() != null ) {
            throw new ArgumentosExcludentesException();
        }
        if( dto.getTipoPagamento() == null &&
                dto.getContratacao() == null &&
                dto.getClientePacote() == null ) {
            throw new ArgumentosInvalidosException();
        }
    }

    private void vincularCPOuContratacao( PagamentoEntity pgto ) {
        if ( this.ehRefContratacao( pgto ) ) {
            this.vincularContratacao( pgto );
        } else {
            this.vincularClientePacote( pgto );
        }
    }

    private boolean ehRefContratacao( PagamentoEntity pgto ) {
        return pgto.getContratacao() != null;
    }

    private void vincularContratacao( PagamentoEntity pgto ) {
        Optional<ContratacaoEntity> contratacao = contraRepository.findById( pgto.getContratacao().getId() );
        pgto.setContratacao( contratacao.get() );
    }

    private void vincularClientePacote( PagamentoEntity pgto ) {
        Optional<ClientePacoteEntity> clientePacote = cpRepository.findById( pgto.getClientePacote().getId() );
        pgto.setClientePacote( clientePacote.get() );
    }

    private void gerarSaldoPorContratacao( PagamentoEntity pgto ) {
        Optional<ContratacaoEntity> contratacao = contraRepository.findById( pgto.getContratacao().getId() );
        saldoService.gerarSaldoPorContratacao( contratacao.get() );
    }

    private void gerarSaldoPorPacote( PagamentoEntity pgto ) {
        Optional<ClientePacoteEntity> cp = cpRepository.findById( pgto.getClientePacote().getId() );
        saldoService.gerarSaldoPorPacote( cp.get() );
    }
}
