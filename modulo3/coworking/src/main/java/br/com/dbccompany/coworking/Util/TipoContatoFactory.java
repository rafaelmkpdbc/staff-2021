package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.List;

public class TipoContatoFactory extends AbsFactoryModelMapper {
    private static TipoContatoFactory factory;

    private TipoContatoFactory(){}

    public static TipoContatoFactory getInstance() {
        if( factory == null ){
            factory = new TipoContatoFactory();
        }
        return factory;
    }

    public TipoContatoDTO fazerDTO( TipoContatoEntity entidade ) {
        return super.modelMapper.map( entidade, TipoContatoDTO.class );
    }

    public TipoContatoEntity fazerEntity( TipoContatoDTO dto ) {
        return super.modelMapper.map( dto, TipoContatoEntity.class );
    }

    public List<TipoContatoDTO> fazerVariosDTO( List<TipoContatoEntity> entidades ) {
        return super.modelMapper.map( entidades, listTipoContatoDTO );
    }

    public List<TipoContatoEntity> fazerVariasEntity( List<TipoContatoDTO> dtos ) {
        return super.modelMapper.map( dtos, listTipoContatoEntity );
    }
}
