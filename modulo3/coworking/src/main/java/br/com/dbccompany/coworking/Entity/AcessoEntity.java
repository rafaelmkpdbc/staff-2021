package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ" )
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    private SaldoClienteEntity saldoCliente;

    private Boolean isEntrada;
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    private LocalDateTime data;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( SaldoClienteEntity saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada( Boolean entrada ) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData( LocalDateTime data ) {
        this.data = data;
    }
}
