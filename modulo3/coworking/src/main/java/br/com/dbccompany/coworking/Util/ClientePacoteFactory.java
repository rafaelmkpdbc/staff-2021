package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;

import java.util.List;

public class ClientePacoteFactory extends AbsFactoryModelMapper {
    private static ClientePacoteFactory factory;

    private ClientePacoteFactory() {}

    public static ClientePacoteFactory getInstance() {
        if( factory == null ) {
            factory = new ClientePacoteFactory();
        }
        return factory;
    }

    public ClientePacoteDTO fazerDTO( ClientePacoteEntity entidade ) {
        return super.modelMapper.map( entidade, ClientePacoteDTO.class );
    }

    public ClientePacoteEntity fazerEntity( ClientePacoteDTO dto ) {
        return super.modelMapper.map( dto, ClientePacoteEntity.class );
    }

    public List<ClientePacoteDTO> fazerVariosDTO( List<ClientePacoteEntity> entidades ) {
        return super.modelMapper.map( entidades, listClientePacoteDTO );
    }

    public List<ClientePacoteEntity> fazerVariasEntity( List<ClientePacoteDTO> dtos ) {
        return super.modelMapper.map( dtos, listClientePacoteEntity );
    }
}
