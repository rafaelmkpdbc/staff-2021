package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity extends GenericEntity {
    @Id
    @SequenceGenerator( name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ" )
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private Double valor;

    @OneToMany( mappedBy = "pacote" )
    private List<ClientePacoteEntity> clientePacote;

    @OneToMany( mappedBy = "pacote")
    private List<EspacoPacoteEntity> espacoPacote;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor( Double valor ) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote( List<ClientePacoteEntity> clientePacote ) {
        this.clientePacote = clientePacote;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote( List<EspacoPacoteEntity> espacoPacote ) {
        this.espacoPacote = espacoPacote;
    }
}
