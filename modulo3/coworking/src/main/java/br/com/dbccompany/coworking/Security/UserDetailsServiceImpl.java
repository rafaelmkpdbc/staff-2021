package br.com.dbccompany.coworking.Security;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UsuarioRepository repository;

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
        Optional<UsuarioEntity> user = repository.findByUsername( username );

        if(!user.isPresent()) {
            throw new UsernameNotFoundException( "Usuário não encontrado" );
        }

        return user.get();
    }
}
