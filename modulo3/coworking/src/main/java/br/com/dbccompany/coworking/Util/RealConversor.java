package br.com.dbccompany.coworking.Util;

import java.text.NumberFormat;
import java.util.Locale;

public class RealConversor {
    private static RealConversor conversor;
    private final Locale BR = new Locale("pt", "BR");
    private final NumberFormat FORMATADOR = NumberFormat.getCurrencyInstance( BR );

    private RealConversor() {}

    public static RealConversor getInstance() {
        if( conversor == null ) {
            conversor = new RealConversor();
        }
        return conversor;
    }

    public Double extrairValor( String monetario ) {
        monetario = limparString( monetario );
        return Double.parseDouble( monetario );
    }

    public String gerarMonetario( Double valor ) {
        return FORMATADOR.format( valor );
    }

    private String limparString( String monetario ) {
        monetario = tirarCodigoMoeda( monetario );
        monetario =tirarSeparadorMilhar( monetario );
        monetario = corrigirSeparadorDecimal( monetario );
        return monetario;
    }

    private String tirarCodigoMoeda( String monetario ) {
        monetario = monetario.replaceAll( "R\\$", "" );
        monetario = monetario.replaceAll( " ", "" );
        monetario = monetario.replaceAll( "\u00A0", "" );
        return monetario;
    }

    private String tirarSeparadorMilhar( String monetario ) {
        monetario = monetario.replaceAll( "\\.", "" );
        return monetario;
    }

    private String corrigirSeparadorDecimal( String monetario ) {
        monetario = monetario.replaceAll( ",", "." );
        return monetario;
    }
}
