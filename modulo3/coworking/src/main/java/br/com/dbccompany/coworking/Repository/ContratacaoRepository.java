package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findAll();
    Optional<ContratacaoEntity> findById( Integer integer );
    List<ContratacaoEntity> findAllByCliente( ClienteEntity cliente );
    List<ContratacaoEntity> findAllByEspaco( EspacoEntity espaco );
    List<ContratacaoEntity> findAllByTipoContratacao ( TipoContratacaoEnum tipoContratacao );
}
