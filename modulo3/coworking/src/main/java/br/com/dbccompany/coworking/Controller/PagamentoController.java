package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Exception.Pagamento.PagamentoException;
import br.com.dbccompany.coworking.Service.PagamentoService;
import br.com.dbccompany.coworking.Util.PagamentoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/pagamento" )
public class PagamentoController {
    @Autowired
    private PagamentoService service;

    private PagamentoFactory factory = PagamentoFactory.getInstance();

    @GetMapping( value = "/" )
    @ResponseBody
    public List<PagamentoDTO> buscarTodosPagamentos() {
        return service.buscarTodosPagamentos();
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public PagamentoDTO salvar( @RequestBody PagamentoDTO dto ) {
        try {
            return service.salvar( dto );
        } catch ( PagamentoException e ) {
            System.err.println(e.getMensagem());
            return null;
        }
    }
}
