package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    List<EspacoPacoteEntity> findAll();
    Optional<EspacoPacoteEntity> findById( Integer id );
    List<EspacoPacoteEntity> findAllByEspaco( EspacoEntity espaco );
    List<EspacoPacoteEntity> findAllByPacote( PacoteEntity pacote );
    List<EspacoPacoteEntity> findAllByPrazoAndTipoContratacao( Integer prazo, TipoContratacaoEnum tipoContratacao );
    List<EspacoPacoteEntity> findAllByTipoContratacao( TipoContratacaoEnum tipoContratacao );
}
