package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Util.UsuarioFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    private UsuarioFactory factory = UsuarioFactory.getInstance();

    @Transactional( rollbackFor = Exception.class )
    public UsuarioDTO salvar( UsuarioEntity usuario ) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        usuario.setPassword(bcryptEncoder.encode(usuario.getPassword()));
        return factory.fazerDTO( repository.save(usuario) );
    }
}
