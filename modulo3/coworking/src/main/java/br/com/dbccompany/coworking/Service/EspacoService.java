package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.EspacoFctory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoService extends GenericService<EspacoRepository, EspacoEntity, Integer> {

    @Autowired
    private EspacoRepository repository;

    private EspacoFctory espacoFactory = EspacoFctory.getInstance();

    public List<EspacoDTO> buscarTodosEspacos() {
        List<EspacoEntity> espacos = super.buscarTodos();
        return espacoFactory.fazerVariosDTO( espacos );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoDTO salvar( EspacoDTO dto ) {
        EspacoEntity espacoSalvo = super.salvarEEditar( espacoFactory.fazerEntity( dto ) );
        return espacoFactory.fazerDTO( espacoSalvo );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoDTO editar( EspacoDTO dto, Integer id ) {
        EspacoEntity espaco = espacoFactory.fazerEntity( dto );
        espaco.setId( id );
        EspacoEntity entidade = super.salvarEEditar( espaco );

        return espacoFactory.fazerDTO( entidade );
    }

}
