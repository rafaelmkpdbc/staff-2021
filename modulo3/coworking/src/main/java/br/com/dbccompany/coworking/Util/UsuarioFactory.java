package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;

public class UsuarioFactory extends AbsFactoryModelMapper {
    private static UsuarioFactory factory;

    private UsuarioFactory(){}

    public static UsuarioFactory getInstance() {
        if( factory == null ){
            factory = new UsuarioFactory();
        }
        return factory;
    }

    public UsuarioDTO fazerDTO( UsuarioEntity entidade ) {
        return super.modelMapper.map( entidade, UsuarioDTO.class );
    }

    public UsuarioEntity fazerEntity( UsuarioDTO dto ) {
        return super.modelMapper.map( dto, UsuarioEntity.class );
    }
}
