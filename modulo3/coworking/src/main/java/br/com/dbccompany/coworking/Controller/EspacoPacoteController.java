package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/espaco-pacote" )
public class EspacoPacoteController {
    @Autowired
    private EspacoPacoteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<EspacoPacoteDTO> buscarTodosEspacosPacotes() {
        return service.buscarTodosEspacoPacote();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public EspacoPacoteDTO salvarEspacoPacote( @RequestBody EspacoPacoteDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacoPacoteDTO editarEspacoPacote( @RequestBody EspacoPacoteDTO dto, @PathVariable Integer id ) {
        return service.editar( dto, id );
    }
}
