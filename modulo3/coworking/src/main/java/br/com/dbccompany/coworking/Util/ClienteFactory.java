package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;

import java.util.List;

public class ClienteFactory extends AbsFactoryModelMapper {
    private static ClienteFactory factory;

    private ClienteFactory(){}

    public static ClienteFactory getInstance() {
        if( factory == null ){
            factory = new ClienteFactory();
        }
        return factory;
    }

    public ClienteDTO fazerDTO( ClienteEntity entidade ) {
        return super.modelMapper.map( entidade, ClienteDTO.class );
    }

    public ClienteEntity fazerEntity( ClienteDTO dto ) {
        return super.modelMapper.map( dto, ClienteEntity.class );
    }

    public List<ClienteDTO> fazerVariosDTO( List<ClienteEntity> entidades ) {
        return super.modelMapper.map( entidades, listClienteDTO );
    }

    public List<ClienteEntity> fazerVariasEntity( List<ClienteDTO> dtos ) {
        return super.modelMapper.map( dtos, listClienteEntity );
    }
}
