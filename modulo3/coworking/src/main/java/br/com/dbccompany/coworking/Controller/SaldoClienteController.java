package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/coworking/saldo-cliente" )
public class SaldoClienteController {
    @Autowired
    private SaldoClienteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<SaldoClienteDTO> buscarTodosSaldosCliente() {
        return service.buscarTodosSaldosCliente();
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public SaldoClienteDTO salvar( @RequestBody SaldoClienteDTO dto ) {
        return service.salvar( dto );
    }

    @PostMapping( value = "/editar/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO editar( @RequestBody SaldoClienteDTO dto, @PathVariable Integer idCliente,
                                   @PathVariable Integer idEspaco ) {
        return service.editar( dto, idCliente, idEspaco );
    }
}
