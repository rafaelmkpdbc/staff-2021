package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
    List<PacoteEntity> findAll();
    Optional<PacoteEntity> findById( Integer id );
    List<PacoteEntity> findAllByValor( Double valor );
}
