package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ContratacaoDTO {
    private Integer id;
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private String tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private String valor;
    private Integer prazo;
    private List<PagamentoDTO> pagamento;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente( ClienteDTO cliente ) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco( EspacoDTO espaco ) {
        this.espaco = espaco;
    }

    public String getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao( String tipoContratacao ) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto( Double desconto ) {
        this.desconto = desconto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor( String valor ) {
        this.valor = valor;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo( Integer prazo ) {
        this.prazo = prazo;
    }

    public List<PagamentoDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento( List<PagamentoDTO> pagamento ) {
        this.pagamento = pagamento;
    }
}
