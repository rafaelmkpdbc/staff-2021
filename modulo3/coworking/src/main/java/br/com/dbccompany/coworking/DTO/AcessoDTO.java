package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.time.LocalDate;
import java.time.LocalDateTime;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AcessoDTO {
    private Integer id;
    private SaldoClienteDTO saldoCliente;
    private Boolean isEntrada;
    private LocalDateTime data;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public SaldoClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente( SaldoClienteDTO saldoCliente ) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada( Boolean entrada ) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData( LocalDateTime data ) {
        this.data = data;
    }
}
