package br.com.dbccompany.coworking.Exception.Pagamento;

public class ArgumentosInvalidosException extends PagamentoException {
    public ArgumentosInvalidosException() {
        super( "Necessário informar tipo de pagamento e pacote ou contratação!" );
    }
}
