package br.com.dbccompany.coworking.Exception.Cliente;

public class ClienteArgumentoFaltando extends ClienteException {
    public ClienteArgumentoFaltando() {
        super( "Necessário informar nome, CPF e data de nascimento!" );
    }
}
