package br.com.dbccompany.coworking.DTO;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.List;

@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "nome")
public class TipoContatoDTO {
    private Integer id;
    private String nome;
//    @JsonManagedReference
//    private List<ContatoDTO> contatos;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }
//
//    public List<ContatoDTO> getContatos() {
//        return contatos;
//    }
//
//    public void setContatos( List<ContatoDTO> contatos ) {
//        this.contatos = contatos;
//    }
}
