package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.PacoteFactory;
import br.com.dbccompany.coworking.Util.RealConversor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacoteService extends GenericService<PacoteRepository, PacoteEntity, Integer> {
    @Autowired
    private PacoteRepository repository;

    private PacoteFactory factory = PacoteFactory.getInstance();

    public List<PacoteDTO> buscarTodosPacotes() {
        List<PacoteEntity> pacotes = super.buscarTodos();
        return factory.fazerVariosDTO( pacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public PacoteDTO salvar( PacoteDTO dto ) {

        PacoteEntity pacoteSalvo = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( pacoteSalvo );
    }

    @Transactional( rollbackFor = Exception.class )
    public PacoteDTO editar( PacoteDTO dto, Integer id ) {
        dto.setId( id );
        PacoteEntity pacoteEditado = super.salvarEEditar( factory.fazerEntity( dto ) );
        return factory.fazerDTO( pacoteEditado );
    }
}
