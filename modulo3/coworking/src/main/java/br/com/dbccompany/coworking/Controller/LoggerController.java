package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Exception.Cliente.ClienteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.SimpleDateFormat;

@Controller
@RequestMapping( value = "/coworking/test-log")
public class LoggerController {

    Logger logger = LoggerFactory.getLogger( LoggerController.class );

    @GetMapping( value = "/" )
    public void index() {
        logger.trace( "A trace message" );
        logger.debug( "A debug message" );
        logger.info( "A info message" );
        logger.warn( "A warn message" );
        logger.error( "An error message" );
//        logger.error( "erro de cliente", new ClienteException("erro de cliente") );
        System.err.println( getPID() );
        System.err.println( getCurrentTimeStamp() + " ERROR " + "mensagem de erro" );
    }

    private String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(System.currentTimeMillis());
    }

    private String getPID() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        return rt.getName();
    }
}
