package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.ClienteFactory;
import br.com.dbccompany.coworking.Util.ContatoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService extends GenericService<ContatoRepository, ContatoEntity, Integer> {
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private TipoContatoRepository tipoRepository;

    private ContatoFactory contatoFactory = ContatoFactory.getInstance();
    private ClienteFactory clienteFactory = ClienteFactory.getInstance();

    public List<ContatoDTO> buscarTodosContatos() {
        List<ContatoEntity> contatos = super.buscarTodos();
        return contatoFactory.fazerVariosDTO( contatos );
    }

    @Transactional( rollbackFor = Exception.class )
    public ContatoDTO salvar( ContatoDTO dto ) {
        ContatoEntity contato = super.salvarEEditar( contatoFactory.fazerEntity( dto ) );
        return contatoFactory.fazerDTO( contato );
    }

    @Transactional( rollbackFor = Exception.class )
    public ContatoDTO editar( ContatoDTO dto, Integer id ) {
        ContatoEntity contato = contatoFactory.fazerEntity( dto );
        contato.setId( id );
        ContatoEntity entidade = super.salvarEEditar( contato );
        return contatoFactory.fazerDTO( entidade );
    }
}
