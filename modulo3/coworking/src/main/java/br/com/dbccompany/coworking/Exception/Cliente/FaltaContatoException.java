package br.com.dbccompany.coworking.Exception.Cliente;

public class FaltaContatoException extends ClienteException {
    public FaltaContatoException() {
        super( "Necessário informar no mínimo um email e um telefone para contato." );
    }
}
