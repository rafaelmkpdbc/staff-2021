package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;

import java.util.List;

public class EspacoPacoteFactory extends AbsFactoryModelMapper{
    private static EspacoPacoteFactory factory;

    private EspacoPacoteFactory(){}

    public static EspacoPacoteFactory getInstance() {
        if( factory == null ){
            factory = new EspacoPacoteFactory();
        }
        return factory;
    }

    public EspacoPacoteDTO fazerDTO( EspacoPacoteEntity entidade ) {
        return super.modelMapper.map( entidade, EspacoPacoteDTO.class );
    }

    public EspacoPacoteEntity fazerEntity( EspacoPacoteDTO dto ) {
        return super.modelMapper.map( dto, EspacoPacoteEntity.class );
    }

    public List<EspacoPacoteDTO> fazerVariosDTO( List<EspacoPacoteEntity> entidades ) {
        return super.modelMapper.map( entidades, listEspacoPacoteDTO );
    }

    public List<EspacoPacoteEntity> fazerVariasEntity( List<EspacoPacoteDTO> dtos ) {
        return super.modelMapper.map( dtos, listEspacoPacoteEntity );
    }
}
