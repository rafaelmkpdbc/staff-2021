package br.com.dbccompany.coworkinglogs.Entity;

import org.springframework.data.annotation.Id;

public class LogsEntity {
    @Id
    private String id;
    //adicionar atributo timestamp
    private String codigo;
    private String tipo;
    private String descricao;

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo( String codigo ) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo( String tipo ) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao( String descricao ) {
        this.descricao = descricao;
    }
}
