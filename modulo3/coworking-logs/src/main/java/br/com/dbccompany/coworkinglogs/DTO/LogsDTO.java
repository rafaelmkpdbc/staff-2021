package br.com.dbccompany.coworkinglogs.DTO;

import br.com.dbccompany.coworkinglogs.Entity.LogsEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class LogsDTO {
    private String id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss.SSSZ")
    private LocalDateTime timeStamp;
    private String tipo;
    private String descricao;

    protected LogsDTO(){}

    public LogsDTO( LogsEntity entidade ) {
        this.id = entidade.getId();
        this.tipo = entidade.getTipo();
        this.descricao = entidade.getDescricao();
    }

    public LogsEntity converter() {
        LogsEntity entidade = new LogsEntity();
        entidade.setId( id );
        entidade.setTipo( tipo );
        entidade.setDescricao( descricao );
        return entidade;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo( String tipo ) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao( String descricao ) {
        this.descricao = descricao;
    }
}
