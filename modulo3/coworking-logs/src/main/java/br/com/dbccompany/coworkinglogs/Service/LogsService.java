package br.com.dbccompany.coworkinglogs.Service;

import br.com.dbccompany.coworkinglogs.DTO.LogsDTO;
import br.com.dbccompany.coworkinglogs.Entity.LogsEntity;
import br.com.dbccompany.coworkinglogs.Repository.LogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//TODO: Add excecao em buscar e salvar
//TODO: testar metodos
@Service
public class LogsService {
    @Autowired
    private LogsRepository repository;

    public List<LogsDTO> buscarTodos() {
        List<LogsEntity> entidades = repository.findAll();
        return this.converterListaParaDTO( entidades );
    }

    @Transactional( rollbackFor = Exception.class )
    public LogsDTO salvar( LogsDTO dto ) {
        LogsEntity log = repository.save( dto.converter() );
        return new LogsDTO( log );
    }

    public LogsDTO buscarPorId( String id ) {
        Optional<LogsEntity> entidade = repository.findById( id );
        return entidade.isPresent() ?
                new LogsDTO( entidade.get() ) :
                null;
    }

    public List<LogsDTO> buscarTodosPorCodigo( String codigo ) {
        List<LogsEntity> entidades = repository.findAllByCodigo( codigo );
        return this.converterListaParaDTO( entidades );
    }

    public List<LogsDTO> buscarTodosPorTipo( String tipo ) {
        List<LogsEntity> entidades = repository.findAllByTipo( tipo );
        return this.converterListaParaDTO( entidades );
    }

    public List<LogsDTO> buscarTodosContendoDescricao( String descricao ) {
        List<LogsEntity> entidades = repository.findAllByDescricaoLike( descricao );
        return this.converterListaParaDTO( entidades );
    }


    private List<LogsDTO> converterListaParaDTO( List<LogsEntity> entidades ) {
        ArrayList<LogsDTO> dtos = new ArrayList<>();
        for( LogsEntity entidade : entidades ) {
            dtos.add( new LogsDTO( entidade ) );
        }
        return dtos;
    }

    private List<LogsEntity> converterListaParaEntidade( List<LogsDTO> dtos ) {
        ArrayList<LogsEntity> entidades = new ArrayList<>();
        for( LogsDTO dto : dtos ) {
            entidades.add( dto.converter() );
        }
        return entidades;
    }
}
