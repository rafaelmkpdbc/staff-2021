package br.com.dbccompany.coworkinglogs.Repository;

import br.com.dbccompany.coworkinglogs.Entity.LogsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface LogsRepository extends MongoRepository<LogsEntity, String> {
    List<LogsEntity> findAll();
    Optional<LogsEntity> findById( String id );
    List<LogsEntity> findAllByCodigo( String codigo );
    List<LogsEntity> findAllByTipo( String tipo );
    List<LogsEntity> findAllByDescricaoLike( String descricao );
}
