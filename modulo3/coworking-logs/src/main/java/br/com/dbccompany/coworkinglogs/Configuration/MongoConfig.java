package br.com.dbccompany.coworkinglogs.Configuration;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableMongoRepositories( basePackages = "br.com.dbccompany.coworkinglogs" )
public class MongoConfig extends AbstractMongoClientConfiguration {
    @Override
    protected String getDatabaseName() {
        return "boot-mongodb";
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString( "mongodb://localhost:27017/boot-mongodb" );
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString( connectionString )
                .build();

        return MongoClients.create( mongoClientSettings );
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton( "br.com.dbccompany" );
    }
}
