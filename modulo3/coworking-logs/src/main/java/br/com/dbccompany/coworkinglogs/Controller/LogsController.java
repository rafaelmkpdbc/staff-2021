package br.com.dbccompany.coworkinglogs.Controller;

import br.com.dbccompany.coworkinglogs.DTO.LogsDTO;
import br.com.dbccompany.coworkinglogs.Service.LogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = "/coworking/logs" )
public class LogsController {
    @Autowired
    private LogsService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<LogsDTO> buscarTodosLogs() {
        return service.buscarTodos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public LogsDTO salvar( @RequestBody LogsDTO log ) {
        return service.salvar( log );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public LogsDTO buscarLogPorId( @PathVariable String id ) {
        return service.buscarPorId( id );
    }

    @GetMapping( value = "/{codigo}" )
    @ResponseBody
    public List<LogsDTO> buscarTodosPorCodigo( @PathVariable String codigo ) {
        return service.buscarTodosPorCodigo( codigo );
    }

    @GetMapping( value = "/{tipo}")
    @ResponseBody
    public List<LogsDTO> buscarTodosPorTipo( @PathVariable String tipo ) {
        return service.buscarTodosPorTipo( tipo );
    }
}
