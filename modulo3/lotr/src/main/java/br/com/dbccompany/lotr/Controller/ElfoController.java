package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/personagem/elfo" )
public class ElfoController {
    @Autowired
    private ElfoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosAnoes() {
        return service.buscarTodosElfos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ElfoDTO salvarElfo( @RequestBody ElfoDTO elfo ) {
        return service.salvarElfo( elfo.converter() );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public ElfoDTO editarElfo( @RequestBody ElfoDTO elfo, @PathVariable Integer id ) {
        return service.editarElfo( elfo.converter(), id );
    }

    @GetMapping( value = "/{ id }" )
    @ResponseBody
    public ElfoDTO buscarPorId( @PathVariable Integer id ) {
        return service.buscarElfoPorId( id );
    }

    @GetMapping( value = "/{ ids }")
    @ResponseBody
    public List<ElfoDTO> buscarPorTodosEssesIds( @PathVariable List<Integer> ids ) {
        return service.buscarTodosElfosPorEssesIds( ids );
    }

    @GetMapping( value = "/{ nome }" )
    @ResponseBody
    public ElfoDTO buscarPorNome( @PathVariable String nome ) {
        return service.buscarElfoPorNome( nome );
    }

    @GetMapping( value = "/{ nomes }")
    public List<ElfoDTO> buscarTodosElfosPorEssesNomes( @PathVariable List<String> nomes ) {
        return service.buscarTodosElfosPorEssesNomes( nomes );
    }

    @GetMapping( value = "/{ exp }")
    @ResponseBody
    public ElfoDTO buscarElfoPorExp( @PathVariable Integer exp ) {
        return service.buscarElfoPorExperiencia( exp );
    }

    @GetMapping( value = "/todos/{ exp }")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorExp( @PathVariable Integer exp ) {
        return service.buscarTodosElfosPorExeperiencia( exp );
    }

    @GetMapping( value = "/{ exps }")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorEssasExp( @PathVariable List<Integer> exps ) {
        return service.buscarTodosElfosPorEssasExperiencias( exps );
    }

    @GetMapping( value = "/{ vida }")
    @ResponseBody
    public ElfoDTO buscarElfoPorVida( @PathVariable Double vida ) {
        return service.buscarElfoPorVida( vida );
    }

    @GetMapping( value = "/todos/{ vida }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorVida( @PathVariable Double vida ) {
        return service.buscarTodosElfosPorVida( vida );
    }

    @GetMapping( value = "/{ vidas }")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorEssasVidas( @PathVariable List<Double> vidas ) {
        return service.buscarTodosElfosPorEssasVidas( vidas );
    }

    @GetMapping( value = "/{ dano }")
    @ResponseBody
    public ElfoDTO buscarElfoPorDano( @PathVariable Double dano ) {
        return service.buscarElfoPorDano( dano );
    }

    @GetMapping( value = "/todos/{ dano }")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorDano( @PathVariable Double dano ) {
        return service.buscarTodosElfosPorDano( dano );
    }

    @GetMapping( value = "/{ danos }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorDanos( @PathVariable List<Double> danos ) {
        return service.buscarTodosElfosPorEssesDanos( danos );
    }

    @GetMapping( value = "/{ expAtk }" )
    @ResponseBody
    public ElfoDTO buscarElfoPorExpAtk( @PathVariable Integer expAtk ) {
        return service.buscarElfoPorAtk( expAtk );
    }

    @GetMapping( value = "/todos/{ expAtk }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorAtk( @PathVariable Integer expAtk ) {
        return service.buscarTodosElfosPorExpAtk( expAtk );
    }

    @GetMapping( value = "/{ expAtks }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorEssasExpAtk( @PathVariable List<Integer> expAtks ) {
        return service.buscarTodosElfosPorEssasExpAtk( expAtks );
    }

    @GetMapping( value = "/{ status }" )
    @ResponseBody
    public ElfoDTO buscarElfoPorStatus( @PathVariable String status ) {
        return service.buscarElfoPorStatus( StatusEnum.valueOf( status ) );
    }

    @GetMapping( value = "/todos/{ status }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfoPorStatus( @PathVariable String status ) {
        return service.buscarTodosElfosPorStatus( StatusEnum.valueOf( status ) );
    }

    @GetMapping( value = "/{ statuses }" )
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfoPorStatuses( @PathVariable List<String> statuses ) {
        List<StatusEnum> statusEn = new ArrayList<>();
        for( String status: statuses ) {
            statusEn.add( StatusEnum.valueOf( status ) );
        }
        return service.buscarTodosElfosPorEssesStatus( statusEn );
    }

    @GetMapping( value = "/{ inventario }" )
    @ResponseBody
    public ElfoDTO buscarElfoPorInventario( @PathVariable InventarioDTO inventario ) {
        return service.buscarElfoPorInventario( inventario.converter() );
    }
}
