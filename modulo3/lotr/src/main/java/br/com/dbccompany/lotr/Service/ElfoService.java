package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity> {

    @Autowired
    private ElfoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO salvarElfo( ElfoEntity elfo ) {
        ElfoEntity salvo = super.salvar( elfo );
        return new ElfoDTO( salvo );
    }

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO editarElfo( ElfoEntity elfo, Integer id ) {
        ElfoEntity editado = super.editar( elfo, id );
        return new ElfoDTO( editado );
    }

    public List<ElfoDTO> buscarTodosElfos() {
        List<ElfoEntity> encontrados = super.buscarTodosPersonagens();
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorId( Integer id ){
        ElfoEntity elfo = super.buscarPorId( id );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssesIds( List<Integer> ids ) {
        List<ElfoEntity> elfos = super.buscarTodosEssesPorIds( ids );
        return this.converterLista( elfos );
    }

    public ElfoDTO buscarElfoPorNome( String nome ) {
        ElfoEntity elfo = super.buscarPorNome( nome );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssesNomes( List<String> nomes ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssesNomes( nomes );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorExperiencia( Integer exp ) {
        ElfoEntity elfo = super.buscarPorExperiencia( exp );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorExeperiencia( Integer exp ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorExperiencia( exp );
        return this.converterLista( encontrados );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssasExperiencias( List<Integer> exps ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssasExperiencias( exps );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorVida( Double vida ) {
        ElfoEntity elfo = super.buscarPorVida( vida );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorVida( Double vida ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorVida( vida );
        return this.converterLista( encontrados );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssasVidas( List<Double> vidas ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssasVidas( vidas );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorDano( Double dano ) {
        ElfoEntity elfo = super.buscarPorDano( dano );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorDano( Double dano ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorDano( dano );
        return this.converterLista( encontrados );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssesDanos( List<Double> danos ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssesDanos( danos );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorAtk( Integer expAtk ) {
        ElfoEntity elfo = super.buscarPorExpPorAtk( expAtk );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorExpAtk( Integer expAtk ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorExpPorAtk( expAtk );
        return this.converterLista( encontrados );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssasExpAtk( List<Integer> expAtks ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssesExpPorAtk( expAtks );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorStatus( StatusEnum status ) {
        ElfoEntity elfo = super.buscarPorStatus( status );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> buscarTodosElfosPorStatus( StatusEnum status ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorStatus( status );
        return this.converterLista( encontrados );
    }

    public List<ElfoDTO> buscarTodosElfosPorEssesStatus( List<StatusEnum> statuses ) {
        List<ElfoEntity> encontrados = super.buscarTodosPorEssesStatus( statuses );
        return this.converterLista( encontrados );
    }

    public ElfoDTO buscarElfoPorInventario( InventarioEntity inventario ) {
        ElfoEntity elfo = super.buscarPorInventario( inventario );
        return new ElfoDTO( elfo );
    }

    private List<ElfoDTO> converterLista( List<ElfoEntity> elfos ) {
        ArrayList<ElfoDTO> dtos = new ArrayList<>();
        for(ElfoEntity elfo : elfos ) {
            dtos.add( new ElfoDTO( elfo ) );
        }
        return dtos;
    }
}
