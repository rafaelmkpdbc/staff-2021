package br.com.dbccompany.lotr.Entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue( value = TipoPersonagemEnum.Values.ELFO)
public class ElfoEntity extends PersonagemEntity {
    public ElfoEntity() {}

    public ElfoEntity( String nome ) {
        super(nome);
        super.setVida( 100.0 );
    }
}
