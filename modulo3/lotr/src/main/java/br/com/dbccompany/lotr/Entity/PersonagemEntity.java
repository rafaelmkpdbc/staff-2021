package br.com.dbccompany.lotr.Entity;

import org.hibernate.annotations.ColumnDefault;
import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn( name = "tipo_personagem", length = 32, discriminatorType = DiscriminatorType.STRING )
public abstract class PersonagemEntity {
    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ" )
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE )
    protected Integer id;

    @Column( unique = true, nullable = false )
    protected String nome;

    @Column( nullable = false )
    @ColumnDefault("0")
    protected Integer experiencia = 0;

    @Column( nullable = false, precision = 4, scale = 2 )
    protected Double vida = 0.0;

    @Column( nullable = false, precision = 4, scale = 2 )
    @ColumnDefault("0")
    protected Double dano = 0.0;

    @Column( nullable = false )
    @ColumnDefault("1")
    protected Integer expPorAtk = 1;

    @Enumerated( EnumType.STRING )
    @ColumnDefault("'RECEM_CRIADO'")
    protected StatusEnum status;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_inventario")
    protected InventarioEntity inventario;

    public PersonagemEntity() {}

    public PersonagemEntity( String nome ) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getDano() {
        return dano;
    }

    public void setDano(Double dano) {
        this.dano = dano;
    }

    public Integer getExpPorAtk() {
        return expPorAtk;
    }

    public void setExpPorAtk(Integer expPorAtk) {
        this.expPorAtk = expPorAtk;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}