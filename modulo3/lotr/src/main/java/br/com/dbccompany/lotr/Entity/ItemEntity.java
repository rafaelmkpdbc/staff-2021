package br.com.dbccompany.lotr.Entity;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

import javax.persistence.*;
import java.util.List;

@Entity
public class ItemEntity {
    @Id
    @SequenceGenerator( name = "ITEM_SEQ", sequenceName = "ITEM_SEQ" )
    @GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false, length = 20, unique = true )
    private String descricao;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "item" )
    private List<Inventario_X_ItemEntity> inventarioItem;

    protected ItemEntity() {}

    public ItemEntity( String descricao ) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
