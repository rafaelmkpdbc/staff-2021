package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventarioItem;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario-item")
public class Inventario_X_ItemController {
    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosItensInventarios() {
        return service.trazerTodosOsItensInventarios();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public Inventario_X_ItemDTO salvarInventario( @RequestBody Inventario_X_ItemDTO inventario ) {
        try {
            return service.salvar( inventario.converter() );
        } catch ( ArgumentosInvalidosInventarioItem e ) {
            System.err.println(e.getMensagem());
            return null;
        }
    }

    @PutMapping( value = "/editar/{idInventario}/{idItem}")
    @ResponseBody
    public Inventario_X_ItemDTO editarInventario( @RequestBody Inventario_X_ItemDTO inventario,
                                                  @PathVariable Integer idInventario, @PathVariable Integer idItem ) {
        try {
            return service.editar( inventario.converter(), new Inventario_X_ItemId( idInventario, idItem ) );
        } catch ( ArgumentosInvalidosInventarioItem e ) {
            System.err.println(e.getMensagem());
            return null;
        }
    }


    @GetMapping( value = "/{idInventario}/{idItem}")
    @ResponseBody
    public Inventario_X_ItemDTO buscarPorId( @PathVariable Integer idInventario, @PathVariable Integer idItem ) {
        try {
            return service.buscarPorId( new Inventario_X_ItemId( idInventario, idItem ) );
        } catch ( InventarioItemNaoEncontrado e ) {
            System.err.println(e.getMensagem());
            return null;
        }
    }
}
