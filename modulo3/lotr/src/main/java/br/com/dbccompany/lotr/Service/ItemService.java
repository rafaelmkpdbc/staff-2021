package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemException;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    public List<ItemDTO> trazerTodosOsItens() {
        ArrayList<ItemDTO> itensAchados = new ArrayList<>();
        for( ItemEntity item : repository.findAll() ) {
            itensAchados.add( new ItemDTO( item ));
        }
         return itensAchados;
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO salvar( ItemEntity item ) throws ArgumentosInvalidosItem {
        try{
            ItemEntity itemNovo = this.salvarEEditar( item );
            return new ItemDTO( itemNovo );
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosItem();
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO editar( ItemEntity item, Integer id ) throws ArgumentosInvalidosItem {
        try{
            item.setId(id);
            ItemEntity editado = this.salvarEEditar( item );
            return new ItemDTO( editado );
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosItem();
        }
    }

    private ItemEntity salvarEEditar( ItemEntity item ) {
            return repository.save( item );
    }

    public ItemDTO buscarPorId( Integer id ) throws ItemNaoEncontrado {
        Optional<ItemEntity> item = repository.findById( id );
        if(!item.isPresent()) {
            throw new ItemNaoEncontrado();
        }
        return new ItemDTO( item.get() );
    }

    public List<ItemDTO> buscarTodosPorEssesIds( List<Integer> ids ) {
        ArrayList<ItemDTO> itensAchados = new ArrayList<>();
        for( ItemEntity item : repository.findAllByIdIn( ids ) ) {
            itensAchados.add( new ItemDTO( item ));
        }
        return itensAchados;
    }

    public ItemDTO buscarPorDescricao( String descricao ) {
        ItemEntity item = repository.findByDescricao( descricao );
        return new ItemDTO( item );
    }

    public List<ItemDTO> buscarTodosPorDescricao( String descricao ) {
        ArrayList<ItemDTO> itensAchados = new ArrayList<>();
        for( ItemEntity item : repository.findAllByDescricao( descricao ) ) {
            itensAchados.add( new ItemDTO( item ) );
        }
        return itensAchados;
    }

    public List<ItemDTO> buscarTodosPorEssasDescricoes( List<String> descricoes ) {
        ArrayList<ItemDTO> itensAchados = new ArrayList<>();
        for( ItemEntity item : repository.findAllByDescricaoIn( descricoes ) ) {
            itensAchados.add( new ItemDTO( item ));
        }
        return itensAchados;
    }

    public ItemDTO buscarPorInventarioItem( Inventario_X_ItemEntity inventarioItem ) {
        Integer id = inventarioItem.getId().getId_item();
        try {
            return this.buscarPorId( id );
        } catch ( ItemException e ) {
            return null;
        }
    }

    public List<ItemDTO> buscarTodosPorEssesInventarioItem( List<Inventario_X_ItemEntity> inventariosItens ) {
        ArrayList<Integer> ids = new ArrayList<>();
        for ( Inventario_X_ItemEntity inventarioItem : inventariosItens ) {
            ids.add( inventarioItem.getId().getId_item() );
        }
        return this.buscarTodosPorEssesIds( ids );
    }
}