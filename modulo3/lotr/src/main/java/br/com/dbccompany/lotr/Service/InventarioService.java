package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public InventarioDTO salvar( InventarioEntity inventario ) throws ArgumentosInvalidosInventario {
        try {
            InventarioEntity salvo = this.salvarEEditar( inventario );
            return new InventarioDTO( salvo );
        } catch( Exception e ) {
            throw new ArgumentosInvalidosInventario();
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioDTO editar( InventarioEntity inventario, Integer id ) throws ArgumentosInvalidosInventario{
        inventario.setId(id);
        try {
            InventarioEntity editado = this.salvarEEditar( inventario );
            return new InventarioDTO( editado );
        } catch( Exception e ) {
            throw new ArgumentosInvalidosInventario();
        }
    }

    private InventarioEntity salvarEEditar( InventarioEntity inventario ) {
        return repository.save( inventario );
    }

    public List<InventarioDTO> buscarTodos() {
        List<InventarioEntity> encontrados = repository.findAll();
        return this.transformarListaEmDTO( encontrados );
    }

    public InventarioDTO buscarPorId( Integer id ) throws InventarioNaoEncontrado {
        Optional<InventarioEntity> inventario = repository.findById( id );
        if(!inventario.isPresent()) {
            throw new InventarioNaoEncontrado();
        }
        return new InventarioDTO( inventario.get() );
    }

    public List<InventarioDTO> buscarTodosPorEssesIds( List<Integer> ids ) {
        List<InventarioEntity> encontrados = repository.findAllByIdIn( ids );
        return this.transformarListaEmDTO( encontrados );
    }

    private List<InventarioDTO> transformarListaEmDTO( List<InventarioEntity> inventarios ) {
        List<InventarioDTO> dtos = new ArrayList<>();
        for( InventarioEntity inventario : inventarios ) {
            dtos.add( new InventarioDTO( inventario ) );
        }
        return dtos;
    }
}
