package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Entity.TipoPersonagemEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    Optional<T> findById( Integer id );
    List<T> findAllByIdIn( List<Integer> id );
    T findByNome( String nome );
    List<T> findAllByNomeIn( List<String> nomes );
    T findByExperiencia( Integer experiencia );
    List<T> findAllByExperiencia(Integer experiencia);
    List<T> findAllByExperienciaIn( List<Integer> experiencias );
    T findByVida( Double vida );
    List<T> findAllByVida( Double vida );
    List<T> findAllByVidaIn( List<Double> vidas );
    T findByDano( Double dano );
    List<T> findAllByDano ( Double dano );
    List<T> findAllByDanoIn( List<Double> danos );
    T findByExpPorAtk( Integer expPorAtk );
    List<T> findAllByExpPorAtk( Integer expPorAtk );
    List<T> findAllByExpPorAtkIn( List<Integer> expPorAtk );
    T findByStatus( StatusEnum status );
    List<T> findAllByStatus( StatusEnum status );
    List<T> findAllByStatusIn( List<StatusEnum> statuses );
    T findByInventario(InventarioEntity inventario );
    List<T> findAll();
}
