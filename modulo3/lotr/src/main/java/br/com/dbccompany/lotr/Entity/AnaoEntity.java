package br.com.dbccompany.lotr.Entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue( value = TipoPersonagemEnum.Values.ANAO )
public class AnaoEntity extends PersonagemEntity {
    public AnaoEntity() {}

    public AnaoEntity( String nome ) {
        super(nome);
        super.setVida( 110.0 );
    }
}
