package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {

    @Autowired
    private AnaoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO salvarAnao( AnaoEntity anao ) {
        AnaoEntity salvo = super.salvar( anao );
        return new AnaoDTO( salvo );
    }

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO editarAnao( AnaoEntity anao, Integer id ) {
        AnaoEntity editado = super.editar( anao, id );
        return new AnaoDTO( editado );
    }

    public List<AnaoDTO> buscarTodosAnoes() {
        List<AnaoEntity> encontrados = super.buscarTodosPersonagens();
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorId( Integer id ){
        AnaoEntity anao = super.buscarPorId( id );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssesIds( List<Integer> ids ) {
        List<AnaoEntity> anoes = super.buscarTodosEssesPorIds( ids );
        return this.converterLista( anoes );
    }

    public AnaoDTO buscarAnaoPorNome( String nome ) {
        AnaoEntity anao = super.buscarPorNome( nome );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssesNomes( List<String> nomes ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssesNomes( nomes );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorExperiencia( Integer exp ) {
        AnaoEntity anao = super.buscarPorExperiencia( exp );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorExeperiencia( Integer exp ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorExperiencia( exp );
        return this.converterLista( encontrados );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssasExperiencias( List<Integer> exps ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssasExperiencias( exps );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorVida( Double vida ) {
        AnaoEntity anao = super.buscarPorVida( vida );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorVida( Double vida ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorVida( vida );
        return this.converterLista( encontrados );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssasVidas( List<Double> vidas ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssasVidas( vidas );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorDano( Double dano ) {
        AnaoEntity anao = super.buscarPorDano( dano );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorDano( Double dano ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorDano( dano );
        return this.converterLista( encontrados );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssesDanos( List<Double> danos ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssesDanos( danos );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorAtk( Integer expAtk ) {
        AnaoEntity anao = super.buscarPorExpPorAtk( expAtk );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorExpAtk( Integer expAtk ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorExpPorAtk( expAtk );
        return this.converterLista( encontrados );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssasExpAtk( List<Integer> expAtks ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssesExpPorAtk( expAtks );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorStatus( StatusEnum status ) {
        AnaoEntity anao = super.buscarPorStatus( status );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> buscarTodosAnoesPorStatus( StatusEnum status ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorStatus( status );
        return this.converterLista( encontrados );
    }

    public List<AnaoDTO> buscarTodosAnoesPorEssesStatus( List<StatusEnum> statuses ) {
        List<AnaoEntity> encontrados = super.buscarTodosPorEssesStatus( statuses );
        return this.converterLista( encontrados );
    }

    public AnaoDTO buscarAnaoPorInventario( InventarioDTO inventario ) {
        AnaoEntity anao = super.buscarPorInventario( inventario.converter() );
        return new AnaoDTO( anao );
    }

    private List<AnaoDTO> converterLista( List<AnaoEntity> anoes ) {
        ArrayList<AnaoDTO> dtos = new ArrayList<>();
        for(AnaoEntity anao : anoes ) {
            dtos.add( new AnaoDTO( anao ) );
        }
        return dtos;
    }

}
