package br.com.dbccompany.lotr.Exception;

public class InventarioItemException extends Exception {
    private String mensagem;

    public InventarioItemException( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
