package br.com.dbccompany.lotr.Exception;

public class ArgumentosInvalidosInventarioItem  extends InventarioItemException {
    public ArgumentosInvalidosInventarioItem() {
        super( "Faltam campos!" );
    }
}
