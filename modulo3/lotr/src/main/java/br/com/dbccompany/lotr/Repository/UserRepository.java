package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.UserEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    @NotNull
    @Override
    List<UserEntity> findAll();

    Optional<UserEntity> findByUsername( String username );

}
