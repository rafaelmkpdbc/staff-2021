package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UserEntity;
import br.com.dbccompany.lotr.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository repository;


    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
        Optional<UserEntity> user = repository.findByUsername( username );

        if(!user.isPresent()){
            throw new UsernameNotFoundException( "Usuario não encontrado" );
        }

        return user.get();
    }
}
