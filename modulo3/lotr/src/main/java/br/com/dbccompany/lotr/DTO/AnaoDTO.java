package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;

public class AnaoDTO extends PersonagemDTO {
    private Integer id;
    private String nome;
    private Integer experiencia;
    private Double dano;
    private Integer expPorAtk;
    private InventarioDTO inventario;

    protected AnaoDTO(){}

    public AnaoDTO( AnaoEntity anao ) {
        this.id = anao.getId();
        this.nome = anao.getNome();
        this.experiencia = anao.getExperiencia();
        this.dano = anao.getDano();
        this.expPorAtk = anao.getExpPorAtk();
        this.inventario = anao.getInventario() != null ?
                new InventarioDTO( anao.getInventario() ) :
                null;
    }

    public AnaoEntity converter() {
        AnaoEntity convertido = new AnaoEntity();
        convertido.setId( this.id );
        convertido.setNome( this.nome );
        convertido.setExperiencia( this.experiencia );
        convertido.setDano( this.dano );
        convertido.setExpPorAtk( this.expPorAtk );
        if( this.inventario != null ){
            convertido.setInventario( this.inventario.converter() );
        }

        return convertido;
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia( Integer experiencia ) {
        this.experiencia = experiencia;
    }

    public Double getDano() {
        return dano;
    }

    public void setDano( Double dano ) {
        this.dano = dano;
    }

    public Integer getExpPorAtk() {
        return expPorAtk;
    }

    public void setExpPorAtk( Integer expPorAtk ) {
        this.expPorAtk = expPorAtk;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario( InventarioDTO inventario ) {
        this.inventario = inventario;
    }
}
