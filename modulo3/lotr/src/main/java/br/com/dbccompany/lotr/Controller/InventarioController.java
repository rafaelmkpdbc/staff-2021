package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario")
public class InventarioController {
    @Autowired
    private InventarioService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<InventarioDTO> trazerTodosInventarios() {
        return service.buscarTodos();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public InventarioDTO salvarInventario( @RequestBody InventarioDTO inventario ) {
        try{
            return service.salvar( inventario.converter() );
        } catch ( ArgumentosInvalidosInventario e ) {
            System.err.println( e.getMensagem() );
            return null;
        }
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public InventarioDTO editarInventario( @RequestBody InventarioDTO inventario, @PathVariable Integer id ) {
        try {
            return service.editar( inventario.converter(), id );
        } catch ( ArgumentosInvalidosInventario e ) {
            System.err.println( e.getMessage() );
            return null;
        }
    }

    @GetMapping( value = "/{ id }")
    @ResponseBody
    public InventarioDTO buscarPorId( @PathVariable Integer id ) {
        try {
            return service.buscarPorId( id );
        } catch ( InventarioNaoEncontrado e ) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @GetMapping( value = "/{ ids }")
    @ResponseBody
    public List<InventarioDTO> buscarTodosPorEssesIds( @PathVariable List<Integer> ids ) {
        return service.buscarTodosPorEssesIds( ids );
    }
}
