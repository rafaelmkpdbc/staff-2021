package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E>, E extends PersonagemEntity> {

    @Autowired
    private R repository;

    public List<E> buscarTodosPersonagens(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public E salvar( E personagem ){
        return this.salvarEEditar( personagem );
    }

    @Transactional( rollbackFor = Exception.class )
    public E editar( E personagem, Integer id ) {
        personagem.setId(id);
        return this.salvarEEditar( personagem );
    }

    private E salvarEEditar( E personagem ) {
        return repository.save( personagem );
    }

    public E buscarPorId( Integer id ) {
        Optional<E> personagem = repository.findById(id);
        return personagem.orElse(null);
    }

    public List<E> buscarTodosEssesPorIds( List<Integer> ids ) {
        return repository.findAllByIdIn( ids );
    }

    public E buscarPorNome( String nome ) {
        return repository.findByNome( nome );
    }

    public List<E> buscarTodosPorEssesNomes( List<String> nomes ) {
        return repository.findAllByNomeIn( nomes );
    }

    public E buscarPorExperiencia( Integer exp ) {
        return repository.findByExperiencia( exp );
    }

    public List<E> buscarTodosPorExperiencia( Integer exp ) {
        return repository.findAllByExperiencia( exp );
    }

    public List<E> buscarTodosPorEssasExperiencias( List<Integer> exps) {
        return repository.findAllByExperienciaIn( exps );
    }

    public E buscarPorVida( Double vida ) {
        return repository.findByVida( vida );
    }

    public List<E> buscarTodosPorVida( Double vida ) {
        return repository.findAllByVida( vida );
    }

    public List<E> buscarTodosPorEssasVidas( List<Double> vidas ) {
        return repository.findAllByVidaIn( vidas );
    }

    public E buscarPorDano( Double dano ) {
        return repository.findByDano( dano );
    }

    public List<E> buscarTodosPorDano( Double dano ) {
        return repository.findAllByDano( dano );
    }

    public List<E> buscarTodosPorEssesDanos( List<Double> danos ) {
        return repository.findAllByDanoIn( danos );
    }

    public E buscarPorExpPorAtk( Integer expAtk ) {
        return repository.findByExpPorAtk( expAtk );
    }

    public List<E> buscarTodosPorExpPorAtk( Integer expAtk ) {
        return repository.findAllByExpPorAtk( expAtk );
    }

    public List<E> buscarTodosPorEssesExpPorAtk( List<Integer> expsAtks ) {
        return repository.findAllByExpPorAtkIn( expsAtks );
    }

    public E buscarPorStatus( StatusEnum status ) {
        return repository.findByStatus( status );
    }

    public List<E> buscarTodosPorStatus( StatusEnum status ) {
        return repository.findAllByStatus( status );
    }

    public List<E> buscarTodosPorEssesStatus( List<StatusEnum> statuses ) {
        return repository.findAllByStatusIn( statuses );
    }

    public E buscarPorInventario( InventarioEntity inventario ) {
        return repository.findByInventario( inventario );
    }
}
