package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;

import java.util.HashMap;

public class InventarioDTO {
    private Integer id;
    private HashMap<String, Inventario_X_ItemDTO> itens;
    private PersonagemDTO personagem;

    protected InventarioDTO(){}

    public InventarioDTO( InventarioEntity inventario ) {
        this.id = inventario.getId();

        if( inventario.getPersonagem() != null ) {
            if ( inventario.getPersonagem() instanceof AnaoEntity ) {
                this.personagem = new AnaoDTO( ( AnaoEntity ) inventario.getPersonagem() );
            } else if ( inventario.getPersonagem() instanceof ElfoEntity ) {
                this.personagem = new ElfoDTO( ( ElfoEntity ) inventario.getPersonagem() );
            }
        }

        if( inventario.getInventarioItem() != null ) {
            for ( Inventario_X_ItemEntity ii : inventario.getInventarioItem() ) {
                String itemDesc = ii.getItem().getDescricao();
                if ( !this.itens.containsKey( itemDesc ) ) {
                    this.itens.put( itemDesc, new Inventario_X_ItemDTO( ii ) );
                }
            }
        }
    }

    public InventarioEntity converter() {
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId( this.id );

        if( this.personagem != null ) {
            inventario.setPersonagem( this.personagem.converter() );
        }

        if( this.itens != null ) {
            this.itens.forEach( ( k, v ) -> {
                inventario.getInventarioItem().add( v.converter() );
            } );
        }

        return inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public HashMap<String, Inventario_X_ItemDTO> getItens() {
        return itens;
    }

    public void setItens( HashMap<String, Inventario_X_ItemDTO> itens ) {
        this.itens = itens;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem( PersonagemDTO personagem ) {
        this.personagem = personagem;
    }
}
