package br.com.dbccompany.lotr.Entity;

//String value permite a utilizacao do enum como identificador da classe na tabela personagem
public enum TipoPersonagemEnum {
    ELFO(Values.ELFO),
    ANAO(Values.ANAO);

    private String value;

    private TipoPersonagemEnum(String value) {
        this.value = value;
    }

    public static class Values{
        public static final String ELFO = "ELFO";
        public static final String ANAO = "ANAO";
    }

}
