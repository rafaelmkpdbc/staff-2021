package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemException;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {
    @Autowired
    private ItemService service;

    private Logger logger = LoggerFactory.getLogger( LotrApplication.class );

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosItens() {
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ItemDTO> trazerItemEspecifico( @PathVariable Integer id ) {
        try{
            return new ResponseEntity<>( service.buscarPorId( id ), HttpStatus.ACCEPTED);
        } catch ( ItemException e ) {
            logger.error( "Item teve problema ao buscar especifico: " + e.getMensagem() );
            restTemplate.postForObject( url, e.getMensagem(), Object.class );
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public ResponseEntity<Object> salvarItem( @RequestBody ItemDTO item ) {
        try {
            return new ResponseEntity<>( service.salvar( item.converter() ), HttpStatus.ACCEPTED );
        } catch ( ArgumentosInvalidosItem e ) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemEntity item, @PathVariable Integer id ) {
        try {
            return service.editar( item, id );
        } catch ( ArgumentosInvalidosItem e ) {
            System.err.println(e.getMensagem());
            return null;
        }
    }

    @GetMapping( value = "/todos-ids/{ids}")
    @ResponseBody
    public List<ItemDTO> trazerItensPorEssesIds( @PathVariable ArrayList<Integer> ids ) {
        return service.buscarTodosPorEssesIds( ids );
    }

    @GetMapping( value = "/desc/{descricao}")
    @ResponseBody
    public ItemDTO trazerPorDescricao( @PathVariable String descricao ) {
        return service.buscarPorDescricao( descricao );
    }

    @GetMapping( value = "/todos-desc/{descricao}")
    @ResponseBody
    public List<ItemDTO> trazerTodosPorDescricao( @PathVariable String descricao ) {
        return service.buscarTodosPorDescricao( descricao );
    }

    @GetMapping( value = "/todos-descs/{descricoes}")
    @ResponseBody
    public List<ItemDTO> trazerTodosPorEssasDescricoes( @PathVariable List<String> descricoes ) {
        return service.buscarTodosPorEssasDescricoes( descricoes );
    }
}
