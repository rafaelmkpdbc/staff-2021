package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;

public class ElfoDTO extends PersonagemDTO {
    private String nome;
    private Integer experiencia;
    private Double dano;
    private Integer expPorAtk;
    private InventarioDTO inventario;

    protected ElfoDTO(){}

    public ElfoDTO( ElfoEntity elfo ) {
        this.nome = elfo.getNome();
        this.experiencia = elfo.getExperiencia();
        this.dano = elfo.getDano();
        this.expPorAtk = elfo.getExpPorAtk();
        this.inventario = elfo.getInventario() != null ?
                new InventarioDTO( elfo.getInventario() ) :
                null;    }

    public ElfoEntity converter() {
        ElfoEntity convertido = new ElfoEntity();
        convertido.setNome( this.nome );
        convertido.setExperiencia( this.experiencia );
        convertido.setDano( this.dano );
        convertido.setExpPorAtk( this.expPorAtk );
        if( this.inventario != null ){
            convertido.setInventario( this.inventario.converter() );
        }
        return convertido;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia( Integer experiencia ) {
        this.experiencia = experiencia;
    }

    public Double getDano() {
        return dano;
    }

    public void setDano( Double dano ) {
        this.dano = dano;
    }

    public Integer getExpPorAtk() {
        return expPorAtk;
    }

    public void setExpPorAtk( Integer expPorAtk ) {
        this.expPorAtk = expPorAtk;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario( InventarioDTO inventario ) {
        this.inventario = inventario;
    }
}
