package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping( value = "/api/personagem/anao")
public class AnaoController {
    @Autowired
    private AnaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosAnoes() {
        return service.buscarTodosAnoes();
    }

    @PostMapping( value = "/salvar" )
    @ResponseBody
    public AnaoDTO salvarAnao( @RequestBody AnaoDTO anao ) {
        return service.salvarAnao( anao.converter() );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AnaoDTO editarAnao( @RequestBody AnaoDTO anao, @PathVariable Integer id ) {
        return service.editarAnao( anao.converter(), id );
    }

    @GetMapping( value = "/{ ids }" )
    @ResponseBody
    public List<AnaoDTO> buscarPorTodosEssesIds( @PathVariable List<Integer> ids ) {
        return service.buscarTodosAnoesPorEssesIds( ids );
    }

    @GetMapping( value = "/todos/{ nome }" )
    @ResponseBody
    public AnaoDTO buscarPorNome( @PathVariable String nome ) {
        return service.buscarAnaoPorNome( nome );
    }

    @GetMapping( value = "/{ nomes }" )
    @ResponseBody
    public List<AnaoDTO> buscarPorTodosEssesNomes( @PathVariable List<String> nomes ) {
        return service.buscarTodosAnoesPorEssesNomes( nomes );
    }

    @GetMapping( value = "/{ exp }" )
    @ResponseBody
    public AnaoDTO buscarPorExperiencia( @PathVariable Integer exp ) {
        return service.buscarAnaoPorExperiencia( exp );
    }

    @GetMapping( value = "/todos/{ exp }")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorExp( @PathVariable Integer exp ) {
        return service.buscarTodosAnoesPorExeperiencia( exp );
    }

    @GetMapping( value = "/{ exps }" )
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorEssesExp( @PathVariable List<Integer> exps ) {
        return service.buscarTodosAnoesPorEssasExperiencias( exps );
    }

    @GetMapping( value = "/{ vida }" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorVida( @PathVariable Double vida ) {
        return service.buscarAnaoPorVida( vida );
    }

    @GetMapping( value = "/todos/{ vida }" )
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorVida( @PathVariable Double vida ) {
        return service.buscarTodosAnoesPorVida( vida );
    }

    @GetMapping( value = "/{ vidas }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorEssasVidas( @PathVariable List<Double> vidas ) {
        return service.buscarTodosAnoesPorEssasVidas( vidas );
    }

    @GetMapping( value = "/{ dano }" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorDano( @PathVariable Double dano ) {
        return service.buscarAnaoPorDano( dano );
    }

    @GetMapping( value = "/todos/{ dano }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorDano( @PathVariable Double dano ) {
        return service.buscarTodosAnoesPorDano( dano );
    }

    @GetMapping( value = "/{ danos }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorEssesDanos( List<Double> danos ) {
        return service.buscarTodosAnoesPorEssesDanos( danos );
    }

    @GetMapping( value = "/{ expAtk }" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorExpAtk( @PathVariable Integer expAtk ) {
        return service.buscarAnaoPorAtk( expAtk );
    }

    @GetMapping( value = "/todos/{ expAtk }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorExpAtk( @PathVariable Integer expAtk ) {
        return service.buscarTodosAnoesPorExpAtk( expAtk );
    }

    @GetMapping( value = "/{ expAtks }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorEssasExpAtk( @PathVariable List<Integer> expAtks ) {
        return service.buscarTodosAnoesPorEssasExpAtk( expAtks );
    }

    @GetMapping( value = "/{ status }" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorStatus( @PathVariable StatusEnum status ) {
        return service.buscarAnaoPorStatus( status );
    }

    @GetMapping( value = "/todos/{ status }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorStatus( @PathVariable String status ) {
        return service.buscarTodosAnoesPorStatus( StatusEnum.valueOf(status) );
    }

    @GetMapping( value = "/{ statuses }" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorEssesStatuses( @PathVariable List<String> statuses ) {
        List<StatusEnum> statusEn = new ArrayList<>();
        for( String status: statuses ) {
            statusEn.add( StatusEnum.valueOf( status ) );
        }
        return service.buscarTodosAnoesPorEssesStatus( statusEn );
    }

    @GetMapping( value = "/{ inventario }" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorInventario( @PathVariable InventarioDTO inventario ) {
        return service.buscarAnaoPorInventario( inventario );
    }
}
