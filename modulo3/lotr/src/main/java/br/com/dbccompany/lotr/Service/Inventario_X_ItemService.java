package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventarioItem;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository repository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InventarioRepository invRepository;

    public List<Inventario_X_ItemDTO> trazerTodosOsItensInventarios() {
        return this.transformarListaEmDTO( repository.findAll() );
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemDTO salvar( Inventario_X_ItemEntity inventarioItem ) throws ArgumentosInvalidosInventarioItem {
        try{
            Inventario_X_ItemEntity salvo = this.salvarEEditar( inventarioItem );
            return new Inventario_X_ItemDTO( salvo );
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosInventarioItem();
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemDTO editar( Inventario_X_ItemEntity inventarioItem, Inventario_X_ItemId id ) throws ArgumentosInvalidosInventarioItem {
        try {
            inventarioItem.setId( id );
            Inventario_X_ItemEntity editado = this.salvarEEditar( inventarioItem );
            return new Inventario_X_ItemDTO( editado );
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosInventarioItem();
        }
    }

    private Inventario_X_ItemEntity salvarEEditar( Inventario_X_ItemEntity inventarioItem ){
        Integer idItem = inventarioItem.getId().getId_item();
        ItemEntity item = itemRepository.findById( idItem ).orElse(null);
        Integer idInventario = inventarioItem.getId().getId_inventario();
        InventarioEntity inventario = invRepository.findById( idInventario ).orElse(null);
        inventarioItem.setItem(item);
        inventarioItem.setInventario(inventario);
        return repository.save( inventarioItem );
    }

    public Inventario_X_ItemDTO buscarPorId( Inventario_X_ItemId id ) throws InventarioItemNaoEncontrado {
        Optional<Inventario_X_ItemEntity> itemInventario = repository.findById(id);
        if(!itemInventario.isPresent()) {
            throw new InventarioItemNaoEncontrado();
        }
        return new Inventario_X_ItemDTO( itemInventario.orElse(new Inventario_X_ItemEntity() ) );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorEssesIds( List<Inventario_X_ItemId> ids ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByIdIn( ids );
        return this.transformarListaEmDTO( encontrados );
    }

    public Inventario_X_ItemDTO buscarPorQuantidade( Integer qtd ) {
        Inventario_X_ItemEntity inventarioItem = repository.findByQuantidade( qtd );
        return new Inventario_X_ItemDTO( inventarioItem );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidade( Integer qtd ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByQuantidade( qtd );
        return this.transformarListaEmDTO( encontrados );
    }

    public List<Inventario_X_ItemDTO> buscarTodasEssasQuantidades( List<Integer> qtds ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByQuantidadeIn( qtds );
        return this.transformarListaEmDTO( encontrados );
    }

    public Inventario_X_ItemDTO buscarPorInventario( InventarioEntity inventario ) {
        Inventario_X_ItemEntity inventarioItem = repository.findByInventario( inventario );
        return new Inventario_X_ItemDTO( inventarioItem );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventario( InventarioEntity inventario ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByInventario( inventario );
        return this.transformarListaEmDTO( encontrados );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorEssesInventarios( List<InventarioEntity> inventarios ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByInventarioIn( inventarios );
        return this.transformarListaEmDTO( encontrados );
    }

    public Inventario_X_ItemDTO buscarPorItem( ItemEntity item ) {
        Inventario_X_ItemEntity inventarioItem = repository.findByItem( item );
        return new Inventario_X_ItemDTO( inventarioItem );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItem( ItemEntity item ) {
        List<Inventario_X_ItemEntity> encontrados = repository.findAllByItem( item );
        return this.transformarListaEmDTO( encontrados );
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorEssesItens( List<ItemEntity> itens ) {
        return this.transformarListaEmDTO( repository.findAllByItemIn( itens ) );
    }

    private List<Inventario_X_ItemDTO> transformarListaEmDTO( List<Inventario_X_ItemEntity> inventarioItens ) {
        List<Inventario_X_ItemDTO> dtos = new ArrayList<>();
        for( Inventario_X_ItemEntity inventarioItem : inventarioItens ) {
            dtos.add( new Inventario_X_ItemDTO( inventarioItem ) );
        }
        return dtos;
    }
}











