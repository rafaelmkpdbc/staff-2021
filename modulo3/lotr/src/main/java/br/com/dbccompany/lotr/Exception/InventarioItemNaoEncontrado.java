package br.com.dbccompany.lotr.Exception;

public class InventarioItemNaoEncontrado extends InventarioItemException {
    public InventarioItemNaoEncontrado() {
        super("Inventario x Item não encontrado");
    }
}
