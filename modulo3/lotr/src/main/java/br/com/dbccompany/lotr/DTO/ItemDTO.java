package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class ItemDTO {
    private Integer id;
    private String descricao;
    private List<Inventario_X_ItemDTO> inventarios;

    protected ItemDTO(){}

    public ItemDTO( ItemEntity item ) {
        this.id = item.getId();
        this.descricao = item.getDescricao();
        List<Inventario_X_ItemDTO> inventariosDTOs = new ArrayList<>();
        if( item.getInventarioItem() != null ) {
            for ( Inventario_X_ItemEntity ii : item.getInventarioItem() ) {
                inventariosDTOs.add( new Inventario_X_ItemDTO( ii ) );
            }
        }
        this.inventarios = inventariosDTOs;
    }

    public ItemEntity converter(  ) {
        ItemEntity item = new ItemEntity( this.descricao );
        item.setId( this.id );
        return item;
    }

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao( String descricao ) {
        this.descricao = descricao;
    }

    public List<Inventario_X_ItemDTO> getInventarios() {
        return inventarios;
    }

    public void setInventarios( List<Inventario_X_ItemDTO> inventarios ) {
        this.inventarios = inventarios;
    }
}
