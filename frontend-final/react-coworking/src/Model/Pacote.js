export default class Pacote {
  constructor({ id, valor, espacoPacote = [] }) {
    this.id = id;
    this.valor = valor;
    this.espacoPacote = espacoPacote;
  }

  configurarEspacoPacote() {
    if(!this.espacos) {
      return;
    }
    this.espacoPacote = this.espacos.map( espaco => {
      return { 
        pacote: { id: this.id }, 
        espaco: { id: espaco.id },
        tipoContratacao: espaco.tipoContratacao,
        quantidade: parseInt(espaco.qtd),
        prazo: parseInt(espaco.prazo)
      };
    })
  }
}