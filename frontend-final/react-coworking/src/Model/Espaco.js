export default class Espaco {
  constructor({ id, nome, qtdPessoas, valor, saldoCliente = [] }) {
    this.id = id;
    this.nome = nome;
    this.qtdPessoas = qtdPessoas;
    this.valor = valor;
    this.saldoCliente = saldoCliente;
  }
}