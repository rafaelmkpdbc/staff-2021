export default class Clientes {
  constructor({ id, nome, cpf, dataNascimento, contato =[] }) {
    this.id = id;
    this.nome = nome;
    this.cpf = cpf;
    this.dataNascimento = dataNascimento;
    this.contato = contato;
  }
}