import React, { Component } from 'react';
import ContentMeu from '../ContentMeu';

export default class ContentContratacao extends Component {
  render() {
    const { children } = this.props;

    return (
      <ContentMeu>
        { children }
      </ContentMeu>
    )
  }
}
