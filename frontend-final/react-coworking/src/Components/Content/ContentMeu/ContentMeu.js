import React from 'react';
import { Layout } from 'antd';
import './ContentMeu.css';

export default function ContentMeu(props) {
  const { children } = props;
  const { Content } = Layout;
  
  return (
    <Content >
      <div className="content">
        <div className="content-wrapper">
          { children }
        </div>
      </div>
    </Content>
  )
}
