import React, { Component } from 'react';
import './pacoteDetail.css';


export default class PacoteDetail extends Component {
  render() {
    const { id, valor, espacoPacote } = this.props.pacote;
    return (
      <div className="pacote-detail">
        <h4>{`Pacote ${id}`}</h4>
        <p>{`Valor: ${valor}`}</p>
        <p>{`Espaços:`}</p>
        {  espacoPacote.map( ep => {
          return ep.espaco && 
            <div key={ep.espaco.id}>
              <p>{`${ep.espaco.nome}: ${ep.espaco.qtdPessoas} pessoas`}</p>
            </div>
        } ) }
      </div>
    )
  }
}
