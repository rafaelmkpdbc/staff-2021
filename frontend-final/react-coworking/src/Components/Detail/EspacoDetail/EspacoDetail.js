import React, { Component } from 'react';
import './espacoDetail.css';

export default class EspacoDetail extends Component {
  render() {
    const { nome, qtdPessoas, valor } = this.props.espaco;

    return (
      <div className="espaco-detail">
        <h4>{nome}</h4>
        <p>{`Quantidade de pessoas: ${qtdPessoas}`}</p>
        <p>{`Valor: ${valor}`}</p>
      </div>
    )
  }
}
