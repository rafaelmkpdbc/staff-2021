import React, { Component } from 'react'

export default class ClienteDetail extends Component {
  render() {
    const { nome, cpf, contato, dataNascimento } = this.props.cliente;
    
    return (
      <div className="cliente-detail">
        <h4>{nome}</h4>
        <p>{cpf}</p>
        <p>{dataNascimento}</p>
        { contato.map( contato => {
          return <p key={contato.valor}>{`${contato.tipoContato}: ${contato.valor}`}</p>
        } ) }
      </div>
    )
  }
}
