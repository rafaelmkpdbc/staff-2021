import React, { Component } from 'react';
import { List } from 'antd';

export default class ListaClientes extends Component {
  constructor(props) {
    super(props)
    this.selectItem = this.selectItem.bind(this);
  }
  
  selectItem(evt) {
    this.props.onClick(evt);
  }

  render() {
    return (
      <List
        dataSource={ this.props.data }
        renderItem={ item => (
          <List.Item key={item.id} onClick={() => this.selectItem(item)}>
            <List.Item.Meta 
              title={item.nome}
              description={`${item.cpf}`}
            />
          </List.Item> 
        )}
      >
      </List>
    )
  }
}
