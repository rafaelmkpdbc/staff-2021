import React, { Component } from 'react';
import { List } from 'antd';

import './listaPacotes.css';

export default class ListaPacotes extends Component {
  constructor(props) {
    super(props)
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem(evt) {
    this.props.onClick(evt);
  }

  render() {
    return (
      <List
      dataSource={ this.props.data }
      renderItem={ pacote => (
        <List.Item key={pacote.id} onClick={() => this.selectItem(pacote)}>
          <List.Item.Meta className="item-meta"
            title={`Pacote ${pacote.id}`}
            description={`Valor: ${pacote.valor}`}
          />
          <div className="pacotes-espacos-wrapper">
            <h4>Espaços:</h4>
            { pacote.espacoPacote.map( ep => {
                return ep.espaco && <span 
                  className="pacotes-espacos-desc" 
                  key={ep.espaco.id}
                >
                  {`${ep.espaco.nome} - ${ep.espaco.qtdPessoas} pessoas`}
                </span>
              })
            }
          </div>
        </List.Item> 
      )}
      >
      </List>
    )
  }
}
