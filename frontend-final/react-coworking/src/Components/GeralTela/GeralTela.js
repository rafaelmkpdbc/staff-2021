import React from 'react'
import { Link } from 'react-router-dom';
import HeaderMeu from '../HeaderMeu';
import LayoutMeu from '../LayoutMeu';
import MenuMeu from '../MenuMeu';
import MenuItemMeu from '../MenuItemMeu';
import SiderMeu from '../SiderMeu';
import SubMenuMeu from '../SubMenuMeu';
import 'antd/dist/antd.css';
import { logout } from '../../Helpers/auth';
import carregarDadosTeste from '../../Api/baseTeste';


export default function GeralTela(props) {
  const { children } = props;

  return (
    <React.Fragment>
      <LayoutMeu>
        <HeaderMeu >
          <Link to="/" className="home-link">
            Coworking
          </Link>
          <div className="header-buttons-wrapper">
            <Link to={ { pathname: '/login', state: { from: props.location } } } >
              <button className="button-header" type="button" onClick={logout}>Logout</button>
            </Link>
            <button className="button-header" type="button" onClick={carregarDadosTeste}>Lancar Teste</button>
          </div>
        </HeaderMeu>
      </LayoutMeu>
      <LayoutMeu>
        <SiderMeu >
          <MenuMeu  mode="inline" rootKeys={['sub1', 'sub2', 'sub3', 'sub4', 'sub5']} >
            <SubMenuMeu key="sub1" title="Clientes">
              <MenuItemMeu key="1"><Link to='/clientes'>Listar clientes</Link></MenuItemMeu>
              <MenuItemMeu key="2">Novo cliente</MenuItemMeu>
            </SubMenuMeu> 
            <SubMenuMeu key="sub2" title="Espaços">
              <MenuItemMeu key="3"><Link to='/espacos'>Listar espaços</Link></MenuItemMeu>
              <MenuItemMeu key="4">Novo espaço</MenuItemMeu>
            </SubMenuMeu> 
            <SubMenuMeu key="sub3" title="Pacotes">
              <MenuItemMeu key="5"><Link to='/pacotes'>Listar pacotes</Link></MenuItemMeu>
              <MenuItemMeu key="6">Novo pacote</MenuItemMeu>
            </SubMenuMeu>
            <SubMenuMeu key="sub4" title="Acessos">
              <MenuItemMeu key="7"><Link to='/acessos'>Listar acessos</Link></MenuItemMeu>
              <MenuItemMeu key="8">Novo Acesso</MenuItemMeu>
            </SubMenuMeu>
            <SubMenuMeu key="sub5" title="Contratação">
              <MenuItemMeu key="9"><Link to='/contratacao'>Nova contratação</Link></MenuItemMeu>
            </SubMenuMeu>
          </MenuMeu>
        </SiderMeu>
        { children }
      </LayoutMeu>
    </React.Fragment>
  )
}
