import React, { Component } from 'react';
import './SearchCliente.css';

export default class SearchCliente extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
       encontrados: [],
       clientes: props.clientes,
       filter: ''
    }
  }
  
  handleChange = (evt) => {
    this.setState(state => { return { ...state, filter: evt.target.value }});
    const val = evt.target.value.toLowerCase();
    const encontrados = this.state.clientes.filter( cli => cli.nome.toLowerCase().includes(val) );
    this.setState( state => {
      return { ...state, encontrados }});
  } 

  render() {
    const { encontrados, filter } = this.state;
    const { selecionarCliente } = this.props;

    return (
      <div className="search-cliente">
        <label>
          Selecione o cliente:
          <input type="text" className="input input-search" value={filter} onChange={this.handleChange} />
        </label>
        <ul>
          { encontrados.map( cliente => {
            return <div 
              key={cliente.id}
              className="found-cliente"
              onClick={() => selecionarCliente(cliente)}
            >
              <span>{`${cliente.nome} - CPF ${cliente.cpf}`}</span>
            </div>
          } ) }
        </ul>
      </div>
    )
  }
}
