import React, { Component } from 'react';
import {Button} from 'antd';
import { Link } from 'react-router-dom';
import './botao.css'

export default class Botao extends Component {
  render() {
    const { titulo, onClick, href } =this.props;
    return (
      <Link to={href}>
        <Button type="primary" htmlType="button" onClick={onClick} className='botao' >
          {titulo}
        </Button>
      </Link>
    )
  }
}
