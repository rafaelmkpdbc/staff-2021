import React, { Component } from 'react';
import Espaco from '../../../Model/Espaco'
import './espacoForms.css'

export default class EspacoForms extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      edicao: props.edicao,
      espaco: this.props.espaco
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { nome, qtdPessoas, valor } = this.state;
    const espaco = new Espaco({ nome, qtdPessoas, valor });
    this.props.onSubmit(espaco);
  }

  handleChange= (evt) => {
    this.setState( state => { 
      return { 
        ...state,
        [evt.target.name]: evt.target.value
      }
    });
  }
  
  render() {
    const { espaco } = this.props;

    return (
      <div className="espaco-form">
        <h2>Preencha os dados do espaço</h2>
        <form onSubmit={this.handleSubmit}>
          <label className="espaco-label" htmlFor="nome">
            Nome do espaço: 
            <input className="input" type="text" id="nome" name="nome" onBlur={this.handleChange} />
          </label>
          <label className="espaco-label" htmlFor="qtdPessoas">
            Quantidade de pessoas: 
            <input className="input" type="number" id="qtdPessoas" name="qtdPessoas" onBlur={this.handleChange} />
          </label>
          <label className="espaco-label" htmlFor="valor" >
            Valor do espaço: 
            <input className="input" type="text" id="valor" name="valor" onBlur={this.handleChange} />
          </label>
          <div className="form-botao">
            <input className="button" type="submit" value="Salvar" />
          </div>
        </form>
      </div>
    )
  }
}
