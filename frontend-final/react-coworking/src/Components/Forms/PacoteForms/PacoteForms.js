import React, { Component } from 'react';
import Pacote from '../../../Model/Pacote';
import Espaco from '../../../Model/Espaco';
import EspacoApi from '../../../Api/EspacoApi';
import '../forms.css'
import './pacoteForms.css';

export default class PacoteForms extends Component {
  constructor(props) {
    super(props);
    this.espacoApi = new EspacoApi();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selecionarEspaco = this.selecionarEspaco.bind(this);
    this.carregarEspacos = this.carregarEspacos.bind(this);
    this.handleValorChange = this.handleValorChange.bind(this);
    this.handlePrazoChange = this.handlePrazoChange.bind(this);
    this.handleTipoChange = this.handleTipoChange.bind(this);
    this.handleQtdChange = this.handleQtdChange.bind(this);
    this.state = {
      edicao: props.edicao,
      pacote: this.props.pacote,
      espacos: [],
      espacosSelecionados: []
    }
  }

  componentDidMount() {
    this.carregarEspacos();
  }

  carregarEspacos = async () => {
    const data = await this.espacoApi.buscarTodosEspacos().then(res => res.data);
    const espacos = data.map( espaco => new Espaco(espaco) );
    this.setState( state => { return { ...state, espacos }});
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { espacosSelecionados, valor } = this.state;
    const pacote = new Pacote({ valor: valor });
    pacote.espacos = espacosSelecionados;
    this.props.onSubmit(pacote);
  }

  handleValorChange = (evt) => {
    this.setState( state => { 
      return { 
        ...state,
        [evt.target.name]: evt.target.value
      }
    });
  }

  handleTipoChange = (espaco, evt) => {
    espaco.tipoContratacao = evt.target.value;
  }

  handleQtdChange = (espaco, evt) => {
    espaco.qtd = evt.target.value;
  }

  handlePrazoChange = (espaco, evt) => {
    espaco.prazo = evt.target.value;
  }

  selecionarEspaco = (espaco) => {
    espaco.tipoContratacao = "DIARIA";
    const espacos = this.state.espacos.filter( esp => esp.id !== espaco.id );
    const espacosSelecionados = this.state.espacosSelecionados;
    espacosSelecionados.push(espaco);
    this.setState( state => { 
      return { 
        ...state, 
        espacos,
        espacosSelecionados
      }}
    );
  }

  retirarEspaco = (espaco) => {
    const espacosSelecionados = this.state.espacosSelecionados.filter( esp => esp.id !== espaco.id );
    const espacos = this.state.espacos;
    espacos.push(espaco);
    this.setState( state => { 
      return { 
        ...state, 
        espacos,
        espacosSelecionados
      }}
    );
  }

  render() {
    const { pacote } = this.props;
    const { espacos, espacosSelecionados } = this.state;

    return (
      <div className="pacote-form">
        <h2>Preencha os dados do pacote</h2>
        <div className="form-wrapper">
          <form onSubmit={this.handleSubmit} className="form-pacote">
            <div className="form-pacote-data">
              <label className="pacote-label" >
                Valor do pacote: 
                <input type="text" className="input input-pacote" name="valor" onBlur={this.handleValorChange} />
              </label>
            </div>
            <div className="espacos-selecionados">
              <h4>Espaços selecionados</h4>
              {
                espacosSelecionados.map( espaco => {
                  return <div 
                    key={espaco.id}
                    className="espaco-desc"
                  >
                    <span onClick={() => this.retirarEspaco(espaco)}>{`${espaco.nome} `}</span>
                    <div>
                      <select name={`tipo-${espaco.id}`} defaultValue="DIARIA" onBlur={(evt) => this.handleTipoChange(espaco, evt)}>
                        <option value="MINUTO">Minuto</option>
                        <option value="HORA">Hora</option>
                        <option value="TURNO">Turno</option>
                        <option value="DIARIA">Diária</option>
                        <option value="SEMANA">Semana</option>
                        <option value="MES">Mês</option>
                      </select>
                      <input 
                        type="number" 
                        name={`qtd-${espaco.id}`} 
                        onBlur={(evt) => this.handleQtdChange(espaco, evt)}
                        className="input input-pacote-qtd" 
                        placeholder="qtd" 
                        min="0" 
                        required/>
                      <input 
                        type="number" 
                        name={`prazo-${espaco.id}`} 
                        onBlur={(evt) => this.handlePrazoChange(espaco, evt)}
                        className="input input-pacote-qtd" 
                        placeholder="prazo" 
                        min="0" 
                        required/>
                    </div>
                  </div>
                })
              }
            </div>
            <div className="form-botao">
              <input className="button" type="submit" value="Salvar" />
            </div>
          </form>
          <div className="select-espacos-wrapper">
            <h4>Espaços disponíveis</h4>
            {
              espacos.map( espaco => {
                return <div 
                  key={espaco.id}
                  className="espaco-desc"
                  onClick={() => this.selecionarEspaco(espaco)}
                >
                  <span>{`${espaco.nome} - ${espaco.qtdPessoas} pessoas`}</span>
                </div>
              })
            }
          </div>
        </div>
      </div>
    )
  }
}
