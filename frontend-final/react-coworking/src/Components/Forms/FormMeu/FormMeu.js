import React, { Component } from 'react'
import { Form } from 'antd';

export default class FormMeu extends Component {
  
  render() {
    const { name, labelCol, wrapperCol, onFinish, onFinishFailed, children } = this.props;

    return (
      <Form  
        name={name} 
        labelCol={{span: labelCol}} 
        wrapperCol={{span: wrapperCol}} 
        initialValues={{remember: true}} 
        onFinish={ onFinish }
        onFinishFailed={ onFinishFailed }
      >
        { children }
      </Form>
    )
  }
}
