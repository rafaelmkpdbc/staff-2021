import React, { Component } from 'react';
import { Button } from 'antd';

export default class Submit extends Component {
  render() {
    const { titulo } = this.props;

    return (
      <Button type="primary" htmlType="submit" >
        { titulo }
      </Button>
    )
  }
}
