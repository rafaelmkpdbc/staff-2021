import React, { Component } from 'react'
import { Form, Input } from 'antd';

export default class FormItemPsswd extends Component {

  handleChange(evt) {
    console.log(evt);
  }

  render() {
    const { label, name, message } = this.props;

    return (
      <Form.Item 
        label={label} 
        name={name} 
        rules={[{ required: true, messsage: {message} }]} 
      >
        <Input.Password  />
      </Form.Item>
    )
  }
}
