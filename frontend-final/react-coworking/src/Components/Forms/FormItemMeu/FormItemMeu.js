import React, { Component } from 'react';
import { Form, Input } from 'antd';

export default class FormItemMeu extends Component {
  constructor(props) {
    super(props);
    this.state = {
       value: this.props.value || ''
    };
  }

  render() {
    const { label, name, required, message } = this.props;
    const { value } = this.state;
    return (
      <Form.Item 
        label={label} 
        name={name} 
        rules={[{ required: required, message: message }]} 
      >
        <Input  
          type="text"
          value={value} 
        />
      </Form.Item>
    )
  }
}
