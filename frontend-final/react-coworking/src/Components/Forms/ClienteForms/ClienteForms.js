import React, { Component } from 'react'
import Clientes from '../../../Model/Cliente';
import './clienteForm.css';
import '../forms.css'

export default class ClienteForms extends Component {
  constructor(props) {
    super(props)
    this.handleNomeChange = this.handleNomeChange.bind(this);
    this.handleCpfChange = this.handleCpfChange.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
    this.handleTelChange = this.handleTelChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      edicao: this.props.edicao,
      cliente: this.props.cliente || new Clientes({})
    }
  }

  handleNomeChange = (evt) => {
    const cliente = this.state.cliente;
    cliente.nome = evt.target.value;
    this.setState( state => { return { ...state, cliente } } );
  }

  handleCpfChange = (evt) => {
    const cliente = this.state.cliente;
    cliente.cpf = evt.target.value;
    this.setState( state => { return { ...state, cliente } } );
  }
  
  handleDataChange = (evt) => {
    const cliente = this.state.cliente;
    const dataPieces = evt.target.value.split('-');
    const data = `${dataPieces[2]}/${dataPieces[1]}/${dataPieces[0]}`;
    cliente.dataNascimento = data;
    this.setState( state => { return { ...state, cliente } } );
  }
  
  handleTelChange = (evt) => {
    const cliente = this.state.cliente;
    cliente.contato.push({ tipoContato: "telefone", valor: evt.target.value });
    this.setState( state => { return { ...state, cliente } } );
  }
  
  handleEmailChange = (evt) => {
    const cliente = this.state.cliente;
    cliente.contato.push({ tipoContato: "email", valor: evt.target.value });
    this.setState( state => { return { ...state, cliente } } );
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    console.log(this.state.cliente);
    this.props.onSubmit(this.state.cliente);
  }

  render() {
    const { edicao } = this.props;

    return (
      <React.Fragment>
        <h2>Preencha os dados do cliente</h2>
        <form onSubmit={this.handleSubmit} className="form">
          <div className="form-half">
            <label className="cliente-label" >Nome do cliente: 
              <input className="input" type="text" name="nome" onBlur={this.handleNomeChange}/>
            </label>
            <label className="cliente-label" >CPF: 
              <input className="input" type="text" name="cpf" onBlur={this.handleCpfChange}/>
            </label>
            <label className="cliente-label" >Data nascimento: 
              <input className="input" type="date" name="dataNascimento"  onBlur={this.handleDataChange}/>
            </label>
          </div>
          <div className="form-half">
          <label className="cliente-label" >Telefone: 
              <input className="input" type="text" name="telefone" />
            </label>
          <label className="cliente-label" >Email: 
              <input className="input" type="email" name="email" />
            </label>
          </div>
          <div className="form-botao">
            <input className="ant-btn-primary button" type="submit" value={edicao ? 'Salvar dados' : 'Cadastrar'} />
          </div>
        </form>
      </React.Fragment>
    )
  }
}
