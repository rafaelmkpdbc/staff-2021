import React from 'react'
import { Layout } from 'antd';
import './HeaderMeu.css';

export default function HeaderMeu(props) {
  const { Header } = Layout;
  const { children } = props;

  return (
    <Header>
      {children}
    </Header>
  )
}
