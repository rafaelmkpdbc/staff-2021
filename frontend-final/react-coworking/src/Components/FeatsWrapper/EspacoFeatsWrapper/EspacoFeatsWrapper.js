import React, { Component } from 'react'
import Botao from '../../Botao/Botao';
import FeatsWrapper from '../FeatsWrapper/FeatsWrapper';
import EspacoDetail from '../../Detail/EspacoDetail';
import EspacoForms from '../../Forms/EspacoForms';

export default class EspacoFeatsWrapper extends Component {
  constructor(props) {
    super(props)
    this.toggleEdicao = this.toggleEdicao.bind(this);
    this.state = {
       edicao: this.props.edicao
    }
  }

  toggleEdicao() {
    this.setState( state => { return { ...state, edicao: !this.state.edicao } } );
  }

  render() {
    const { espaco, close, onSubmit } = this.props;
    const { edicao } = this.state;

    return (
      <FeatsWrapper>
        { espaco  && !edicao && <EspacoDetail espaco={espaco} edicao={edicao} /> }
        { espaco && !edicao && <Botao titulo="editar" onClick={this.toggleEdicao} /> }
        { edicao && <EspacoForms  espaco={espaco} edicao={edicao} onSubmit={onSubmit}/> }
        <Botao titulo='fechar' onClick={close} />
      </FeatsWrapper>
    )
  }
}
