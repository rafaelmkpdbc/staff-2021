import React, { Component } from 'react';
import FeatsWrapper from '../FeatsWrapper/FeatsWrapper';

export default class ContratacaoFeatsWrapper extends Component {
  render() {
    const { children } = this.props;

    return (
      <FeatsWrapper>
        { children }
      </FeatsWrapper>
    )
  }
}
