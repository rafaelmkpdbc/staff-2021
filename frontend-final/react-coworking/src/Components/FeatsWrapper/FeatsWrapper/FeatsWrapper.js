import React, { Component } from 'react';
import './featsWrapper.css'

export default class FeatsWrapper extends Component {
  
  render() {
    const { children } = this.props;

    return (
      <div className="feat-wrapper">{children}</div>
    )
  }
}
