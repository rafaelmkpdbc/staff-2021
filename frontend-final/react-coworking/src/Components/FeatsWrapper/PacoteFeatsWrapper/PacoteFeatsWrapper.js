import React, { Component } from 'react';
import Botao from '../../Botao';
import FeatsWrapper from '../FeatsWrapper';
import PacoteDetail from '../../Detail/PacoteDetail';
import PacoteForms from '../../Forms/PacoteForms';

export default class PacoteFeatsWrapper extends Component {
  constructor(props) {
    super(props)
    this.toggleEdicao = this.toggleEdicao.bind(this);
    this.state = {
       edicao: this.props.edicao
    }
  }

  toggleEdicao() {
    this.setState( state => { return { ...state, edicao: !this.state.edicao } } );
  }

  render() {
    const { pacote, close, onSubmit } = this.props;
    const { edicao } = this.state;

    return (
      <FeatsWrapper>
        { pacote && !edicao && <PacoteDetail pacote={pacote} edicao={edicao} /> }
        { pacote && !edicao && <Botao titulo="editar" onClick={this.toggleEdicao} /> }
        { edicao && <PacoteForms  pacote={pacote} edicao={edicao} onSubmit={onSubmit}/> }
        <Botao titulo='fechar' onClick={close} />
      </FeatsWrapper>
    )
  }
}
