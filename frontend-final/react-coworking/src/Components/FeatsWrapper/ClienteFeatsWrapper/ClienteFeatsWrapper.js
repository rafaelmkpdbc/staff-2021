import React, { Component } from 'react'
import Botao from '../../Botao/Botao';
import ClienteDetail from '../../Detail/ClienteDetail';
import ClienteForms from '../../Forms/ClienteForms/ClienteForms';
import FeatsWrapper from '../FeatsWrapper/FeatsWrapper'

export default class ClienteFeatsWrapper extends Component {
  constructor(props) {
    super(props);
    this.toggleEdicao = this.toggleEdicao.bind(this);
    this.state = {
       edicao: this.props.edicao,
    }
  }
  
  toggleEdicao() {
    this.setState( state => { return { ...state, edicao: !this.state.edicao }} );
  }

  render() {
    const { cliente, close, onSubmit } = this.props;
    const { edicao } = this.state;

    return (
      <FeatsWrapper >
        { cliente && !edicao && <ClienteDetail cliente={cliente} /> }
        { cliente && !edicao && <Botao titulo="editar" onClick={this.toggleEdicao}/> }
        { edicao && <ClienteForms  cliente={cliente} edicao={edicao} onSubmit={onSubmit} /> }
        <Botao titulo='fechar' onClick={ close } />
      </FeatsWrapper>
    )
  }
}
