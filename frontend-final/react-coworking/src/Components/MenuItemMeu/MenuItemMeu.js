import React from 'react';
import { Menu } from 'antd';

export default function MenuItemMeu( props ) {
  const { children, eventKey, onClick } = props;

  return (
    <Menu.Item eventKey={eventKey} onClick={onClick} >
      { children }
    </Menu.Item>
  )
}
