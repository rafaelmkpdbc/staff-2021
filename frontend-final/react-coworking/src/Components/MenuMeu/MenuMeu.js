import React, { useState } from 'react';
import { Menu } from 'antd';

export default function MenuMeu(props) {
  const { children, mode, theme, defaultSelectedKeys, defaultOpenKeys } = props;
  const { rootKeys } = props;

  const [ openKeys, setOpenKeys ] = useState([]);

  const onOpenChange = keys => {
    const latestOpenKey = keys.find( key => openKeys.indexOf( key ) === -1 );
    if ( rootKeys.indexOf( latestOpenKey ) === -1 ) {
      setOpenKeys( keys );
    } else {
      setOpenKeys( latestOpenKey ? [latestOpenKey] : [] );
    }
  }

  return (
    <Menu 
      mode={ mode } 
      theme={theme} 
      defaultSelectedKeys={ defaultSelectedKeys } 
      defaultOpenKeys={ defaultOpenKeys }
      openKeys={openKeys}
      onOpenChange={onOpenChange}
    >
      { children }
    </Menu>
  )
}
