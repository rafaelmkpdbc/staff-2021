import React from 'react'
import { Layout } from 'antd';
import './SiderMeu.css';

export default function SiderMeu(props) {
  const { children } = props;
  const { Sider } = Layout;

  return (
    <Sider 
      collapsible 
      breakpoint="md"
      collapsedWidth='0'
      defaultCollapsed={false} 
      className={`site-layout-background`}
    >
      { children }
    </Sider>
  )
}
