import React from 'react';
import { Link } from 'react-router-dom';
import './mainCard.css'

export default function MainCard(props) {
  const { children, href } = props;

  return (
    <React.Fragment >
      { href ? 
        <Link className="card" to={{ pathname:`${href}` , state: { } }}>
          { children }
        </Link> :
        <div className="card">
          { children }
        </div>
      }
    </React.Fragment>
  )
}
