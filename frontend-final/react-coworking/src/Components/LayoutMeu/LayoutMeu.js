import React from 'react';
import { Layout } from 'antd';

export default function LayoutMeu(props) {
  const { children } = props;
  
  return (
    <Layout>
      {children}
    </Layout>
  )
}