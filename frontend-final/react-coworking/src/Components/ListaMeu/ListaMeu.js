import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import './ListaMeu.css';

export default class ListaMeu extends Component {
  constructor(props) {
    super(props)
    this.state = {
       data: this.props.data,
    }
  }

  handleInfiniteData = () => {
    let { data } = this.state;
    this.setState( state => {
      return {...state, loading: true }
    });
    if (data.length > 14) {
      console.log('Infinite List loaded all');
      this.setState({
        hasMore: false,
        loading: false,
      });
      return;
    }
  }

  selectItem(evt) {
    this.props.onClick(evt);
  }

  render() {
    const { children } = this.props;

    return (
      <div className="infinite-container">
        <InfiniteScroll
          initialLoad={true}
          useWindow={false}
          loadMore={this.handleInfiniteData}
        >
          {children}
        </InfiniteScroll>
      </div>
    )
  }
}
