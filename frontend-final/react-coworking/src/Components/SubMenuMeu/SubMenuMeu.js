import React from 'react'
import { Menu } from 'antd';

export default function SubMenuMeu(props) {
  const { SubMenu } = Menu;
  const { children, eventKey, icon, title } = props;

  return (
    <SubMenu eventKey={eventKey} icon={icon} title={title} >
      { children }
    </SubMenu>
  )
}
