import React, { Component } from 'react';
import { List } from 'antd';

export default class ListaEspacos extends Component {
  constructor(props) {
    super(props)
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem(evt) {
    this.props.onClick(evt);
  }
  
  render() {
    return (
      <List
        dataSource={ this.props.data }
        renderItem={ espaco => (
          <List.Item key={espaco.id} onClick={() => this.selectItem(espaco)}>
            <List.Item.Meta 
              title={espaco.nome}
              description={`Valor: ${espaco.valor}`}
            />
            <div>
              <span>{`Espaco para ${espaco.qtdPessoas} pessoas`}</span>
            </div>
          </List.Item> 
        )}
      >
      </List>
    )
  }
}
