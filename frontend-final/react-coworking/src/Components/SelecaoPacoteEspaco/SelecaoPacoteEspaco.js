import React, { Component } from 'react';
import Botao from '../Botao';

export default class SelecaoPacoteEspaco extends Component {
  constructor(props) {
    super(props)
    this.listarEspacos = this.listarEspacos.bind(this);
    this.listarPacotes = this.listarPacotes.bind(this);
    this.fecharLista = this.fecharLista.bind(this);
    this.state = {
      mostrarEspacos: false,
      mostrarPacotes: false,
    }
  }

  listarEspacos = () => {
    this.setState(state => { return { ...state, mostrarEspacos: true, mostrarPacotes: false }});
  }

  listarPacotes = () => {
    this.setState(state => { return { ...state, mostrarEspacos: false, mostrarPacotes: true }});
  }

  fecharLista = () => {
    this.setState(state => { return { ...state, mostrarEspacos: false, mostrarPacotes: false }});
  }
  
  render() {
    const { mostrarEspacos, mostrarPacotes } = this.state;
    const { pacotes, espacos, selecionarPacote, selecionarEspaco } = this.props;

    return (
      <React.Fragment>
        {!mostrarEspacos && !mostrarPacotes && <div className="contratacao-escolha">
          <div className="escolha-espaco" onClick={this.listarEspacos}><h3>Espaço</h3></div> 
          <div className="escolha-pacote" onClick={this.listarPacotes}><h3>Pacote</h3></div>
        </div>}
        { mostrarPacotes && 
          <div>
            <ul>
              { pacotes.map( pacote => {
                return <div 
                key={pacote.id}
                onClick={() => selecionarPacote(pacote)}
                >
                  <span>{`Pacote ${pacote.id} - ${pacote.valor}`}</span>
                </div>
              })}
            </ul>
            <Botao titulo="fechar" onClick={this.fecharLista}/>
          </div>
        }
        { mostrarEspacos && 
          <div>
            <ul>
              { espacos.map( espaco => {
                return <div 
                key={espaco.id}
                onClick={() => selecionarEspaco(espaco)}
                >
                  <span>{`${espaco.nome} - ${espaco.valor}`}</span>
                </div>
              })}
            </ul>
            <Botao titulo="fechar" onClick={this.fecharLista}/>
          </div>
        }
      </React.Fragment>
    )
  }
}
