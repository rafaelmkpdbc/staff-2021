import React from 'react'
import Botao from '../../Components/Botao';
import ContentMeu from '../../Components/Content/ContentMeu';
import './PagamentosTela.css';

export default function PagamentosTela() {
  return (
    <React.Fragment>
      <ContentMeu className="content-pagamento">
        <div className="content-pagamento">
          <h2>Tela ainda em desenvolvimento...</h2>
          <h2>Pedimos desculpas pelo transtorno</h2>
          <Botao titulo="Voltar à tela inicial" href="/"/>
        </div>
      </ContentMeu>
    </React.Fragment>
  )
}
