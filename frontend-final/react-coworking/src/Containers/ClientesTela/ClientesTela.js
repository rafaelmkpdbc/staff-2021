import React from 'react'
import { useState, useEffect } from 'react';

import './ClientesTela.css';

import ClienteApi from '../../Api/ClienteApi';
import Cliente from '../../Model/Cliente.js';

import ContentCliente from '../../Components/Content/ContentCliente/ContentCliente';
import ClienteFeatsWrapper from '../../Components/FeatsWrapper/ClienteFeatsWrapper';
import ListaMeu from '../../Components/ListaMeu';
import Botao from '../../Components/Botao/Botao';
import ListaClientes from '../../Components/ListaClientes/ListaClientes';

export default function ClientesTela(props) {
  const clienteApi = new ClienteApi();
  
  const [ detail, setDetail ] = useState(false);
  const [ edicao, setEdicao ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ cliente, setCliente ] = useState({});
  
  const fetchData = async () => {
    const clientesData = await clienteApi.buscarTodosClientes().then(res => res.data);
    const clientes = clientesData.map( cli => new Cliente(cli));
    setData(clientes);
  }
  
  useEffect(() => {
    fetchData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);
  
  const fecharDetail = () => {
    setCliente(null);
    setDetail(false);
    setEdicao(false);
  }

  const acessarCliente = (cliente) => {
    setCliente( new Cliente(cliente ) );
    setDetail(true);
  }

  const novoCliente = () => {
    setDetail(true);
    setEdicao(true);
    setCliente(null);
  }

  const salvarCliente = async (cliente) => {
    cliente.id ? 
      await clienteApi.editarCliente(cliente) :
      await clienteApi.salvarCliente(cliente);
    setCliente(null);
    setDetail(false);
    setEdicao(false);
    await fetchData();
  }

  return (
    <React.Fragment>
        <ContentCliente >
          { 
            detail ? 
            <ClienteFeatsWrapper close={fecharDetail} cliente={cliente} edicao={edicao} onSubmit={salvarCliente}/> : 
            data.length > 0 && 
            <ListaMeu>
              <ListaClientes 
                data={data} 
                onClick={acessarCliente} 
              />
            </ListaMeu> 
          }
          { !detail && <Botao titulo="Novo Cliente" onClick={novoCliente}/> }
        </ContentCliente>
    </React.Fragment>
  );
}
