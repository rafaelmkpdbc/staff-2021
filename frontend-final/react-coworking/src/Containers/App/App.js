import GeralTela from '../../Components/GeralTela';
import './App.css';
import React from 'react';
import Rotas from '../Rotas';
import { BrowserRouter } from 'react-router-dom';

function App(props) {
  

  return (
    <BrowserRouter forceRefresh={false} >
      <GeralTela>
        <Rotas />
      </GeralTela> 
    </BrowserRouter>
    );
}

export default App;
