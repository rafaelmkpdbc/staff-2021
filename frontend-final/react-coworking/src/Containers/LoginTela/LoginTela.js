import React from 'react';
import LoginApi from '../../Api/LoginApi';
import Botao from '../../Components/Botao/Botao';
import ContentMeu from '../../Components/Content/ContentMeu';
import FormItemMeu from '../../Components/Forms/FormItemMeu';
import FormItemPsswd from '../../Components/Forms/FormItemPsswd';
import FormMeu from '../../Components/Forms/FormMeu';
import Submit from '../../Components/Forms/Submit';
import { FORMS } from '../../Constants/mensagens';
import { isAuthenticated, logout } from '../../Helpers/auth';
import './LoginTela.css';

export default function LoginTela(props) {
  const loginApi = new LoginApi();

  const onSubmit = async ({username, password} ) => {
    await loginApi.fazerLogin({ username: username, password: password });
    if( isAuthenticated() ) {
      props.history.push('/');
    }
  }
  
  const onSubmitFail = (values) => {
    console.log('submit failed');
    console.log(values);
  }

  return (
    <ContentMeu>
      <FormMeu
        name='login'
        labelCol='56'
        wrapperCol='64'
        onFinish={ onSubmit }
        onFinishFailed={ onSubmitFail }
        >
        <FormItemMeu 
          label='Usuário'
          name='username'
          required={true}
          message={ FORMS.ERRO_USUARIO } 
          />
        <FormItemPsswd
          label='Senha'
          name='password'
          message={ FORMS.ERRO_SENHA }
          />
        <Submit titulo="enviar" />
        <Botao titulo="logout" onClick={logout}/>
      </FormMeu>
    </ContentMeu>
  )
}
