import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import ClienteApi from '../../Api/ClienteApi';
import EspacoApi from '../../Api/EspacoApi';
import PacoteApi from '../../Api/PacoteApi';
import ContratacaoApi from '../../Api/ContratacaoApi';
import Cliente from '../../Model/Cliente';
import Espaco from '../../Model/Espaco';
import Pacote from '../../Model/Pacote';

import ContentContratacao from '../../Components/Content/ContentContratacao';
import ContratacaoFeatsWrapper from '../../Components/FeatsWrapper/ContratacaoFeatsWrapper';
import SearchCliente from '../../Components/SearchCliente/SearchCliente';

import './ContratacaoTela.css';
import '../containers.css'
import SelecaoPacoteEspaco from '../../Components/SelecaoPacoteEspaco';

export default function ContratacaoTela() {
  const clienteApi = new ClienteApi();   
  const espacoApi = new EspacoApi();   
  const pacoteApi = new PacoteApi();
  const contratacaoApi = new ContratacaoApi();

  const [ clientes, setClientes ] = useState();
  const [ espacos, setEspacos ] = useState([]);
  const [ pacotes, setPacotes ] = useState([]);
  const [ cliente, setCliente ] = useState();
  const [ espaco, setEspaco ] = useState();
  const [ pacote, setPacote ] = useState();
  const [ pagamento, setPagamento ] = useState(false);

  useEffect(() => {
    fetchData();
// eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchData = async () => {
    const clientesData = await clienteApi.buscarTodosClientes().then(res => res.data);
    const espacosData = await espacoApi.buscarTodosEspacos().then(res => res.data);
    const pacotesData = await pacoteApi.buscarTodosPacotes().then(res => res.data);
    const clientes = clientesData.map( cli => new Cliente(cli));
    const espacos = espacosData.map( esp => new Espaco(esp));
    const pacotes = pacotesData.map( pct => new Pacote(pct));
    setClientes(clientes);
    setEspacos(espacos);
    setPacotes(pacotes);
    setPacotes(pacotes);
  }

  const salvarContratacao = async (contratacao) => {
    await contratacaoApi.salvarContratacao(contratacao);
  }

  useEffect(() => {
    if( cliente && (pacote || espaco) ) {
      setPagamento(true);
    }
  }, [cliente, espaco, pacote]);

  return (
    <React.Fragment>
      { pagamento && <Redirect to="/pagamento" /> }
      <ContentContratacao>
        <div className="contratacao-tela">
          <h1 className="title">Contratação</h1>
          <ContratacaoFeatsWrapper>
            {clientes && <SearchCliente clientes={clientes} selecionarCliente={setCliente}/>}
            {espacos && pacotes && <SelecaoPacoteEspaco 
              pacotes={pacotes}
              espacos={espacos}
              selecionarEspaco={setEspaco}
              selecionarPacote={setPacote}
              />}
          </ContratacaoFeatsWrapper>
        </div>
      </ContentContratacao>
    </React.Fragment>
  )
}
