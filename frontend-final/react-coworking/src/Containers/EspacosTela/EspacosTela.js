import React, { useState, useEffect } from 'react';
import EspacoApi from '../../Api/EspacoApi';
import Espaco from '../../Model/Espaco';

import ContentEspaco from '../../Components/Content/ContentEspaco';
import './EspacosTela.css';
import EspacoFeatsWrapper from '../../Components/FeatsWrapper/EspacoFeatsWrapper';
import ListaMeu from '../../Components/ListaMeu';
import ListaEspaco from '../../Components/ListaEspacos';
import Botao from '../../Components/Botao/Botao';

export default function EspacosTela(props) {
  const espacoApi = new EspacoApi();

  const [ detail, setDetail ] = useState(false);
  const [ edicao, setEdicao ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ espaco, setEspaco ] = useState({});
  
  const fetchData = async () => {
    const espacosData = await espacoApi.buscarTodosEspacos().then( res => res.data );
    const espacos = espacosData.map( esp => new Espaco(esp) );
    setData(espacos);
  }
  
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect( () => fetchData(), [] );

  const fecharDetail = () => {
    setEspaco(null);
    setDetail(false);
    setEdicao(false);
  }

  const acessarEspaco = (espaco) => {
    setEspaco( new Espaco(espaco) );
    setDetail(true);
  }

  const novoEspaco = () => {
    setDetail(true);
    setEdicao(true);
    setEspaco(null);
  }

  const salvarEspaco = async (espaco) => {
    espaco.id ? 
      await espacoApi.editarEspaco(espaco) :
      await espacoApi.salvarEspaco(espaco); 
    setEspaco(null);
    setEdicao(false);
    setDetail(false);
    await fetchData();
  }


  return (
    <React.Fragment>
      <ContentEspaco>
        {
          detail ?
          <EspacoFeatsWrapper close={fecharDetail} espaco={espaco} edicao={edicao} onSubmit={salvarEspaco}/> :
          data.length > 0 && 
            <ListaMeu >
              <ListaEspaco 
                data={data} 
                onClick={acessarEspaco} 
              />
            </ListaMeu>
        }
        { !detail && <Botao titulo="Novo Espaço" onClick={novoEspaco}/>}
      </ContentEspaco>
    </React.Fragment>
  )
}
