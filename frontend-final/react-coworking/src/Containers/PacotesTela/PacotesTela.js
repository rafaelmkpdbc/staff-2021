import React, { useState, useEffect } from 'react';
import ContentPacote from '../../Components/Content/ContentPacote';
import PacoteFeatsWrapper from '../../Components/FeatsWrapper/PacoteFeatsWrapper';
import ListaMeu from '../../Components/ListaMeu';
import ListaPacotes from '../../Components/ListaPacotes';
import Botao from '../../Components/Botao';

import PacoteApi from '../../Api/PacoteApi';
import Pacote from '../../Model/Pacote';
import './PacotesTela.css';

export default function PacotesTela(props) {
  const pacoteApi = new PacoteApi();

  const [ detail, setDetail ] = useState(false);
  const [ edicao, setEdicao ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ pacote, setPacote ] = useState({});

  const fetchData = async () => {
    const pacoteData = await pacoteApi.buscarTodosPacotes().then( res => res.data );
    const pacotes = pacoteData.map( pct => new Pacote(pct) );
    setData(pacotes);
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect( () => fetchData(), [] );

  const fecharDetail = () => {
    setPacote(null);
    setDetail(false);
    setEdicao(false);
  }

  const acessarPacote = (pacote) => {
    setPacote( new Pacote(pacote) );
    setDetail(true);
  }

  const novoPacote = () => {
    setEdicao(true);
    setDetail(true);
    setPacote(null);
  }

  const salvarPacote = async (pacote) => {
    pacote.id ? 
      await pacoteApi.editarPacote(pacote) :
      await pacoteApi.salvarPacote(pacote); 
    setPacote(null);
    setEdicao(false);
    setDetail(false);
    await fetchData();
  }

  return (
    <React.Fragment>
      <ContentPacote>
        {
          detail ?
          <PacoteFeatsWrapper close={fecharDetail} pacote={pacote} edicao={edicao} onSubmit={salvarPacote}/> :
          data.length > 0 &&
            <ListaMeu>
              <ListaPacotes 
                data={data} 
                onClick={acessarPacote} 
              />
            </ListaMeu>
        }
        { !detail && <Botao titulo="Novo Pacote" onClick={novoPacote}/>}
      </ContentPacote>
    </React.Fragment>
  )
}
