import React from 'react';
import './MainTela.css';
import MainCard from '../../Components/MainCard';
import ContentMain from '../../Components/Content/ContentMain';

function MainTela(props) {
  return (
    <React.Fragment >
      <ContentMain >
        <MainCard href='/clientes' >
          <h2>Clientes</h2>
        </MainCard>
        <MainCard href='/espacos'>
          <h2>Espaços</h2>
        </MainCard>
        <MainCard href='/pacotes'>
          <h2>Pacotes</h2>
        </MainCard>
        <MainCard href='/contratacao'>
          <h2>Contratação</h2>
        </MainCard>
      </ContentMain>
    </React.Fragment>
  );
}

export default MainTela;
