import React, { Component } from 'react';
import { Route, Switch  } from 'react-router-dom';
import RotaPrivada from '../Components/RotaPrivada';

import ClientesTela from './ClientesTela';
import AcessosTela from './AcessosTela';
import ContratacaoTela from './ContratacaoTela';
import EspacosTela from './EspacosTela';
import LoginTela from './LoginTela';
import PacotesTela from './PacotesTela';
import PagamentosTela from './PagamentosTela';
import MainTela from './MainTela/MainTela';

export default class Rotas extends Component {
  render() {
    return (
      <Switch>
        <Route path="/login" exact component={ LoginTela } />
        <RotaPrivada path="/clientes" exact component={ ClientesTela } />
        <RotaPrivada path="/espacos" exact component={ EspacosTela } /> 
        <RotaPrivada path="/pacotes" exact component={ PacotesTela } />
        <RotaPrivada path="/pagamento" exact component={ PagamentosTela } />
        <RotaPrivada path="/contratacao" exact component={ ContratacaoTela } />
        <RotaPrivada path="/acessos" exact component={ AcessosTela } />
        <RotaPrivada path="/" component={ MainTela } />
      </Switch>
    )
  }
}