import coworking from "../Constants/urls";
import coworkingApi from "./api";

export default class EspacoApi {
  async buscarTodosEspacos() {
    return await coworkingApi.get(`${coworking.URL_ESPACO}/` );
  }

  async salvarEspaco({ nome, qtdPessoas, valor, espacoPacote = [], saldoCliente = [] }) {
    return await coworkingApi.post( `${coworking.URL_ESPACO}/salvar`, { nome: nome, qtdPessoas: qtdPessoas, valor: valor, espacoPacote: espacoPacote, saldoCliente: saldoCliente } );
  }
}

