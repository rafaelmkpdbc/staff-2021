import axios from "axios";
import { getToken } from "../Helpers/auth";
import coworking from "../Constants/urls";

const coworkingApi = axios.create({
  baseURL: coworking.URL_GERAL
})

coworkingApi.interceptors.request.use( async config => {
  const token = getToken();
  if( token ) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
} );

export default coworkingApi;