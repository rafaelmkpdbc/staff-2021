import coworking from "../Constants/urls";
import Pacote from "../Model/Pacote";
import coworkingApi from "./api";

export default class PacoteApi {
  async buscarTodosPacotes() {
    return await coworkingApi.get(`${coworking.URL_PACOTE}/` );
  }

  async salvarPacote({ valor, clientePacote = [], espacoPacote = [], espacos = [] }) {
    const pacoteSalvo = await coworkingApi.post( `${coworking.URL_PACOTE}/salvar`, { valor: valor } )
      .then( res => new Pacote( res.data ) );
    pacoteSalvo.espacos = espacos;
    pacoteSalvo.configurarEspacoPacote();

    pacoteSalvo.espacoPacote.forEach( async espacoPacote => {
      const salvo = await coworkingApi.post(`${coworking.URL_ESPACO_PACOTE}/salvar`, { 
        espaco: espacoPacote.espaco,
        pacote: espacoPacote.pacote,
        tipoContratacao: espacoPacote.tipoContratacao,
        quantidade: espacoPacote.quantidade,
        prazo: espacoPacote.prazo
      } );
      console.log(salvo);
    })
  } 

  async verEspPct() {
    return await coworkingApi.get(`${coworking.URL_ESPACO_PACOTE}/`);
  }
}