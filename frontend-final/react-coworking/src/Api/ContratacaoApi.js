import coworkingApi from './api';
import coworking from '../Constants/urls';

export default class ContratacaoApi {
  async buscarTodasContracoes() {
    return await coworkingApi.get( `${coworking.URL_CONTRATACAO}/`);
  }

  async buscarEspecifico(id) {
    return await coworkingApi.get( `${coworking.URL_CONTRATACAO}/${id} `);
  }

  async salvarContratacao({ cliente, espaco, tipoContratacao, quantidade, desconto, prazo }) {
    return await coworkingApi.post( `${coworking.URL_CONTRATACAO}/salvar`, { cliente: cliente, espaco: espaco, tipoContratacao: tipoContratacao, quantidade: quantidade, desconto: desconto, prazo: prazo }  );
  }
}