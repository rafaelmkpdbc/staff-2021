import coworking from "../Constants/urls";
import coworkingApi from "./api";

export default class PagamentoApi {
  async buscarTodosPagamentos() {
    return await coworkingApi.get(`${coworking.URL_PAGAMENTO}/` );
  }

  async salvarPacote({ clientePacote, contratacao, tipoPagamento }) {
    return await coworkingApi.post( `${coworking.URL_PAGAMENTO}`, 
      { clientePacote: clientePacote, contratacao: contratacao, tipoPagamento: tipoPagamento } );
  }
}