import coworkingApi from './api.js'; 
import { login } from '../Helpers/auth.js';

export default class LoginApi {
  async fazerLogin({ username, password }) {
    const response = await coworkingApi.post( "/login", { username: username, password: password } );
    try{
      const headerAuthorization = response.headers.authorization.split(' ');
      const token = headerAuthorization[1];
      login(token);
    } catch( err ) {
      console.log(`Não foi possível fazer a autenticação.`);
    }
  }
}