import coworkingApi from './api';
import coworking from '../Constants/urls';

export default class TipoContatoApi {
  async buscarTodosTiposContatos() {
    const response = await coworkingApi.get( `${coworking.URL_TIPO_CONTATO}/`);
    return response.data;
  }

  async cadastrarNovoTipoContato(nome) {
    return await coworkingApi.post( `${coworking.URL_TIPO_CONTATO}/salvar`, { nome: nome } );
  }
}