import coworkingApi from './api';
import coworking from '../Constants/urls';

export default class ClienteApi {
  async buscarTodosClientes() {
    return await coworkingApi.get( `${coworking.URL_CLIENTE}/`);
  }

  async buscarEspecifico(id) {
    return await coworkingApi.get( `${coworking.URL_CLIENTE}/${id} `);
  }

  async salvarCliente({ nome, cpf, dataNascimento, contato =[], clientePacote = [], contratacao = [], saldoCliente = [] }) {
    return await coworkingApi.post( `${coworking.URL_CLIENTE}/salvar`, { nome: nome, cpf: cpf, dataNascimento: dataNascimento, contato: contato, clientePacote: clientePacote, contratacao: contratacao, saldoCliente: saldoCliente }  );
  }

  async editarCliente({ id, nome, cpf, dataNascimento, contato =[], clientePacote = [], contratacao = [], saldoCliente = [] }) {
    return await coworkingApi.post( `${coworking.URL_CLIENTE}/editar/${id}`, { nome: nome, cpf: cpf, dataNascimento: dataNascimento, contato: contato, clientePacote: clientePacote, contratacao: contratacao, saldoCliente: saldoCliente }  );
  }
}