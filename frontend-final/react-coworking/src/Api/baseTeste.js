  import coworking from "../Constants/urls";
import coworkingApi from "./api";

export default async function carregarDadosTeste() {
  let requests = [];
  requests.push( async () => { await coworkingApi.post( `${coworking.URL_TIPO_CONTATO}/salvar`, { nome: 'telefone' }); } );

  requests.push( async () => { await coworkingApi.post( `${coworking.URL_TIPO_CONTATO}/salvar`, { nome: 'email' }); } );

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_CLIENTE}/salvar`, 
      { 
        nome: 'Joao da Silva', cpf: '50888845081', dataNascimento: '01/01/1990', contato: [ {valor: '99999999', tipoContato: 'telefone'} , {valor: 'mail@mail.com', tipoContato: 'email'} ] 
      }
    ); 
  });

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_CLIENTE}/salvar`, 
      { 
        nome: 'Rafael dos Santos', cpf: '44611599060', dataNascimento: '01/01/1990', contato: [ {valor: '333333333', tipoContato: 'telefone'} , {valor: 'email@mail.com', tipoContato: 'email'} ] 
      }
    ); 
  });

  requests.push( async () => { await coworkingApi.post(
    `${coworking.URL_ESPACO}/salvar`,
      {
        nome: 'Sala de reunião', qtdPessoas: '6', valor: 'R$ 100,00'
      }
    );
  });

  requests.push( async () => { await coworkingApi.post(
    `${coworking.URL_ESPACO}/salvar`,
      {
        nome: 'Espaço de trabalho', qtdPessoas: '3', valor: 'R$ 150,00'
      }
    );
  });

  requests.push( async () => { await coworkingApi.post(
    `${coworking.URL_ESPACO}/salvar`,
      {
        nome: 'Sala de conferência', qtdPessoas: '10', valor: 'R$ 250,00'
      }
    );
  });

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_PACOTE}/salvar`, { valor: 'R$ 2500,00' } ); });

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_PACOTE}/salvar`, { valor: 'R$ 3000,00' } ); });

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_ESPACO_PACOTE}/salvar`, 
      { 
        espaco: { id: 1 }, pacote: { id: 1 }, tipoContratacao: 'DIARIA', quantidade: 25, prazo: 20
      } 
    ); 
  });
  
  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_ESPACO_PACOTE}/salvar`, 
      { 
        espaco: { id: 2 }, pacote: { id: 1 }, tipoContratacao: 'DIARIA', quantidade: 30, prazo: 20
      } 
    ); 
  });

  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_ESPACO_PACOTE}/salvar`, 
      { 
        espaco: { id: 3 }, pacote: { id: 1 }, tipoContratacao: 'DIARIA', quantidade: 30, prazo: 20
      } 
    ); 
  });
  
  requests.push( async () => { await coworkingApi.post( 
    `${coworking.URL_ESPACO_PACOTE}/salvar`, 
      { 
        espaco: { id: 1 }, pacote: { id: 2 }, tipoContratacao: 'DIARIA', quantidade: 20, prazo: 20
      } 
    ); 
  });

  requests.forEach( request => {
    try {
      request();
    } catch( err ) {    }
  } );
}