import coworking from "../Constants/urls";
import coworkingApi from "./api";

export default class AcessoApi {
  async buscarTodosPacotes() {
    return await coworkingApi.get(`${coworking.URL_ACESSO}/` );
  }

  async salvarPacote({ saldoCliente, isEntrada, data }) {
    return await coworkingApi.post( `${coworking.URL_ACESSO}`, 
      { saldoCliente: saldoCliente, isEntrada: isEntrada, data: data } );
  }
}