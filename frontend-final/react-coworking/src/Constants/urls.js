const coworking = {
  URL_GERAL: 'http://localhost:8080/coworking',
  URL_ACESSO: `/acesso`,
  URL_CLIENTE: `/cliente`,
  URL_CLIENTE_PACOTE: `/cliente-pacote`,
  URL_CONTATO: `/contato`,
  URL_CONTRATACAO: `/contratacao`,
  URL_ESPACO: `/espaco`,
  URL_ESPACO_PACOTE: `/espaco-pacote`,
  URL_PACOTE: `/pacote`,
  URL_PAGAMENTO: `/pagamento`,
  URL_TIPO_CONTATO: `/tipo-contato`,
  URL_LOGIN: `/login`,
};

export default coworking;