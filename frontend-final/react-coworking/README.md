# React-Coworking

Aplicação de front-end criada para comunicação com aplicação [Coworking](../../modulo3/coworking/).

## Executando a aplicação

Antes de executar a aplicação, no diretório do programa deve ser executado o comando para baixar as dependências:
```
npm i
```

A aplicação deve ser executada através do comando:   
```
npm start
```

## Telas e funcionalidades
### Telas   
- [x] Login
- [x] Clientes
- [x] Pacotes
- [x] Espaços
- [x] Contratação
- [ ] Pagamento
- [ ] Acesso

### Funcionalidades   
- [x] Validação de login
- [x] Criação e consulta de clientes
- [x] Criação e consulta de espaços
- [x] Criação e consulta de pacotes (à corrigir)
- [ ] Realizar contratação
- [ ] Realizar pagamento
- [ ] Realizar acesso 

## Dependências 
- [Axios](https://github.com/axios/axios)
- [React Router Dom](https://reactrouter.com/web/guides/quick-start)
- [Infinite Scroller](https://www.npmjs.com/package/react-infinite-scroller)
- [Ant Design](https://ant.design/)