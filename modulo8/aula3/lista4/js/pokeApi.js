// eslint-disable-next-line no-unused-vars
class PokeApi {
  constructor() {
    this._url = 'https://pokeapi.co/api/v2/pokemon';
  }

  buscarTodos() {
    const requisicao = fetch( `${ this._url }?limit=100&offset=200` );
    return requisicao.then( data => data.json() ).then( data => data.results );
  }

  buscarEspecifico( id ) {
    const requisicao = fetch( `${ this._url }/${ id }` );
    return requisicao.then( data => data.json() );
  }
}
