const pokeApi = new PokeApi();
const numeroInput = document.getElementById( 'numero-input' );
const botaoSorte = document.querySelector( '.button-sorte' );
const botaoPrev = document.querySelector( '.button-prev' );
const botaoNext = document.querySelector( '.button-next' );
let indexAtual;

function isValorValido( value ) {
  return value && !Number.isNaN( parseInt( value, 10 ) );
}

function isNotPokemonAtual( input ) {
  return parseInt( input, 10 ) !== indexAtual;
}

function limparCampos() {
  document.querySelector( '.nome-pokemon' ).innerHTML = '';
  document.querySelector( '.sprite' ).src = '';
  document.querySelector( '.atributos' ).innerHTML = '';
}

function criarItemLista( valor, atributo = '' ) {
  const li = document.createElement( 'li' );
  li.innerHTML = atributo === '' ? `- ${ valor }` : `${ atributo }: ${ valor }`;
  return li;
}

function criarSubLista( valores, atributo = '' ) {
  const ul = document.createElement( 'ul' );
  ul.innerHTML = atributo;
  valores.forEach( valor => {
    const subLi = criarItemLista( valor );
    subLi.classList.add( 'sub-lista' );
    ul.appendChild( subLi );
  } );
  return ul;
}

function renderizarParametros( pokemon ) {
  numeroInput.value = pokemon.id;
  const tagAtributos = document.querySelector( '.atributos' );
  const listaAtributos = document.createElement( 'ul' );
  tagAtributos.appendChild( listaAtributos );

  listaAtributos.appendChild( criarItemLista( pokemon.id, 'ID' ) );

  listaAtributos.appendChild( criarItemLista( pokemon.altura, 'ALTURA' ) );

  listaAtributos.appendChild( criarItemLista( pokemon.peso, 'PESO' ) );

  const habilidades = criarSubLista( pokemon.habilidades, 'HABILIDADES' );
  listaAtributos.appendChild( habilidades );

  const listaHabilidades = document.createElement( 'ul' );
  listaHabilidades.classList.add( 'sub-lista' );
  habilidades.appendChild( listaHabilidades );

  listaAtributos.appendChild( habilidades );

  const tipos = document.createElement( 'li' );
  tipos.innerHTML = 'tipos:';
  const listaTipos = document.createElement( 'ul' );
  listaTipos.classList.add( 'sub-lista' );
  tipos.appendChild( listaTipos );
  pokemon.tipos.forEach( tipo => {
    listaTipos.appendChild( criarItemLista( tipo.type.name ) );
  } );
  listaAtributos.appendChild( tipos );

  const estatisticas = document.createElement( 'li' );
  estatisticas.innerHTML = 'estatisticas: ';
  const listaEstatisticas = document.createElement( 'ul' );
  listaEstatisticas.classList.add( 'sub-lista' );
  estatisticas.appendChild( listaEstatisticas );
  pokemon.estatisticas.forEach( estatistica => {
    const liEstatistica = document.createElement( 'li' );
    liEstatistica.innerHTML = `base_stat: ${ estatistica.base_stat } - name: ${
      estatistica.stat.name
    }`;
    listaEstatisticas.appendChild( liEstatistica );
  } );
  listaAtributos.appendChild( estatisticas );
}

function renderizarPokemon( pokemon ) {
  limparCampos();

  indexAtual = pokemon.id;
  const titulo = document.querySelector( '.nome-pokemon' );
  titulo.innerHTML = pokemon.nome;

  const img = document.querySelector( '.sprite' );
  img.src = pokemon.imagem;

  renderizarParametros( pokemon );
}

function renderizarErro( mensagem ) {
  limparCampos();

  const nomePokemon = document.querySelector( '.nome-pokemon' );
  nomePokemon.innerHTML = mensagem;
}

function buscarPokemon( input ) {
  return pokeApi.buscarEspecifico( input ).then( pokeData => new Pokemon( pokeData ) );
}

function buscarERenderizarPokemon( input ) {
  buscarPokemon( input ).then(
    pokemon => {
      renderizarPokemon( pokemon );
    },
    () => {
      renderizarErro( 'Pokémon não encontrado!' );
    },
  );
}

numeroInput.addEventListener( 'blur', event => {
  if ( isValorValido( event.target.value ) ) {
    if ( isNotPokemonAtual( event.target.value ) ) {
      buscarERenderizarPokemon( event.target.value );
    }
  } else {
    renderizarErro( 'Favor digitar um valor válido!' );
  }
} );

function buscarAnterior() {
  buscarERenderizarPokemon( indexAtual - 1 );
}

function buscarProximo() {
  buscarERenderizarPokemon( indexAtual + 1 );
}

function isNumeroJaBuscado( numero ) {
  const aleatoriosGerados = JSON.parse( localStorage.getItem( 'aleatoriosGerados' ) );
  return aleatoriosGerados && aleatoriosGerados.some( gerado => numero === parseInt( gerado, 10 ) );
}

function gerarAleatorio() {
  const random = Math.floor( Math.random() * 893 + 1 );
  return isNumeroJaBuscado( random ) ? gerarAleatorio() : random;
}

function guardarAleatorioGerado( input ) {
  if ( !localStorage.getItem( 'aleatoriosGerados' ) ) {
    const emptyArr = [];
    localStorage.setItem( 'aleatoriosGerados', JSON.stringify( emptyArr ) );
  }
  const aleatorios = JSON.parse( localStorage.getItem( 'aleatoriosGerados' ) );
  aleatorios.push( input );
  localStorage.setItem( 'aleatoriosGerados', JSON.stringify( aleatorios ) );
}

function estouComSorte() {
  const randomInput = gerarAleatorio();
  guardarAleatorioGerado( randomInput );
  numeroInput.value = randomInput;
  buscarERenderizarPokemon( randomInput );
}

botaoSorte.addEventListener( 'click', estouComSorte );
botaoPrev.addEventListener( 'click', buscarAnterior );
botaoNext.addEventListener( 'click', buscarProximo );
