// eslint-disable-next-line no-unused-vars
class Pokemon {
  constructor( objApi ) {
    this._id = objApi.id;
    this._nome = objApi.name;
    this._peso = objApi.weight;
    this._altura = objApi.height;
    this._tipos = objApi.types;
    this._habilidades = objApi.abilities.map(
      ( habilidade ) => habilidade.ability.name,
    );
    this._estatisticas = objApi.stats;
    this._imagem = objApi.sprites.front_default;
  }

  get id() {
    return this._id;
  }

  get nome() {
    return this._nome;
  }

  get peso() {
    return `${ this._peso / 10 } kg`;
  }

  get altura() {
    return `${ this._altura * 10 } cm`;
  }

  get habilidades() {
    return this._habilidades;
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }

  get imagem() {
    return this._imagem;
  }
}
