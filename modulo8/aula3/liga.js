class Time {
  jogadores = [];
  constructor(nome, esporte, status, liga) {
    this._nome = nome;
    this._esporte = esporte;
    this._status = status;
    this._liga = liga;
  }

  adicionarJogador(jogador) {
    this.jogadores.push(jogador);
  }

  buscarJogadorPorNumero(numero) {
    return this.jogadores.find((jogador) => {
      return jogador.numero === numero;
    });
  }

  buscarJogadorPorNome(nome) {
    return this.jogadores.find((jogador) => {
      return jogador.nome === nome;
    });
  }
}

class Jogador {
  constructor(numero, nome) {
    this._numero = numero;
    this._nome = nome;
  }

  get numero() {
    return this._numero;
  }

  get nome() {
    return this._nome;
  }
}

let time = new Time('time', 'futebol', 'ativo', 'brasileirao');
let jogador = new Jogador(23, 'rafael');
time.adicionarJogador(jogador);
console.log(time.buscarJogadorPorNumero(23));
console.log(time.buscarJogadorPorNome('rafael'));
