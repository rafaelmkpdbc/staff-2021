class Elfo {
  constructor(nome) {
    this._nome = nome;
  }

  get nome() {
    return this._nome;
  }

  matarElfo() {
    this.status = 'morto';
  }

  get estaMorto() {
    return (this.status = 'morto');
  }

  atacarComFlecha() {
    setTimeout(() => {
      console.log('atacou');
    }, 2000);
  }
}
