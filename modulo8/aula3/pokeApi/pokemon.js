class Pokemon {
  constructor(objApi) {
    this._nome = objApi.name;
    this._imagem = objApi.sprites.front_default;
    this._altura = objApi.height;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }
  get altura() {
    return `${this._altura * 10} cm`;
  }
}
