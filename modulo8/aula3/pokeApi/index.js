let pokeapi = new PokeApi();

// let buscou = pokeapi.buscarTodos();
// buscou.then((pokemon) => {
//   console.log(pokemon);
// });

// let buscouEspecifico = pokeapi.buscarEspecifico(80);
// buscouEspecifico.then((pokemon) => {
//   let nome = dadosPokemon.querySelector('.nome');
//   nome.innerHTML = pokemon.name;

//   let imagem = dadosPokemon.querySelector('.thumb');
//   imagem.src = pokemon.sprites.front_default;
// });
let buscou = pokeapi.buscarTodos();
buscou.then((pokemon) => {
  console.log(pokemon);
});

pokeapi.buscarEspecifico(80).then((pokemon) => {
  let poke = new Pokemon(pokemon);
  renderizar(poke);
});

renderizar = (pokemon) => {
  let dadosPokemon = document.getElementById('dadosPokemon');

  let nome = dadosPokemon.querySelector('.nome');
  nome.innerHTML = pokemon.nome;

  let imagem = dadosPokemon.querySelector('.thumb');
  imagem.src = pokemon.imagem;

  let altura = dadosPokemon.querySelector('.altura');
  altura.innerHTML = pokemon.altura;
};
