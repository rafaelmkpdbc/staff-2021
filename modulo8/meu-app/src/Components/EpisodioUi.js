import React, { Component } from 'react'

export default class EpisodioUi extends Component {

  render() {
    const {episodio} = this.props;
    return (
      <React.Fragment>
        <h2>{episodio.nome}</h2>
          <img src={episodio.imagem} alt={episodio.nome} />
          <span>Já assisti? {episodio.foiAssistido ? 'SIM' : 'NÃO'}{ episodio.foiAssistido && `, ${episodio.qtdVezesAssistido} vez(es)`} </span>
          <span>Duração: {episodio.duracaoEmMin}</span>
          <span>Temporada/Episódio: {episodio.temporadaEpisodio}</span>
      </React.Fragment>
    )
  }
}
