import React, { Component } from 'react'
import PropTypes from 'prop-types';
import './MeuInputNumero.css';

import Mensagem from '../../Constants/mensagem.js';

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    const { obrigatorio, atualizarValor }  = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    atualizarValor({erro, nota});
  }

  render() {
    const { placeholder, visivel, mensagem, exibirErro, obrigatorio } = this.props;

    return visivel && (
      <React.Fragment>
        {mensagem && <span>{mensagem}: </span>}
        <div className='input-wrapper'>
          <input
            type='number'
            className={exibirErro ? 'erro' : ''}
            placeholder={placeholder}
            onBlur={this.perderFoco}
          />
          {obrigatorio && <span className={exibirErro ? 'mensagem-erro' : ''}>{Mensagem.ERRO.CAMPO_OBRIGATORIO}</span>}
        </div>
      </React.Fragment>
    );
  }
}
