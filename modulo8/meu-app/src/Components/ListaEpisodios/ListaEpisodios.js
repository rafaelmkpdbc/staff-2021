import React from 'react'
import EpAvaliado from '../EpAvaliado/EpAvaliado';

export default function ListaEpisodios({listaEpisodios}) {
  console.log(listaEpisodios);
  return (
    <div className="container-lista-avaliacoes">
      { 
        listaEpisodios && listaEpisodios.map( (e, i) => {
          return <EpAvaliado episodio={e} key={i} />
        })
      }
    </div>
  )
}
