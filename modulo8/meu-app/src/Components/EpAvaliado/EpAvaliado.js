import React, { Component } from 'react'
import './EpAvaliado.css';
import { Link } from 'react-router-dom';

export default class EpAvaliado extends Component {

  
  render() {
    const { nome, nota, id } = this.props.episodio;
    const { episodio } = this.props;
    return (
      <Link className="episodio-card" to={{ pathname:`/episodio/${id}`, state: { episodio } } }  >
        <span className="ep-desc"><strong>{ nome }</strong>{` -  Avaliação: ${ nota }` }</span>
      </Link>
    )
  }
}
