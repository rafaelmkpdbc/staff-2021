import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './BotaoUi.css';

export default class BotaoUi extends Component {
  render() {
    const { link, texto, cor, metodo } = this.props;
    return (
      <button className={`button ${cor}`} onClick={ metodo }>
        {link ? 
          ( <Link className={`button ${cor}`} to={link }>
              {texto}
            </Link> ) 
          : texto
        }
      </button>
    )
  }
}
