import axios from "axios";

const ENDERECO = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ENDERECO}/episodios` ).then( ep => ep.data );
  }

  buscarEpEspecifico( id ) {
    return axios.get( `${ENDERECO}/episodios/${id}` ).then( ep => ep.data );
  }

  buscarDetalheEspecifico( id ) {
    return axios.get( `${ENDERECO}/detalhes/${id}` ).then( ep => ep.data );
  }
 
  buscarTodasNotas() {
    return axios.get( `${ENDERECO}/notas` ).then( ep => ep.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ ENDERECO }/notas`, { nota, episodioId } );
  }
}