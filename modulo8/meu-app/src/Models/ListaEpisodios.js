import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}


export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new Episodio( episodio ) );
  }

  get episodioAleatorio() {
    const indice = sortear(0, this.todos.length);
    return this.todos[indice];
  }

  get avaliados() {
    return this.todos.filter( ep => ep.nota ).sort( ( ep1, ep2 ) => 
      ep1.temporada === ep2.temporada ? 
      ep1.ordem - ep2.ordem 
      : ep1.temporada - ep2.temporada
    );
  }
}
