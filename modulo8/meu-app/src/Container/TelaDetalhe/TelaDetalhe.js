import React, { Component } from 'react';
import Episodio from '../../Models/Episodio';
import EpisodioAPI from '../../Models/EpisodioAPI';
import './TelaDetalhe.css';

export default class TelaDetalhe extends Component {
  constructor(props) {
    super(props);
    this.episodioApi = new EpisodioAPI();
    this.id = this.props.match.params.id;
    this.state = {}
  }


  componentDidMount() {
    const id = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarDetalheEspecifico( id ),
      this.episodioApi.buscarEpEspecifico( id ),
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        console.log(respostas)
        this.setState( state => {
          return { 
            ...state,
            detalhes: respostas[0],
            episodio: new Episodio(respostas[1])
          }
        } );
      } );
  }

  getFormattedDate( date ) {
    return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
  }
  
  render() {
    const { episodio, detalhes } = this.state;

    return (
      !episodio || !detalhes ? 
      ( <h3>Aguarde...</h3> ) :
      ( <div className="ep-detail-wrapper">
        <img src={episodio.imagem} alt={episodio.nome}/>
        <span className="ep-desc"><strong>{episodio.nome}</strong></span>
        <span className="ep-desc">{`Temporada / Episódio: ${episodio.temporadaEpisodio}`}</span>
        <span className="ep-desc">{`Duração: ${episodio.duracao}`}</span>
        <span className="ep-desc">{`Data de estreia: ${episodio.dataEstreia}`}</span>
        <span className="ep-desc">{`Nota que você deu: ${episodio.nota || 'não avaliado'}`}</span>
        <span className="ep-desc">{`Nota iMDB: ${detalhes.notaImdb}`}</span>
      </div> )
    );
  }
}
