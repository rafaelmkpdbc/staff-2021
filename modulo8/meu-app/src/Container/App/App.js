import React, { Component } from 'react';
import ListaEpisodios from '../../Models/ListaEpisodios';
import EpisodioAPI from '../../Models/EpisodioAPI';
import './App.css';
import MeuInputNumero from '../../Components/MeuInputNumero';
import EpisodioUi from '../../Components/EpisodioUi';
import MensagemFlash from '../../Components/MensagemFlash';
import BotaoUi from '../../Components/BotaoUi';

import Mensagem from '../../Constants/mensagem.js';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.episodioAPI = new EpisodioAPI();
    this.sortear = this.sortear.bind(this);
    this.marcarComoAssistido = this.marcarComoAssistido.bind(this);
    this.state = {
      deveExibirMsg: false,
      deveExibirErro: false
    };
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then( episodio => {
        this.listaEpisodios = new ListaEpisodios( episodio );
        this.setState( state => {
          return { ...state, episodio: this.listaEpisodios.episodioAleatorio }
        } );
      } );
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;

    this.setState((state) => {
      return { ...state, episodio };
    });
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();

    this.setState((state) => {
      return { ...state, episodio };
    });
  }

  registrarNota({erro, nota}) {
    this.setState((state) => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    
    if(erro) {
      return;
    }

    const { episodio } = this.state;
    let mensagem, corMensagem;

    if ( episodio.validarNota( nota ) ) {
      episodio.avaliar(nota).then( () => {
        corMensagem = 'verde';
        mensagem = Mensagem.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem({ corMensagem, mensagem });
      } );
    } else {
      corMensagem = 'vermelho';
      mensagem = Mensagem.ERRO.NOTA_INVALIDA;
      this.exibirMensagem({ corMensagem, mensagem });
    }
  }

  atualizarMensagem = (deveExibir) => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMsg: deveExibir,
      };
    });
  };

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMsg: true,
        mensagem,
        corMensagem,
      };
    });
  };

  render() {
    const { episodio, deveExibirMsg, deveExibirErro, mensagem, corMensagem } = this.state;
    const { listaEpisodios } = this;

    return ( 
      !episodio ? 
        ( <h3>Aguarde...</h3> ) :
        (<div className='App'>
          <header className='App-header'>
            <BotaoUi cor='verde' texto='Lugar nenhum' />
            <BotaoUi link={{ pathname:"/avaliacoes", state: { listaEpisodios } }} cor='verde' texto='Lista de Avaliações' />
            <BotaoUi link={{ pathname:"/episodios", state: { listaEpisodios } }} cor='verde' texto='Listar Todos os episódios' />
            <MensagemFlash
              exibir={deveExibirMsg}
              mensagem={mensagem}
              cor={corMensagem}
              segundos='5'
              atualizar={() => this.atualizarMensagem()}
            />
            <EpisodioUi episodio={episodio} />
            <div className='botoes'>
              <button className='btn verde' onClick={this.sortear}>
                Proximo
              </button>
              <button className='btn azul' onClick={this.marcarComoAssistido}>
                Já assisti
              </button>
            </div>
            <span>Nota: {episodio.nota || '0'}</span>
            <MeuInputNumero
               placeholder="Nota de 1 a 5"
                mensagem={Mensagem.DESCRICAO.INPUT_EPISODIO}
                visivel={ episodio.foiAssistido || false }
                obrigatorio
                exibirErro={ deveExibirErro }
                atualizarValor={ this.registrarNota.bind( this ) }
            />
          </header>
        </div> )
    );
  }
}
