import React, { useState, useEffect } from 'react';
import EpisodioAPI from '../../Models/EpisodioAPI';

export default function Formulario( props ) {
  const [ qtdVezesAssistido, setQtdVezesAssistido ] = useState(1);
  const [ nome, setNome ] = useState('Rafael');
  const [ filmeId, setFilmeId ] = useState(1);
  const [ detalhes, setDetalhes ] = useState({});

  useEffect(() => {
    const episodioApi = new EpisodioAPI();
    episodioApi.buscarEpEspecifico( filmeId ).then( e => setDetalhes( e ) );
    console.log(detalhes);
  }, [ filmeId ] );

  return (
    <React.Fragment>
      <h1>Quantidade vezes assistido: { qtdVezesAssistido }</h1>
      <h2>Quem assistiu: { nome }</h2>
      <button type="button" onClick={ () => setQtdVezesAssistido( qtdVezesAssistido +1) }>Já assisti</button>
      <input type="text" onBlur={ evt => setNome( evt.target.value ) } />
      <input type="text" onBlur={ evt => setFilmeId( evt.target.value ) } />
      <span>{detalhes.nome}</span>
    </React.Fragment>
  )
}
