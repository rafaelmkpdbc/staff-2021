import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { RotasPrivadas } from '../Components/RotasPrivadas';

import App from './App';
import Formulario from './Formulario';
import ListagemAvaliacoes from './ListagemAvaliacoes';
import TelaDetalhe from './TelaDetalhe';
import TodosEpisodios from './TodosEpisodios/TodosEpisodios';

export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" exact component={ ListagemAvaliacoes } />
        <RotasPrivadas path="/episodios" exact component={ TodosEpisodios } />
        <Route path="/episodio/:id" exact component={ TelaDetalhe } />
        <Route path="/formulario" exact component={ Formulario } />
      </Router>
    );
  }
}
