import React, { Component } from 'react'
import ListaEpisodios from '../../Components/ListaEpisodios/ListaEpisodios';
import './ListagemAvaliacoes.css';

export default class ListagemAvaliacoes extends Component {
  render() {
    const { listaEpisodios } = this.props.location.state;

    return (
      <div className="page-aval-wrapper">
        <h1>Lista de Avaliação de Episódios</h1>
        <ListaEpisodios listaEpisodios={ listaEpisodios.avaliados } />
      </div>
    )
  }
}
