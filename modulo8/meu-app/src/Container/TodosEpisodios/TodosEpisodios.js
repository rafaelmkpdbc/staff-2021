import React, { Component } from 'react'
import ListaEpisodios from '../../Components/ListaEpisodios/ListaEpisodios';
import EpisodioAPI from '../../Models/EpisodioAPI';

export default class TodosEpisodios extends Component {
  constructor(props) {
    super(props)
    this.episodioApi = new EpisodioAPI();
    this.state = {
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuracao: 'ASC'
    }
  }

  componentDidMount() {
    const { listaEpisodios } = this.props.location.state;
    const requisicoes = listaEpisodios.todos.map( e => this.episodioApi.buscarDetalheEspecifico( e.id ) );
    Promise.all( requisicoes ).then( resultado => {
      listaEpisodios.todos.forEach( episodio => {
        console.log(episodio.dataEstreia)
        episodio.dataEstreia = resultado.find( e => e.id === episodio.id ).dataEstreia
      } );
    } );
  }
  
  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState({
      ordenacao: ( a, b ) => new Date( ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia ) 
        - new Date ( ( tipoOrdenacaoDataEstreia === 'ASC ' ? b : a ).dataEstreia ),
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    });
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState({
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao 
        - ( tipoOrdenacaoDuracao === 'ASC ' ? b : a ).duracao,
        tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    });
  }

  render() {
    const {listaEpisodios} = this.props.location.state;
    listaEpisodios.todos.sort( this.state.ordenacao );

    return (
      <React.Fragment>
        <header className="App-header">
          <h1>Todos Episodios</h1>
          <ListaEpisodios listaEpisodios={listaEpisodios.todos}/>
        </header>
      </React.Fragment>
    );
  }
}
