import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Rotas from './Container/Rotas';
// import StudApp from './Container/StudApp';

ReactDOM.render(<Rotas />, document.getElementById('root'));
