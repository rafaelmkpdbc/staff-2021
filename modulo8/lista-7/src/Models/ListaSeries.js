import { series as seriesJson } from '../Resources/series.js';
import Serie from './Serie.js';
import PropTypes from 'prop-types';

export default class ListaSeries {
  constructor() {
    this.series = seriesJson.map((serie) => new Serie(serie));
  }

  #haCampoInvalido(serie) {
    return Object.values(serie).some( valor => !valor );
  }

  #temAnoInvalido(serie) {
    return serie.anoEstreia > new Date().getFullYear();
  }

  invalidas() {
    const seriesInvalidas = this.series.filter(
      (serie) => this.#temAnoInvalido(serie) || this.#haCampoInvalido(serie)
    );
    return `Séries Inválidas: ${seriesInvalidas
      .map((serie) => serie.titulo)
      .join(' - ')}`;
  }

  filtrarPorAno(ano) {
    return this.series.filter((serie) => serie.anoEstreia >= ano);
  }

  procurarPorNome(nome) {
    return this.series.some((serie) =>
      serie.elenco.some((atuante) =>
        atuante.toLowerCase().includes(nome.toLowerCase())
      )
    );
  }

  mediaDeEpisodios() {
    const mediaEpisodios =
      this.series.reduce((acc, curr) => acc + curr.numeroEpisodios, 0) /
      this.series.length;
    return mediaEpisodios.toFixed(2);
  }

  totalSalarios(indexSerie) {
    const serie = Object.assign({}, this.series[indexSerie]);
    let salarioTotal = 0;
    salarioTotal += serie.diretor.length * 100000;
    salarioTotal += serie.elenco.length * 40000;
    return `R$ ${salarioTotal},00`;
  }

  queroGenero(genero) {
    const seriesDoGenero = this.series.filter((serie) =>
      serie.genero.some((generoAtual) =>
        generoAtual.toLowerCase().includes(genero.toLowerCase())
      )
    );
    return seriesDoGenero.map((serie) => serie.titulo);
  }

  queroTitulo(titulo) {
    const seriesComTitulo = this.series.filter((serie) =>
      serie.titulo.includes(titulo)
    );
    return seriesComTitulo.map((serie) => serie.titulo);
  }

  #ordenarPorSobrenome(arr) {
    if (arr.length === 1) {
      return arr;
    }
    return arr.sort((a, b) => {
      const nomesA = a.split(' ');
      const sobrenomeA = nomesA[nomesA.length - 1];
      const nomesB = b.split(' ');
      const sobrenomeB = nomesB[nomesB.length - 1];
      return sobrenomeA.localeCompare(sobrenomeB);
    });
  }

  creditos(indexSerie) {
    const serie = Object.assign({}, this.series[indexSerie]);
    const diretoresOrdenados = this.#ordenarPorSobrenome(serie.diretor);
    const elencoOrdenado = this.#ordenarPorSobrenome(serie.elenco);
    let creditos = '';
    creditos = creditos.concat(`-- TÍTULO --\n${serie.titulo}\n`);
    creditos = creditos.concat(
      `-- DIRETOR(ES) --\n${diretoresOrdenados.join(', ')}\n`
    );
    creditos = creditos.concat(`-- ELENCO -- \n${elencoOrdenado.join(', ')}\n`);
    return creditos;
  }

  #temNomeAbreviado(nome) {
    const regex = /\s(.*?).\s/;
    return nome.match(regex);
  }

  hashtagSecreta() {
    const elencosAbreviados = this.series.filter((serie) => {
      return serie.elenco.every((atuante) => this.#temNomeAbreviado(atuante));
    });
    const hashtagSecreta = elencosAbreviados.map((serie) =>
      serie.elenco.map((atuante) => this.#temNomeAbreviado(atuante)[1])
    );

    return `#${hashtagSecreta.join().replace(/,/g, '')}`;
  }
}

ListaSeries.propTypes = {
  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  procurarPorNome: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  totalSalarios: PropTypes.array,
  queroGenero: PropTypes.array,
  queroTitulo: PropTypes.array,
  creditos: PropTypes.array,
  hashtagSecreta: PropTypes.array,
};