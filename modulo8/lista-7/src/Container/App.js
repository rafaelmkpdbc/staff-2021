import './App.css';
import ListaSeries from '../Models/ListaSeries';
import React, { Component } from 'react'

export default class App extends Component {
  constructor(props) {
    super(props);

    const listaSeries = new ListaSeries();

    console.log(listaSeries.invalidas()); // 'Band of Brothers'
    console.log(listaSeries.filtrarPorAno(2015)); //6 series
    console.log(listaSeries.procurarPorNome('Rafael')); //false
    console.log(listaSeries.mediaDeEpisodios()); //44.44
    console.log(listaSeries.totalSalarios(0)); //R$640000,00
    console.log(listaSeries.queroGenero('Caos')); // 'The JS Mirror'
    console.log(listaSeries.queroTitulo('The')); // 'The Walking Dead', 'The JS Mirror'
    console.log(listaSeries.creditos(0));
    console.log(listaSeries.hashtagSecreta());
  }
  

  render() {
    return (
      <div>
      </div>
    )
  }
}
