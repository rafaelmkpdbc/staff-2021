function cardapioIFood(veggie = true, comLactose = true) {
  const cardapioPadrao = ['enroladinho de salsicha', 'cuca de uva'];

  if (comLactose) {
    cardapioPadrao.push('pastel de queijo');
  }

  cardapioPadrao.push('pastel de carne', 'empada de legumes marabijosa');

  Object.freeze(cardapioPadrao);

  const cardapioCustomizado = [...cardapioPadrao];

  if (veggie) {
    const enroladinhoIndex = cardapioCustomizado.indexOf(
      'enroladinho de salsicha'
    );
    cardapioCustomizado.splice(enroladinhoIndex, 1);

    const pastelCarneIndex = cardapioCustomizado.indexOf('pastel de carne');
    cardapioCustomizado.splice(pastelCarneIndex, 1);
  }

  const apresentacaoCardapio = cardapioCustomizado.map((item) => {
    return item.toUpperCase();
  });

  return apresentacaoCardapio;
}

let veggie = true;
let lactose = true;
console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
(veggie = false), (lactose = false);
lactose = console.log(cardapioIFood(veggie, lactose)); //esperado: [ 'ENROLADINHO DE SALSICHA', 'CUCA DE UVA', 'PASTEL DE CARNE', 'EMPADA DE LEGUMES MARABIJOSA' ]
(veggie = true), (lactose = false);
console.log(cardapioIFood(veggie, lactose)); // esperado: [ 'CUCA DE UVA', 'EMPADA DE LEGUMES MARABIJOSA' ]
(veggie = false), (lactose = true);
console.log(cardapioIFood(veggie, lactose)); // esperado: [ 'ENROLADINHO DE SALSICHA', 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'PASTEL DE CARNE', 'EMPADA DE LEGUMES MARABIJOSA' ];
