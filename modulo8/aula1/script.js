//EXERCICIO 1
function calcularCirculo({raio, tipoCalculo}) {
  if (tipoCalculo === 'A') {
    return calcularArea(raio);
  }

  if (tipoCalculo === 'C') {
    return calcularCircunferencia(raio);
  }

  return 'tipoCalculo invalido';

  function calcularCircunferencia(raio) {
    return Math.pow(raio, 2) * Math.PI;
  }
  function calcularArea(raio) {
    return raio * 2 * Math.PI;
  }
}

const circulo1 = {
  raio: 6,
  tipoCalculo: 'A',
};

const circulo2 = {
  raio: 6,
  tipoCalculo: 'C',
};

console.log('EXERCICIO 1');
console.log(calcularCirculo(circulo1));
console.log(calcularCirculo(circulo2));

//EXERCICIO 2
function naoBissexto(ano) {
  return !(ano % 400 === 0 || (ano % 4 === 0 && ano % 100 != 0));
}

console.log('EXERCÍCIO 2');
console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

//EXERCICIO 3
function somarPares(numeros = []) {
  let soma = 0;

  for (let i = 0; i < numeros.length; i++) {
    if (i % 2 === 0) {
      soma += numeros[i];
    }
  }
  return soma;
}

console.log('EXERCICIO 3');
console.log(somarPares([1, 56, 4.34, 6, -2]));
console.log(somarPares([0, 1, 2, 3, 4, 5, 6]));

//EXERCICIO 4
function somar(numero1) {
  function somarClosure(numero2) {
    return numero1 + numero2;
  }
  return somarClosure;
}

console.log('EXERCICIO 4');
console.log(somar(3)(4));
console.log(somar(5642)(8749));

//EXERCICIO - currying
let divisao = (divisor) => (numero) => numero / divisor;
let divisaoPor = divisao(2);
console.log('EXERCICIO EXTRA - currying');
console.log(divisaoPor(4));
console.log(divisaoPor(6));
console.log(divisaoPor(8));
console.log(divisaoPor(10));

//EXERCICIO 5
function imprimirBRLComNumberFormat(valor) {
  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  });

  const valorArredondado = arredondarValorPraCima(valor);
  let floatArredondado = parseFloat(valorArredondado);

  return formatter.format(floatArredondado);

  function arredondarValorPraCima(valorArredondar) {
    const valorArredondarAbsoluto = Math.abs(valorArredondar);
    let arredondado = parseFloat(valorArredondarAbsoluto.toFixed(2));

    if (arredondado < valorArredondarAbsoluto) {
      arredondado = parseFloat(arredondado) + 0.01;
    }

    return Math.sign(valorArredondar) >= 0
      ? arredondado.toFixed(2)
      : -Math.abs(arredondado).toFixed(2);
  }
}

function imprimirBRLNaMao(valor) {
  const valorAbsoluto = Math.abs(valor);
  let stringArredondada = arredondarValorPraCima(valorAbsoluto);
  stringArredondada = tratarNumeracao(stringArredondada);
  const marcadorMonetario = valor >= 0 ? 'R$ ' : '-R$ ';

  return marcadorMonetario + stringArredondada;

  function arredondarValorPraCima(valorArredondar) {
    let arredondadoInicial = valorArredondar.toFixed(2);
    if (parseFloat(arredondadoInicial) < valorArredondar) {
      valorArredondar += 0.01;
    }
    return valorArredondar.toFixed(2);
  }

  function tratarNumeracao(stringTratar) {
    stringTratar = stringTratar.replace('.', ',');

    if (stringTratar.length > 6) {
      let chars = [...stringTratar];
      for (let i = chars.length - 5; i > 1; i--) {
        if ((i + 1) % 3 === 0 && i + 1 < chars.length) {
          chars.splice(i - 1, 0, '.');
        }
      }
      stringTratar = chars.join('');
    }
    return stringTratar;
  }
}

console.log('EXERCICIO 5 - NumberFormat');
console.log(imprimirBRLComNumberFormat(0));
console.log(imprimirBRLComNumberFormat(3498.99));
console.log(imprimirBRLComNumberFormat(-3498.99));
console.log(imprimirBRLComNumberFormat(2313477.0135));
console.log(imprimirBRLComNumberFormat(-2313477.0135));

console.log('EXERCICIO 5 - Na Mão');
console.log(imprimirBRLNaMao(0));
console.log(imprimirBRLNaMao(3498.99));
console.log(imprimirBRLNaMao(-3498.99));
console.log(imprimirBRLNaMao(2313477.0135));
console.log(imprimirBRLNaMao(-2313477.0135));
