use sistemaBancario;

db.createCollection("banco");

//Banco Alfa
gerAraci = {
    cpf: "11111111111111",
    nome: "Araci da Silva",
    nascimento: "01-01-1980",
    endereco: "Rua dos testes, n 500, NA, NA, NA, Brasil",
    estado_civil: "Solteiro",
    codigo_funci: 111,
    tipo: "GC",    
};

clFulano = {
    cpf: "465461651981958",
    nome: "Fulano da silva",
    nascimento: "03-03-1990",
    endereco: "Rua dos testes, n 150, NA, NA, NA, Brasil",
    estado_civil: "Solteiro",
};

cc100 = {
    numero: 100,
    titular: [
        clFulano,
    ],
    tipo: "PF",
    gerente: gerAraci,
};

agWebAlfa = {
    codigo: 0001,
    nome: "Web",
    endereco: "Rua Testando, n 55, loja 1, NA, NA, NA, Brasil",
    conta: [
        cc100,
    ],
};

gerBira = {
    cpf: "33333333333333",
    nome: "Ubirajara de Oliveira",
    endereco: "Testing street, n 680, Between Hyde and Powell Streets, Sao Francisco, California, EUA",
    codigoFunci: 555,
    tipo: "GC",    
}

clCiclano = {
    cpf: "12345678987654",
    nome: "Ciclano da silva",
    nascimento: "10-01-1989",
    endereco: "Testing street, n 333, Between Hyde and Powell Streets, Sao Francisco, California, EUA",
    estadoCivil: "Casado",
};

clBeltrano = {
    cpf: "98765432123456",
    nome: "Beltrano da silva",
    nascimento: "01-03-1970",
    endereco: "Testing street, n 333, Between Hyde and Powell Streets, Sao Francisco, California, EUA",
    estadoCivil: "Casado",
};

cc200 = {
    numero: 200,
    titular: [
        clCiclano,
        clBeltrano,
    ],
    tipo: "PJ",
    gerente: gerBira,
};

agCaliforniaAlfa = {
    codigo: 0002,
    nome: "California",
    endereco: "Rua Testing, n 122, Between Hyde and Powell Streets, Sao Francisco, California, EUA",
    conta: [
        cc200,
    ],
};

agLondresAlfa = {
    codigo: 0101,
    nome: "Londres",
    endereco: "Rua Tesing, n 525, Croydon, Londres, Boroughs, England",
    conta: [],
};

bancoAlfa = {
    codigo: 011,
    nome: "Alfa",
    agencia: [
        agWebAlfa,
        agCaliforniaAlfa,
        agLondresAlfa,
    ],
};

//Banco Beta
agWebBeta = {
    codigo: 0001,
    nome: "Web",
    endereco: "Rua Testando, n 55, loja 2, NA, NA, NA, Brasil",
    conta: [],
};

bancoBeta = {
    codigo: 241,
    nome: "Beta",
    agencia: [
        agWebBeta,
    ],
};

//Banco Omega
agWebOmega = {
    codigo: 0001,
    nome: "Web",
    endereco: "Rua Testando, n 55, loja 3, NA, NA, NA, Basil",
    conta: [],
};

agItuOmega = {
    codigo: 8761,
    nome: "Itu",
    endereco: "Rua do meio, n 2233, Qualquer, Itu, Sao Paulo, Brasil",
};

agHermanaOmega = {
    codigo: 4567,
    nome: "Hermana",
    endereco: {},
    consolidacao: [{}],
    conta: [{
        codigo: "0",
        tipoConta: "PF",
        saldo: 0.0,
        gerente: [{
            nome: "",
            cpf: "000.000.000-00",
            estadoCivil: "casado",
            dataNascimento: "00/00/0000",
            codigoFuncionario: "00011",
            tipoGerente: "GC",
            endereco: {
                lugradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 1",
                cep: "000000-00",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
            }
        }],
        titular: [{}],
        movimentacao: [{
            tipo: "",
            valor: "",
        }]
    }],
};

bancoOmega = {
    codigo: 307,
    nome: "Omega",
    agencia: [
        agWebOmega,
        agItuOmega,
        agHermanaOmega,
    ],
};

//insert bancos
db.banco.insert(
    [
        bancoAlfa,
        bancoBeta,
        bancoOmega,
    ]
);

db.banco.find();
db.banco.find({ nome: "Fulano" }).pretty();
db.banco.find({ nome: { $regex: /lf/ } }).pretty();
db.banco.find({ codigo: { $eq: "011" } }).pretty(); // =
db.banco.find({ codigo: { $ne: "011" } }).pretty(); // <>
db.banco.find({ saldo: { $in: [ 10, 100 ] } }); //in
db.banco.find({ saldo: { $gt: 1 } }).pretty();
db.banco.find({nome : "Alfa" }, { "agencia": 1 }); //vai retornar só agencias do banco alfa
db.banco.find({nome: "Alfa" }, { "agencia.endereco": 1 });//vai retornar só enderecos das agencias do banco alfa
    