use sistemaBancario;
db.banco.find();

db.banco.update( { nome: "Alfa" }, { $set: {nome: "Alfa 2"} } );//muda soh o nome do documento
db.banco.updateOne( { nome: "Alfa" }, { nome: "Alfa 2" } );
db.banco.update( {_id: ObjectId("24fg345yg5y54yw45h") }, { nome: "Alfa 2" } );//pesquisar por id

/*
    $unset - apagar campo
    $inc - incrementar valor
    $mul - multiplicar valor 
    $rename - renomear campo
*/

db.banco.deleteOne({});//{query}
db.banco.deleteMany();