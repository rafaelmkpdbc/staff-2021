use bancoNormalizado;

db.createCollection("pais");

nap = {
    nomePais: "NA"
};

brasil = {
    nomePais: "Brasil"
};

eua = {
    nomePais: "EUA"
};

england = {
    nomePais: "England"
};

argentina = {
    nomePais: "Argentina"
};

db.pais.insert([
    nap,
    brasil,
    eua, 
    england,
    argentina
]);
    

db.createCollection("estado");

nae = {
    nomeEstado: "NA"
};

california = {
    nomeEstado: "California"
};

saoPaulo = {
    nomeEstado: "Sao Paulo"
};

boroughs = {
    nomeEstado: "Boroughs"
};

bsase = {
    nomeEstado: "Buenos Aires"
};

db.estado.insert([
    nae,
    california, 
    saoPaulo,
    boroughs,
    bsase
]);

db.createCollection("cidade");

nac = {
    nomeCidade: "NA",
};

saoFranc = {
    nomeCidade: "Sao Francisco"
};

londres = {
    nomeCidade: "Londres"
};

bsas = {
    nomeCidade: "Buenos Aires"
};

itu = {
    nomeCidade: "Itu"
}

db.cidade.insert([
    nac,
    saoFranc,
    londres,
    bsas,
    itu
]);

db.createCollection("bairro");

nab = {
    nomeBairro: "NA"
};

qualquer = {
    nomeBairro: "Qualquer"
};


bhanps = {
    nomeBairro: "Between Hyde and Powell Streets"
};

croydon = {
    nomeBairro: "Croydon"
};

caminito = {
    nomeBairro: "Caminito"
};

db.bairro.insert([
    nab,
    qualquer,
    bhanps,
    croydon,
    caminito   
]);

db.createCollection("endereco");

enderecoAlfaWeb = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfda"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfcc"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd5"),
    cidade: ObjectId("60bfca63fbf33f2aba8fdfd0"),
    logradouro: "Rua Testando",
    numero: 55,
    complemento: "loja 1"
};

enderecoAlfaCalifornia = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfdc"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfcd"),
    cidade: ObjectId("60bfca63fbf33f2aba8fdfd1"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd7"),
    logradouro: "Rua Testing",
    numero: 122
};

enderecoAlfaLondres = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfdd"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfcf"),
    cidade: ObjectId("60bfca63fbf33f2aba8fdfd2"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd8"),
    logradouro: "Rua Tesing",
    numero: 525
};

enderecoBetaWeb = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfda"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfcc"),
    cidade: ObjectId("60bfca63fbf33f2aba8fdfd0"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd5"),
    logradouro: "Rua Testando",
    numero: 55,
    complemento: "loja 2"    
};

enderecoOmegaWeb = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfda"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfcc"),
    cidade: ObjectId("60bfca63fbf33f2aba8fdfd0"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd5"),
    logradouro: "Rua Testando",
    numero: 55,
    complemento: "loja 3"
};

enderecoOmegaItu = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfdb"),
    estado: ObjectId("60bfca59fbf33f2aba8fdfce"),
    cidade: ObjectId("60bfcf9cfbf33f2aba8fdfe8"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd6"),
    logradouro: "Rua do meio",
    numero: 2233
};

enderecoOmegaHermana = {
    pais: ObjectId("60bfcd58fbf33f2aba8fdfde"),
    estado: ObjectId("60bfcf58fbf33f2aba8fdfe3"),
    cidade: ObjectId("60bfcf9cfbf33f2aba8fdfe7"),
    bairro: ObjectId("60bfca6efbf33f2aba8fdfd9"),
    logradouro: "Rua do boca",
    numero: 222
};

db.endereco.insert([
    enderecoAlfaWeb,
    enderecoAlfaCalifornia,
    enderecoAlfaLondres,
    enderecoBetaWeb,
    enderecoOmegaWeb,
    enderecoOmegaItu,
    enderecoOmegaHermana
]);

db.createCollection("pessoa");

ger1 = {
    cpf: "111.111.111-11",
    nome: "Gerente da silva",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfe9"),
    codFunci: "0123",
    tipoGerente: "GC"
};

ger2 = {
    cpf: "222.222.222-22",
    nome: "Gerente oliveira",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfea"),
    codFunci: "3210",
    tipoGerente: "GC"
};

ger3 = {
    cpf: "333.333.333-33",
    nome: "Gerente rodrigues",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfeb"),
    codFunci: "1112",
    tipoGerente: "GC"
};

ger4 = {
    cpf: "444.444.444-44",
    nome: "Gerente da silvas",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfec"),
    codFunci: "4444",
    tipoGerente: "GC"
};

ger5 = {
    cpf: "555.555.555-55",
    nome: "Gerente da silvs",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfed"),
    codFunci: "4567",
    tipoGerente: "GC"
};

ger6 = {
    cpf: "666.666.666-66",
    nome: "Gerente da silvas",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfee"),
    codFunci: "4444",
    tipoGerente: "GC"
};

ger7 = {
    cpf: "777.777.777-77",
    nome: "Gerente da silvas",
    dataNascimento: "01-01-1990",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfef"),
    codFunci: "4444",
    tipoGerente: "GC"
};

cli1 = {
    cpf: "123.123.123-12",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfe9"),
};

cli2 = {
    cpf: "231.213.213-21",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfea"),
};

cli3 = {
    cpf: "321.321.321-32",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfeb"),
};

cli4 = {
    cpf: "654-654-654-65",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfec"),
};

cli5 = {
    cpf: "789.789.789-78",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfed"),
};

cli6 = {
    cpf: "789.789.789-78",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfee"),
};

cli7 = {
    cpf: "789.789.789-78",
    nome: "Cliente da silva",
    dataNascimento: "10-03-1970",
    estadoCivil: "Solteiro",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfef"),
};

db.pessoa.insert([
    ger1, 
    ger2,
    ger3,
    ger4,
    ger5,
    ger6,
    ger7,
    cli1,
    cli2,
    cli3,
    cli4,
    cli5,
    cli6,
    cli7,
]);

db.createCollection("movimentacao");

mov1 = {
    tipo: "saque",
    valor: 100.0
};

mov2 = {
    tipo: "saque",
    valor: 100.0
};

mov3 = {
    tipo: "saque",
    valor: 100.0
};

mov4 = {
    tipo: "saque",
    valor: 100.0
};

mov5 = {
    tipo: "deposito",
    valor: 100.0
};

mov6 = {
    tipo: "deposito",
    valor: 100.0
};

mov7 = {
    tipo: "deposito",
    valor: 100.0
};

db.movimentacao.insert([
    mov1,
    mov2,
    mov3,
    mov4,
    mov5,
    mov6,
    mov7
]);

db.createCollection("conta");

cc100 = {
    numero: "100",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff0"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdff7")
    ],
    tipoConta: "PJ",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fdffe")
    ]
};

cc200 = {
    numero: "200",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff1"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdff8")
    ],
    tipoConta: "PF",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fdfff")
    ]
};

cc300 = {
    numero: "300",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff2"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdff9")
    ],
    tipoConta: "CONJ",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fe000")
    ]
};

cc400 = {
    numero: "400",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff3"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdffa")
    ],
    tipoConta: "INVEST",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fe001")
    ]
};

cc500 = {
    numero: "500",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff4"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdffb")
    ],
    tipoConta: "PJ",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fe002")
    ]
};

cc600 = {
    numero: "600",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff5"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdffc")
    ],
    tipoConta: "PF",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fe003")
    ]
};

cc700 = {
    numero: "700",
    gerente: ObjectId("60bfd228fbf33f2aba8fdff6"),
    cliente: [
        ObjectId("60bfd228fbf33f2aba8fdffd")
    ],
    tipoConta: "CONJ",
    saldo: 0.0,
    movimentacao: [
        ObjectId("60bfd241fbf33f2aba8fe004")
    ]
};

db.conta.insert([
    cc100,
    cc200,
    cc300,
    cc400,
    cc500,
    cc600,
    cc700,
]);

db.createCollection("consolidacao");

con1 = {
    saldo: 0.0,
    saques: 0.0,
    depositos: 0.0,
    numCorrentistas: 0.0
};

db.consolidacao.insert([
    con1
]);

use bancoNormalizado;

db.createCollection("agencia");

webAlfa = {
    codigo: "0001",
    nome: "Web",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfe9"),
    consolidacao: [
        ObjectId("60bfd4b8fbf33f2aba8fe00c")
    ],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe005")
    ]
};

calidorniaAlfa = {
    codigo: "0001",
    nome: "California",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfe9"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe006")
    ]
};

londresAlfa = {
    codigo: "0001",
    nome: "Londres",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfeb"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe007")
    ]
};

webBeta = {
    codigo: "0001",
    nome: "Web",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfec"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe008")
    ]
};

webOmega = {
    codigo: "0001",
    nome: "",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfed"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe009")
    ]
};

ituOmega = {
    codigo: "0001",
    nome: "Itu",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfee"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe00a")
    ]
};

hermanaOmega = {
    codigo: "0001",
    nome: "Hermana",
    endereco: ObjectId("60bfd0d4fbf33f2aba8fdfef"),
    consolidacao: [],
    conta: [
        ObjectId("60bfd437fbf33f2aba8fe00b")
    ]
};

db.agencia.insert([
    webAlfa,
    californiaAlfa,
    londresAlfa,
    webBeta,
    webOmega,
    ituOmega,
    hermanaOmega
]);

db.createCollection("banco");

alfa = {
    codigo: "011",
    nome: "Alfa",
    agencia: [
        ObjectId("60bfd437fbf33f2aba8fe005"),
        ObjectId("60bfd437fbf33f2aba8fe006"),
        ObjectId("60bfd437fbf33f2aba8fe007")
    ]
};

beta = {
    codigo: "241",
    nome: "Beta",
    agencia: [
        ObjectId("60bfd437fbf33f2aba8fe008")
    ]
};

omega = {
    codigo: "307",
    nome: "Omega",
    agencia: [
        ObjectId("60bfd437fbf33f2aba8fe009"),
        ObjectId("60bfd437fbf33f2aba8fe00a"),
        ObjectId("60bfd437fbf33f2aba8fe00b")
    ]
};

db.banco.insert([
    alfa,
    beta,
    omega
]);


